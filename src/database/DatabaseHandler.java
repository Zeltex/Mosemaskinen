package database;

import java.io.File;
import java.util.ArrayList;
import java.util.prefs.Preferences;

import controllers.MainTabController;
import javafx.scene.control.ListView;
import managers.ProductManager;
import managers.UserManager;
import objects.Product;
import objects.User;
import objects.UserProduct;

public class DatabaseHandler {

	SQLiteDriverConnection driver;
	UserManager userManager;
	ProductManager productManager;
	MainTabController mtc;
	final String DATABASE_FILE_TAG = GlobalVariables.DATABASE_FILE_TAG;
	final String DATABASE_ROOT_TAG = GlobalVariables.DATABASE_ROOT_TAG;

	public DatabaseHandler(){
		driver = new SQLiteDriverConnection(this);
		userManager = new UserManager(this);
		productManager = new ProductManager(this);
//		this.mtc = mtc;
	}
	

	public void setMainTabController(MainTabController mtc) {
		this.mtc = mtc;
	}
	
	// Retrieves path to database from preferences
	private boolean getDatabasePathFromPreferences() {
		Preferences prefs = Preferences.userRoot().node("Mosemaskinen");
		String path = prefs.get(DATABASE_FILE_TAG, "");
		boolean result = true;
		if (path.isEmpty()) {
			prefs.put(DATABASE_FILE_TAG, GlobalVariables.defaultDatabasePath);
			GlobalVariables.databasePath = GlobalVariables.defaultDatabasePath;
			result = false;
		} else {
			GlobalVariables.databasePath = path;
		}
		path = prefs.get(DATABASE_ROOT_TAG, "");
		if (path.isEmpty()) {
			prefs.put(DATABASE_ROOT_TAG, GlobalVariables.defaultDatabaseRoot);
			GlobalVariables.databaseRoot = GlobalVariables.defaultDatabaseRoot;
			result = false;
		} else {
			GlobalVariables.databaseRoot = path;
		}
		return result;
	}

	// Loads database, creates new one if none exists
	public void loadDatabase() {
		if (new File(GlobalVariables.databasePath).exists()) {
			driver.createNewDatabase(GlobalVariables.databasePath);
			GlobalVariables.loadSettings(driver.loadSettings());
			productManager.setProducts(driver.selectAllProducts());
			userManager.setUsers(driver.selectAllUsers());
			GlobalVariables.databaseConnectionEstablished = true;

		} else {
			File f = new File(GlobalVariables.databasePath);
			f.getParentFile().mkdirs();
			driver.createNewDatabase(GlobalVariables.databasePath);
		}
	}

	// Create new settings file containing default database path
	public void createNewDefaultSettingsFile() {
		setPreferences(GlobalVariables.defaultDatabasePath, new File(GlobalVariables.defaultDatabasePath).getParent());
	}

//	// Creates new settings file which contains the path of the database
//	private void createNewSettingsFile(String path) {
//		File file = new File(GlobalVariables.settingsFilePath);
//		File directory = new File(file.getParent());
//		if (!directory.exists()) {
//			directory.mkdirs();
//		}
//		try (PrintWriter pw = new PrintWriter(file);){
//			pw.println("db: " + path);
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		}
//	}
	
	public void setPreferences(String databaseFile, String databaseRoot) {
		Preferences prefs = Preferences.userRoot().node("Mosemaskinen");
		if (databaseFile != null) prefs.put(DATABASE_FILE_TAG, databaseFile);
		if (databaseRoot != null) prefs.put(DATABASE_ROOT_TAG, databaseRoot);
	}

	// Load database
	public void load() {
		System.out.println("reached");
//		if (
				getDatabasePathFromPreferences();
//				) {
			loadDatabase();
//		} else {
//			// Prompt user to handle database
//			int result = DialogManager.chooseDatabasePathDialog();
//
//			// User canceled
//			if (result == 0) {
//
//			// User chosen directory
//			} else if (result == 1) {
//				File chosenDirectory = DialogManager
//						.getUserChosenDirectory(new File(GlobalVariables.databasePath).getParent());
//				if (chosenDirectory == null)
//					load();
//				else {
//					String chosenPath = chosenDirectory.getAbsolutePath();
//					GlobalVariables.databaseRoot = chosenPath;
//					GlobalVariables.databasePath = chosenPath + "\\" + GlobalVariables.databaseFileName;
//					setPreferences(GlobalVariables.databasePath, GlobalVariables.databaseRoot);
//					loadDatabase();
//				}
//
//			// User chosen existing database
//			} else if (result == 2) {
//				File selectedFile = DialogManager.getExistingDatabase("db",
//						new File(GlobalVariables.databasePath).getParent());
//				if (selectedFile == null) {
//					load();
//				} else {
//					String chosenPath = selectedFile.getAbsolutePath();
//					GlobalVariables.databasePath = chosenPath;
//					GlobalVariables.databaseRoot = new File(chosenPath).getParent();
//					setPreferences(GlobalVariables.databasePath, GlobalVariables.databaseRoot);
//					loadDatabase();
//					
//				}
//
//			// Default folder
//			} else if (result == 3) {
//				createNewDefaultSettingsFile();
//				GlobalVariables.databasePath = GlobalVariables.defaultDatabasePath;
//				GlobalVariables.databaseRoot = new File(GlobalVariables.defaultDatabasePath).getParent();
//				loadDatabase();
//			}
//		}
	}
	
	public void cleanupRTC() {
		String unknown = "Unknown";
		if (cleanupRTCChild(GlobalVariables.ranks)) {
			boolean found = false;
			for (User user : userManager.getUsers()) {
				if (user.getRank().equals(unknown)) found = true;
			}
			if (!found) GlobalVariables.ranks.remove(unknown);
		}
		if (cleanupRTCChild(GlobalVariables.teams)){
			boolean found = false;
			for (User user : userManager.getUsers()) {
				if (user.getTeam().equals(unknown)) found = true;
			}
			if (!found) GlobalVariables.teams.remove(unknown);
		}
		if (cleanupRTCChild(GlobalVariables.categories)){
			boolean found = false;
			for (Product product : productManager.getProducts()) {
				if (product.getCategory().equals(unknown)) found = true;
			}
			if (!found) GlobalVariables.categories.remove(unknown);
		}
	}
	
	private boolean cleanupRTCChild(ArrayList<String> entries) {
		boolean result = false;
		for (int i = 0; i < entries.size(); i ++) {
			for (int j = i + 1; j < entries.size(); j ++) {
				if (entries.get(i).equals(entries.get(j))) {
					entries.remove(j);
				}
			}
			if (entries.get(i).equals("Unknown")) result = true;
		}
		return result;
	}
	
	public ArrayList<String> getCategoryIDS(String category) {
		ArrayList<String> result = new ArrayList<String>();
		for (Product p : productManager.getProducts()) {
			if (p.getCategory().equals(category)) result.add(p.getID());
		}
		return result;
	}
	
	public boolean changeUserBarCodes(ArrayList<Long> difference) {
		return driver.changeUserBarCodes(difference);
	}
	public boolean changeProductBarCodes(ArrayList<Long> difference) {
		return driver.changeProductBarCodes(difference);
	}
	
	public ArrayList<Long> getUserBarCodes() {
		return driver.getUserBarCodes();
	}
	public ArrayList<Long> getProductBarCodes() {
		return driver.getProductBarCodes();
	}
	
	public boolean editMultiple(String table, ArrayList<ArrayList<String>> outer) {
		return driver.editMultiple(table, outer);
	}
	
	public void updateUserComboBoxes() {
		mtc.updateUserComboBoxes();
	}
	
//	public void printSettings() {
//		driver.printAllSettingsNames();
//	}
	
	public boolean updateTeams(String oldTeam, String newTeam) {
		return driver.updateTeams(oldTeam, newTeam);
	}
	public boolean nullTeams(ArrayList<String> oldTeam) {
		return driver.nullTeams(oldTeam);
	}

	public boolean updateRanks(String oldRank, String newRank) {
		return driver.updateRanks(oldRank, newRank);
	}
	
	public boolean nullRanks(ArrayList<String> oldRank) {
		return driver.nullRanks(oldRank);
	}
	public boolean nullCategories(ArrayList<String> oldCategory) {
		return driver.nullCategories(oldCategory);
	}
	
	public boolean updateCategories(String oldCategory, String newCategory) {
		return driver.updateCategories(oldCategory, newCategory);
	}
	
	public boolean editInformation(String table, ArrayList<String> difference, String id) {
		ArrayList<String> ids = new ArrayList<String>();
		ids.add(id);
		return driver.edits(table, difference, ids);
	}
	
	public boolean editInformation(String table, ArrayList<String> difference, ArrayList<String> ids) {
		return driver.edits(table, difference, ids);
	}
	
	public boolean editSettings(String row, String value) {
		return driver.editSettings(row, value);
	}

	public ArrayList<String> getColumnNames() {
		return driver.getColumnNames();
	}
	
	public Long getFreeUserBarCodeNumber() {
		return driver.getFreeUserBarCode();
	}
	public Long getFreeProductBarCodeNumber() {
		return driver.getFreeProductBarCode();
	}

	// User methods
	public ArrayList<User> getUsers() {
		return userManager.getUsers();
	}
	
	public int getFreeUserIDNumber() {
		return driver.getFreeID("users");
	}
	
	public User getUserFromBarCode(long barCode) {
		return userManager.getUserFromBarCode(barCode);
	}
	
	public User getUserByIndex(int index) {
		return userManager.getUserByIndex(index);
	}

	public void updateUser(ArrayList<String> difference, String id) {
		userManager.updateUser(difference, id);
	}

	public void sortUsers(ListView<User> userList, String showFilter, boolean realNames, int sort,
			boolean direction) {
		userManager.sortUsers(sort);
		updateUserList(userList, showFilter, realNames, sort);
		if (!direction)
			userManager.changeDirection(userList, realNames, sort);
	}

	public void changeUserDirection(ListView<User> userList, boolean realNames, int sort) {
		userManager.changeDirection(userList, realNames, sort);
	}

	public void updateUserListType(ListView<User> userList, boolean realNames, int sort) {
		userManager.updateUserListType(userList, realNames, sort);
	}

	public void updateUserList(ListView<User> userList, String filter, boolean realNames, int sort) {
		userManager.updateUserList(userList, filter, realNames, sort);
	}
	public boolean loadUserProductsToNewUser(User user) {
		User temp = userManager.getAUser();
		if (temp != null) {
			if (temp.getUserProducts() != null) {
				ArrayList<UserProduct> temp2 = new ArrayList<UserProduct>();
				temp2.addAll(temp.getUserProducts());
				for (UserProduct up : temp2) {
					up.setAmount(0.0);
				}
				user.setProducts(temp2);
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	
	public User getAUser() {
		return userManager.getAUser();
	}

	public boolean addUser(User user) {
		if (driver.insertUser(user)) {
			if (!loadUserProductsToNewUser(user)){
				mtc.load();
			} else {
				userManager.addUser(user);
			}
			return true;
		} else {
			return false;
		}
	}
	
	public boolean addUsers(ArrayList<User> users) {
		if (driver.insertUsers(users)) {
			userManager.addUsers(users);
			return true;
		} else {
			return false;
		}
	}

	public void deleteUsers(String[] ID) {
		driver.deleteUsers(ID);
		userManager.deleteUsers(ID);
	}
	
	public String getUserNameFromID(String ID) {
		return userManager.getUserNameFromID(ID);
	}
	
	public int getShownUserIndex(String ID) {
		return userManager.getShownUserIndex(ID);
	}
	
	public User getUserFromID(String ID) {
		for (User user : userManager.getUsers()) {
			if (user.getID().equals(ID)) return user;
		}
		return null;
	}

	
	//Product methods
	
	public void updateProductComboBoxes() {
		mtc.updateProductComboBoxes();
	}
	
	public int getFreeProductIDNumber() {
		return driver.getFreeID("products");
	}
	
	public ArrayList<Product> getProducts() {
		return productManager.getProducts();
	}
	
	public ArrayList<String> getProductNames() {
		ArrayList<String> names = new ArrayList<String>();
		for (Product p : productManager.getProducts()) {
			names.add(p.getName());
		}
		return names;
	}
	
	public Product getProductFromBarCode(long barCode) {
		return productManager.getProductFromBarCode(barCode);
	}
	
	public Product getProductByIndex(int index) {
		return productManager.getProductByIndex(index);
	}

	public void updateProduct(ArrayList<String> difference, String id) {
		productManager.updateProduct(difference, id);
	}
	
	public void updateProducts(ArrayList<String> difference, ArrayList<String> ids) {
		productManager.updateProducts(difference, ids);
	}

	public void sortProducts(ListView<Product> productList, String showFilter, int sort, boolean direction) {
		productManager.sortProducts(sort);
		updateProductList(productList, showFilter, sort);
		if (!direction)
			productManager.changeDirection(productList, sort);
	}

	public void changeProductDirection(ListView<Product> productList, int sort) {
		productManager.changeDirection(productList, sort);
	}

	public void updateProductList(ListView<Product> productList, String filter, int sort) {
		productManager.updateProductList(productList, filter, sort);
	}

	public boolean addProduct(Product p) {
		ArrayList<Product> temp = new ArrayList<Product>();
		temp.add(p);
		if (driver.insertProducts(temp)) {
			productManager.addProducts(temp);
			return true;
		} else {
			return false;
		}
	}
	
	public boolean addProducts(ArrayList<Product> p) {
		if (driver.insertProducts(p)) {
			productManager.addProducts(p);
			return true;
		} else {
			return false;
		}
	}

	public void createNewProductTable() {
		driver.createNewProductTable();
	}
	
	public int getShownProductIndex(String ID) {
		return productManager.getShownProductIndex(ID);
	}
	
	public String getProductNameFromID(String ID) {
		return productManager.getProductNameFromID(ID);
	}
	
	public Product getProductFromID(String ID) {
		return productManager.getProductFromID(ID);
	}
	
	public void updateSoldProducts(ArrayList<String> ID, ArrayList<Double> amount) {
		productManager.updateSoldProducts(ID, amount);
	}
	
	public void updateUserProductName(String ID, String name) {
		userManager.updateUserProductName(ID, name);
	}
	
	public void updateUserProductNames(ArrayList<String> IDs, String name) {
		userManager.updateUserProductNames(IDs, name);
	}
	
	public void updateProductSold(String ID) {
		double amount = userManager.getProductSold(ID);
		productManager.getProductFromID(ID).setSold(amount);
	}
	
	public void updateAllProductSold() {
		for (Product p : productManager.getProducts()) {
			double amount = userManager.getProductSold(p.getID());
			p.setSold(amount);
		}
	}
	
	
//	 //Debug
//    public void printAllColumnIDS() {
//	    driver.printAllColumnIDS();
//    }
//    //Debug
//    public void printAllProductNames() {
//    	driver.printAllProductNames();
//    }
//    //Debug
//    public void printAllUserNames() {
//    	driver.printAllUserNames();
//    }
    
    public void deleteProducts(String[] IDs){
    	driver.deleteProducts(IDs);
    	productManager.deleteProducts(IDs);
    }
    
    public double getCount(Product product) {
    	return driver.getCount(product);
    }
}
