package database;

import java.io.File;
import java.util.ArrayList;
import java.util.prefs.Preferences;

import importedCode.NumberFormatChecker;

public class GlobalVariables {
	
	//This class contains variables used in various locations in the program
	//Contains all (atm) settings variables
	
	private GlobalVariables() {	
	}
	
	//Default variables
	public static String defaultDatabasePath;
	public static String defaultDatabaseRoot;
	public static String databaseFileName;
	public static String deletedProductString;
	public static String[] genderNames;
	public static String defaultPassword;
	public static String defaultBannerIconPath;
//	public static String transactionFolder;
	public static String[] themes;
	public final static String DATABASE_FILE_TAG = "DatabaseFile";
	public final static String DATABASE_ROOT_TAG = "DatabaseRoot";
	
	//Changeable variables
	public static boolean databaseConnectionEstablished;
	public static String databasePath;
	public static String databaseRoot;
	public static String bannerIconPath;
	public static ArrayList<String> teams;
	public static ArrayList<String> ranks;
	public static ArrayList<String> categories;
	public static ArrayList<String> soundFiles;
	public static ArrayList<Integer> soundMultiplier;
	public static String[] rowNames;
	public static boolean lockScreen;
	public static boolean backupInProgress;
	public static double backupTimer;
	public static String password;
	public static String recoveryEmail;
	public static String transactionFolderName;
	public static String transactionFile;
	public static int currentThemeInt;
	public static String backupLocation;
	public static boolean debugMode;
	public static boolean lockScan;
	
	public static void init() {
		defaultDatabasePath = "C:\\Mosemaskinen\\Mosemaskinen.db";
		defaultDatabaseRoot = "C:\\Mosemaskinen";
		databaseFileName = "Mosemaskinen.db";
		deletedProductString = "deletedProduct";
		genderNames = new String[]{"Unknown", "Guy", "Girl"};
		databaseConnectionEstablished = false;
//		databasePath = defaultDatabasePath;
		defaultBannerIconPath = "/resources/images/creaft-beers.jpg";
		bannerIconPath = "";
		teams = new ArrayList<String>();
		ranks = new ArrayList<String>();
		categories = new ArrayList<String>();
		soundFiles = new ArrayList<String>();
		soundMultiplier = new ArrayList<Integer>();
		rowNames = new String[]{"teams", "ranks", "categories", "genderNames", "recoveryEmail", 
				"password", "bannerImage", "backupTimer", "backupLocation","sounds","theme","lockScan"};
		lockScreen = false;
		backupInProgress = false;
		defaultPassword = "OLProgram";
		password = defaultPassword;
		recoveryEmail = "mosemaskinen@gmail.com";
//		transactionFolder = "C:\\sqlite\\transactions";
		transactionFolderName = "transactions";
		transactionFile = "";
		backupTimer = 0.0;
		themes = new String[]{"Default","Darcula"};
		currentThemeInt = 1;
		backupLocation = "C:\\Mosemaskinen\\Backup";
		debugMode = true;
		lockScan = false;
	}
	
	public static void setStandardVariables() {
		categories = new ArrayList<String>();
		categories.add("Beer");
		categories.add("Cider");
		categories.add("Cocio");
		categories.add("Soft drinks");
		categories.add("Unknown");
		ranks = new ArrayList<String>();
		ranks.add("Vector");
		ranks.add("Kabs");
		ranks.add("Guest");
		ranks.add("Bombz");
		ranks.add("Rus");
	}
	
	public static String getTransactionsFolder() {
		return new File(databasePath).getParent() + "\\" + transactionFolderName;
	}
	
	
	// Returns whether the screen is locked. It is during backup
	public static boolean isScreenLocked() {
		return lockScreen;
	}
	
	// Loads settings
	public static void loadSettings(ArrayList<String> settings) {
		teams = new ArrayList<String>();
		ranks = new ArrayList<String>();
		categories = new ArrayList<String>();
		
		for (int i = 0; i < settings.size(); i += 2) {
			switch(settings.get(i)) {
			case "teams": {
				if (settings.get(i+1) != null) {
					for (String s : settings.get(i + 1).split("\\_")) {
						if (!s.isEmpty())teams.add(s);
					} 
				} else {System.out.println("Loaded teams null");}
				break;
			}
			case "ranks": {
				if (settings.get(i+1) != null) {
					for (String s : settings.get(i + 1).split("\\_")) {
						if (!s.isEmpty()) ranks.add(s);
					} 
				} else {System.out.println("Loaded ranks null");}
				break;
			}
			case "categories": {
				if (settings.get(i+1) != null) {
					for (String s : settings.get(i + 1).split("\\_")) {
						if (!s.isEmpty()) categories.add(s);
					} 
				} else {System.out.println("Loaded categories null");}
				break;
			}
			case "genderNames": 
				if (settings.get(i + 1) != null){
					genderNames = settings.get(i + 1).split("\\_");
				} else {System.out.println("Loaded categories null");} 
				break;
			case "recoveryEmail": 
				if (settings.get(i + 1) != null) {
					recoveryEmail = settings.get(i + 1);
				} else {System.out.println("Loaded recovery email null");} 
				break;
			case "password": 
				if(settings.get(i + 1) != null) {
					password = settings.get(i + 1);
				} else {System.out.println("Loaded password null");} 
				break;
			
			case "bannerImage":
				if(settings.get(i + 1) != null) {
					bannerIconPath = settings.get(i + 1);
				} else {
					System.out.println("Loaded banner image null");
				}
				break;

			case "backupTimer":
				if (settings.get(i + 1) != null) {
					if (NumberFormatChecker.isDouble(settings.get(i + 1))){
						backupTimer = Double.valueOf(settings.get(i + 1));
					} else {backupTimer = 0.0;}
				} else {
					System.out.println("Loaded backupTimer null");
				}
				break;

			case "backupLocation":
				if (settings.get(i + 1) != null) {
					backupLocation = settings.get(i + 1);
				} else {
					System.out.println("Loaded backup location null");
				}
				break;
			case "sounds":
				if (settings.get(i + 1) != null) {
					for (String s : settings.get(i + 1).split("\\_")) {
						if (!s.isEmpty()) {
							if (NumberFormatChecker.isInteger(s.substring(s.length() - 1, s.length()))) {
								soundFiles.add(s.substring(0, s.length() - 1));
								soundMultiplier.add(Integer.valueOf(s.substring(s.length() - 1, s.length())));
							}
						}
					}
				} else {
					System.out.println("Loaded banner image null");
				}
				break;
			case "theme":
				if (settings.get(i + 1) != null) {
					if (NumberFormatChecker.isInteger(settings.get(i + 1))) {
						currentThemeInt = Integer.parseInt(settings.get(i + 1));
					}
				} else {
					System.out.println("Loaded theme null");
				}
				break;
			case "lockScan":
				if (settings.get(i + 1) != null) {
					switch(settings.get(i + 1).toLowerCase()) {
					case "yes": lockScan = true; break;
					case "no": lockScan = false; break;
					}
				} else {
					System.out.println("Loaded lockscan null");
				}
				break;
			}
			
		}
	}
	
	// Returns all the save strings to be saved
	public static ArrayList<String> getSaveStrings(ArrayList<String> names) {
		ArrayList<String> result = new ArrayList<String>();
		for (String name : names) {
			String input = getSaveStrings(name);
			if (input != null) result.add(input);
		}
		return result;
	}

	// Converts settings information to a string which can be stored in database
	public static String getSaveStrings(String name){
		switch(name) {
		case "teams": return toSettingsString(teams);
		case "ranks":  return toSettingsString(ranks);
		case "categories": return toSettingsString(categories);
		case "genderNames": return toSettingsString(genderNames);
		case "recoveryEmail": return recoveryEmail;
		case "password": return password;
		case "bannerImage": return bannerIconPath;
		case "backupTimer": return backupTimer + "";
		case "backupLocation": return backupLocation;
		case "sounds": return soundToSettingsString();
		case "theme": return currentThemeInt + "";
		case "lockScan": return lockScan ? "yes" : "no";
		default: return null;
		}
		
	}
	
	//Converting sound settings to a string
	private static String soundToSettingsString() {
		String output = "";
		for (int i = 0; i < soundFiles.size(); i ++) {
			if (i != 0) output += "_";
			output += soundFiles.get(i) + soundMultiplier.get(i);
		}
		return output;
	}
	
	// Converting a settings array list to a string
	private static String toSettingsString(ArrayList<String> input) {
		String output = "";
		for (int i = 0; i < input.size(); i ++) {
			if (i != 0) output += "_";
			output += input.get(i);
		}
		return output;
	}
	
	// Converting a settings array to a string
	private static String toSettingsString(String[] input) {
		String output = "";
		for (int i = 0; i < input.length; i ++) {
			if (i != 0) output += "_";
			output += input[i];
		}
		return output;
	}
	
	// Checks whether the given entry is registered
	public static boolean confirmRank(String rank) {
		return ranks.contains(rank);
	}
	public static boolean confirmCategory(String category) {
		return categories.contains(category);
	}
	public static boolean confirmTeam(String team) {
		return teams.contains(team);
	}
	public static boolean confirmGender(String gender) {
		for (String s : genderNames) {
			if (gender.equals(s)) return true;
		}
		return false;
	}
	
	// Return the entries that are not registered
	public static ArrayList<String> getNewRanks(ArrayList<String> check) {
		return getNewEntries(check, ranks);
	}
	public static ArrayList<String> getNewCategories(ArrayList<String> check) {
		return getNewEntries(check, categories);
	}
	public static ArrayList<String> getNewTeams(ArrayList<String> check) {
		return getNewEntries(check, teams);
	}
	public static ArrayList<String> getNewEntries(ArrayList<String> entries, ArrayList<String> list) {
		ArrayList<String> result = new ArrayList<String>();
		for (String s : entries) {
			if (!list.contains(s) && !s.isEmpty()) result.add(s);
		}
		return result;
	}
	
	// Returns the registered entries not present in the parsed entries
	public static ArrayList<String> getMissingRanks(ArrayList<String> check) {
		return getMissingEntries(check, ranks);
	}
	public static ArrayList<String> getMissingCategories(ArrayList<String> check) {
		return getMissingEntries(check, categories);
	}
	public static ArrayList<String> getMissingTeams(ArrayList<String> check) {
		return getMissingEntries(check, teams);
	}	
	public static ArrayList<String> getMissingEntries(ArrayList<String> entries, ArrayList<String> list) {
		ArrayList<String> result = new ArrayList<String>();
		for (String s : list) {
			if (!entries.contains(s)) result.add(s);
		}
		return result;
	}
	
	// Update an entry
	public static void updateRank(String originalRank, String newRank) {
		updateEntry(originalRank, newRank, ranks);
	}
	public static void updateCategory(String originalCategory, String newCategory) {
		updateEntry(originalCategory, newCategory, categories);
	}
	public static void updateTeam(String originalTeam, String newTeam) {
		updateEntry(originalTeam, newTeam, teams);
	}
	public static void updateEntry(String originalEntry, String newEntry, ArrayList<String> list) {
		for (int i = 0; i < list.size(); i ++) {
			if (list.get(i).equals(originalEntry)) list.set(i, newEntry);
		}
	}
	
	// Delete an entry
	public static void deleteRank(String rank){ 
		ranks.remove(rank);
	}	
	public static void deleteCategory(String category){ 
		categories.remove(category);
	}
	public static void deleteTeam(String team){ 
		teams.remove(team);
	}
	
	// Sets entries empty
	public static void setCategoriesEmpty() {
		categories = new ArrayList<String>();
	}
	public static void setTeamsEmpty() {
		teams = new ArrayList<String>();
	}
	public static void setRanksEmpty() {
		ranks = new ArrayList<String>();
	}

	// Resets the database location to default
	public static void resetDatabasePath() {
		databasePath = defaultDatabasePath;
		createNewDefaultSettingsFile();
	}
	
	// Sets new database location
	public static void setNewDatabase(String path) {
		databasePath = path;
		createNewDefaultSettingsFile();
	}
	
	// Sets new database location
	public static void setExistingDatabase(String path) {
		databasePath = path;
		createNewDefaultSettingsFile();
	}
	
	// Returns gender name
	public static String genderName(int which) {
		if (which >= 1 && which < genderNames.length) return genderNames[which];
		else return genderNames[0];
	}
	
	// Returns index of gender name, default return 0, "Unknown"
	public static int getGenderInt(String gender) {
		if (gender.equals(genderNames[2])) return 2;
		if (gender.equals(genderNames[1])) return 1;
		return 0;
	}
	
	public static void addRank(String rank) {ranks.add(rank);}
	public static void addRank(ArrayList<String> rank) {ranks.addAll(rank);}
	public static void addCategory(String category) {categories.add(category);}
	public static void addCategory(ArrayList<String> category) {categories.addAll(category);}
	public static void addTeam(String team) {teams.add(team);}
	public static void addTeam(ArrayList<String> team) {teams.addAll(team);}
	
	
	public static String[] getThemes() {return themes;}
	public static String[] getGenders() {return new String[]{genderNames[1],genderNames[2]};}
	public static String[] getGendersAll() {return genderNames;}
	public static ArrayList<String> getRanks() {return ranks;}
	public static ArrayList<String> getTeams() {return teams;}
	public static ArrayList<String> getCategories() {return categories;}
	public static String getPassword() {return password;}
	public static String getRecoveryEmail() {return recoveryEmail;}
	public static void setRecoveryEmail(String email) {recoveryEmail = email;}
	public static void setPassword(String newPassword) {password = newPassword;}
	public static void setBannerLocation(String path) {bannerIconPath = path;}
	public static void setLockScreen(boolean lock) {lockScreen = lock;}
	
	// Create new settings file containing default database path
		public static void createNewDefaultSettingsFile() {
			setPreferences(GlobalVariables.databasePath, new File(GlobalVariables.databasePath).getParent());
		}
	
	public static void setPreferences(String databaseFile, String databaseRoot) {
		Preferences prefs = Preferences.userRoot().node("Mosemaskinen");
		if (databaseFile != null) prefs.put(DATABASE_FILE_TAG, databaseFile);
		if (databaseRoot != null) prefs.put(DATABASE_ROOT_TAG, databaseRoot);
	}
}
