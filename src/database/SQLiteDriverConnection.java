package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import importedCode.NumberFormatChecker;
import objects.Product;
import objects.User;
import objects.UserProduct;

public class SQLiteDriverConnection {
	final static int STRING = 0, DOUBLE = 1;
	String[] productReals = new String[]{"totalStock", "currentStock", "sold", "price"};
	DatabaseHandler dbHandler;
	
	public SQLiteDriverConnection(DatabaseHandler dbHandler) {
		this.dbHandler = dbHandler;
	}
	
	// Gets total sold units of product
	public double getCount(Product product) {
		String entry = product.getID() ;
		String sql = "SELECT " +  entry +" FROM users";
		double total = 0;
		try (Connection conn = this.connect(); PreparedStatement pstmt = conn.prepareStatement(sql);
			ResultSet rs = pstmt.executeQuery();) {
			while (rs.next()) {
				total += rs.getDouble(entry);
			}
			return total;
		} catch (SQLException e) {
			return 0;
		}
		
	}
	
	// Returns all bar codes
	public ArrayList<Long> getUserBarCodes() {
		String sql = "SELECT barCode FROM users";
		return getBarCodesChild(sql);		
	}	
	public ArrayList<Long> getProductBarCodes() {
		String sql = "SELECT barCode FROM products";
		return getBarCodesChild(sql);		
	}
	private ArrayList<Long> getBarCodesChild(String sql) {
		ArrayList<Long> result = new ArrayList<Long>();
		try (Connection conn = this.connect(); PreparedStatement pstmt = conn.prepareStatement(sql);
			ResultSet rs = pstmt.executeQuery();) {
			while (rs.next()) {
				result.add(rs.getLong("barCode"));
			}
			return result;
		} catch (SQLException e) {
			return null;
		}
	}
	

	// Makes sure database and tables exists
	public void createNewDatabase(String path) {
		try (Connection conn = connect()) {
			if (conn != null) {
				conn.getMetaData();
				createNewUserTable();
				createNewProductTable();
				createNewSettingsTable();
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	// Makes sure users table exists
	public void createNewUserTable() {
		String sql = "CREATE TABLE IF NOT EXISTS users (\n" + "	id text PRIMARY KEY NOT NULL,\n"
				+ "	name text NOT NULL,\n" + "	callingName text NOT NULL,\n" + "	rank text NOT NULL,\n"
				+ "	team text NOT NULL,\n" + "	study text NOT NULL,\n" + "	studyNumber text NOT NULL,\n" + "	rustur text NOT NULL,\n"
				+ "	gender real NOT NULL,\n" + "	barCode real NOT NULL\n" + ");";
		createNewTable(sql);
	}
	
	// Makes sure products table exists
	public void createNewProductTable() {
		String sql = "CREATE TABLE IF NOT EXISTS products (\n" + "	id text PRIMARY KEY NOT NULL,\n"
				+ "	name text NOT NULL,\n" + "	totalStock real NOT NULL,\n" + "	currentStock real NOT NULL,\n"
				+ "	sold real NOT NULL,\n" + "	category text NOT NULL,\n" + "	price real NOT NULL,\n" + "	barCode real NOT NULL\n" + ");";
		createNewTable(sql);
	}
	
	// Makes sure setings table exists
	public void createNewSettingsTable() {
		String sql = "CREATE TABLE IF NOT EXISTS settings (\n" + "	name text PRIMARY KEY NOT NULL,\n" + 
	"	value text NOT NULL\n" + ");";
		createNewTable(sql);
		createSettingsColumns();
	}
	
	// Makes sure the settings table contains all wanted rows
	public void createSettingsColumns() {
		ArrayList<String> retreivedNames = new ArrayList<String>();
		ArrayList<String> namesToAdd = new ArrayList<String>();
		ArrayList<String> stringsToAdd = new ArrayList<String>();
		String sqlGet = "SELECT name FROM settings";
		String sqlInsert = "INSERT INTO settings (name, value) VALUES(?,?)";


		try (Connection conn = this.connect(); PreparedStatement pstmt = conn.prepareStatement(sqlGet);
				PreparedStatement pstmt2 = conn.prepareStatement(sqlInsert); ResultSet rs = pstmt.executeQuery();) {
			// Existing rows
			while (rs.next()) {
				 retreivedNames.add(rs.getString("name"));
			}
			// Rows to add
			for (String s : GlobalVariables.rowNames) {
				if (!retreivedNames.contains(s)) namesToAdd.add(s);
			}
			stringsToAdd = GlobalVariables.getSaveStrings(namesToAdd);
			conn.setAutoCommit(false);
			
			// Adding rows
			for (int i = 0; i < namesToAdd.size(); i ++) {
				pstmt2.setString(1, namesToAdd.get(i));
				pstmt2.setString(2, stringsToAdd.get(i));
				pstmt2.executeUpdate();
			}
			conn.commit();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

	// Creates new table
	public void createNewTable(String sql) {
		try (Connection conn = connect(); Statement stmt = conn.createStatement()) {
			stmt.execute(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	// Deletes users
	public void deleteUsers(String[] ID) {
		String sql = "DELETE FROM users WHERE id = ?";
		try (Connection conn = connect(); PreparedStatement pstmt = conn.prepareStatement(sql); ){
			
			conn.setAutoCommit(false);
			for (int i = 0; i < ID.length; i ++) {
				pstmt.setString(1, ID[i]);
				pstmt.executeUpdate();
			}
			conn.commit();
		} catch (SQLException e) {
			e.printStackTrace();
		} 

	}
	
	// Gets a free barcode for products
	public Long getFreeProductBarCode() {
		return getFreeBarCode("SELECT barCode FROM products", 100000L);
	}
	
	// Gets a free barcode for users
	public Long getFreeUserBarCode() {
		return getFreeBarCode("SELECT barCode FROM users", 1000L);
	}
	
	// Gets the next free barcode value over start value
	public Long getFreeBarCode(String sql, Long start) {
		try (Connection conn = this.connect(); PreparedStatement pstmt = conn.prepareStatement(sql);
			ResultSet rs = pstmt.executeQuery();) {
			Long highest = start;
			while (rs.next()) {
				Long temp = rs.getLong("barCode");
				 if (temp > highest) highest = temp;
				 
			}
			return (highest + 1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1L;
	}

	// Gets a free product id, unused atm
	public String getFreeProductID() {
		return "p" + getFreeID("products");
	}
	
	// Gets a free user id
	private String getFreeUserID() {
		return "u" + getFreeID("users");
	}
	
	// Gets the next free id number, to be used for new product or user
	public int getFreeID(String table) {
		String sql = "SELECT id FROM " + table;
		try (Connection conn = this.connect(); PreparedStatement pstmt = conn.prepareStatement(sql);
			ResultSet rs = pstmt.executeQuery();) {
			int highest = 0;
			while (rs.next()) {
				 String temp = rs.getString("id");
				 temp = temp.substring(1, temp.length());
				 int value;
				 if (NumberFormatChecker.isInteger(temp)) {
					 value = Integer.parseInt(temp);
					 if (value > highest) highest = value;
				 }
			}
			return (highest + 1);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}

//	// Debug method to print all column ids in users
//	public void printAllColumnIDS() {
//		String sql = "SELECT * FROM users";
//		try (Connection conn = this.connect(); PreparedStatement pstmt = conn.prepareStatement(sql);
//			ResultSet rs = pstmt.executeQuery();) {
//			ResultSetMetaData rsmd = rs.getMetaData();
//			int columnCount = rsmd.getColumnCount();
//			for (int i = 1; i <= columnCount; i++) {
//				String columnName = rsmd.getColumnName(i);
//				System.out.println(columnName);
//			}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//	}
	
//	// Debug method to print all settings names
//	public void printAllSettingsNames() {
//		String sql = "SELECT * FROM settings";
//		System.out.println("Starting print of settings");
//		try (Connection conn = this.connect(); PreparedStatement pstmt = conn.prepareStatement(sql);
//			ResultSet rs = pstmt.executeQuery();) {
//			while (rs.next()) {
//				System.out.println(rs.getString("name") + ";" + rs.getString("value"));
//			}
//			System.out.println("Print settings completed");
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//	}

//	// Debug method to print all product names
//	public void printAllProductNames() {
//		String sql = "SELECT name FROM products";
//		try (Connection conn = this.connect(); PreparedStatement pstmt = conn.prepareStatement(sql);
//			ResultSet rs = pstmt.executeQuery();) {
//			while (rs.next()) {
//				System.out.println(rs.getString("name"));
//			}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//	}
	
//	// Debug method to print all user names
//	public void printAllUserNames() {
//		String sql = "SELECT name FROM users";
//		try (Connection conn = this.connect(); PreparedStatement pstmt = conn.prepareStatement(sql);
//				ResultSet rs = pstmt.executeQuery();) {
//			while (rs.next()) {
//				System.out.println(rs.getString("name"));
//			}
//		} catch (SQLException e) {
//			e.printStackTrace();
//		}
//	}
	
	// Prepares column ids and types for deleting product columns from users database
	public String[] getNewColumnIDS(String[] entries) {
		String sql = "SELECT * FROM users";

		try (Connection conn = this.connect(); PreparedStatement pstmt = conn.prepareStatement(sql);
				ResultSet rs = pstmt.executeQuery();) {
			String result1 = "";
			String result2 = "";
			ResultSetMetaData rsmd = rs.getMetaData();
			int columnCount = rsmd.getColumnCount();
			for (int i = 1; i <= columnCount; i++) {
				String columnName = rsmd.getColumnName(i);
				boolean addThis = true;
				for (String s : entries) {
					if (s.equals(columnName)) addThis = false;
				}
				if (addThis) {
					if (!result1.isEmpty())	result1 += ",\n '" + columnName + "'";
					else result1 += "'" + columnName + "'";
					if (!result2.isEmpty())	{
							result2 += ", " + columnName + "";
					}
					else result2 += columnName;
					if (columnName.equals("gender") || columnName.equals("barCode")) {
						result1 += " real";
					} else {
						result1 += " text";
					}
					
				}
			}
			return new String[]{result1,result2};
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;

	}

	// Delete products
	public boolean deleteProducts(String[] IDs) {
		String[] entry = getNewColumnIDS(IDs );
		String sqlDeleteFromProducts = "DELETE FROM products WHERE id = ?";
		String sqlRename = "ALTER TABLE users RENAME TO users_orig";
		String sqlCreateNewTable = "CREATE TABLE IF NOT EXISTS users (\n " + entry[0] + "\n);";
		String sqlCopy = "INSERT INTO users("+ entry[1] +  ") SELECT " + entry[1] + " FROM users_orig";
		String sqlDropOldTable = "DROP TABLE users_orig";
		try (Connection conn = connect(); PreparedStatement pstmtDelete = conn.prepareStatement(sqlDeleteFromProducts);
				){
			conn.setAutoCommit(false);
			
			// Delete from products database
			for (int i = 0; i < IDs.length; i ++) {
				pstmtDelete.setString(1, IDs[i]);
				pstmtDelete.executeUpdate();
			}
			
			// Delete from users database
			PreparedStatement pstmtRename = conn.prepareStatement(sqlRename);
			pstmtRename.executeUpdate();
			pstmtRename.close();
			PreparedStatement pstmtCreateTable = conn.prepareStatement(sqlCreateNewTable);
			pstmtCreateTable.executeUpdate();
			pstmtCreateTable.close();
			PreparedStatement pstmtCopy = conn.prepareStatement(sqlCopy);
			pstmtCopy.executeUpdate();
			pstmtCopy.close();
			PreparedStatement pstmtDrop = conn.prepareStatement(sqlDropOldTable);
			pstmtDrop.executeUpdate();
			pstmtDrop.close();
			
			conn.commit();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		} 
	}

	// Creates connection to database, used everywhere in this class
	private Connection connect() {
		String url = "jdbc:sqlite:" + GlobalVariables.databasePath;
		Connection conn = null;
		try {
			conn = DriverManager.getConnection(url);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return conn;
	}
	
	// Retrieves settings
	public ArrayList<String> loadSettings() {
		String sql = "SELECT * FROM settings";
		ArrayList<String> result = new ArrayList<String>();
		try (Connection conn = this.connect();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql)) {
			while(rs.next()) {
				result.add(rs.getString("name"));
				result.add(rs.getString("value"));
			}
		
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	// Retrieves all users
	public ArrayList<User> selectAllUsers() {
		String sql = "SELECT * FROM users";
		ArrayList<User> result = new ArrayList<User>();
		try (Connection conn = this.connect();
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(sql)) {

			ResultSetMetaData rsmd = rs.getMetaData();
			int columnCount = rsmd.getColumnCount();
			ArrayList<Double> userProductAmount = new ArrayList<Double>();
			ArrayList<String> productIDs = new ArrayList<String>();
			ArrayList<String> productNames = new ArrayList<String>();

			// Retrieve user products
			for (int i = 1; i <= columnCount; i++) {
				String columnName = rsmd.getColumnName(i);
				if (columnName.substring(0, 1).equals("p")) {
					productIDs.add(columnName);
					userProductAmount.add(0.0);
					String name = dbHandler.getProductNameFromID(columnName);
					if (name == null) {
						//TODO - Handle unknow product exception
						return null;
					} else {
						productNames.add(name);
					}
				}
			}
			
			// Retrieve users
			while (rs.next()) {
				ArrayList<UserProduct> userProducts = new ArrayList<UserProduct>();
				for (int i = 0; i < productIDs.size(); i++) {
					
						double amount = rs.getDouble(productIDs.get(i));
						
							userProductAmount.set(i, userProductAmount.get(i) + amount);
						
						userProducts.add(new UserProduct(productIDs.get(i), productNames.get(i), amount));
				
				}

				result.add(new User(rs.getString("name"), rs.getString("callingName"), rs.getString("id"),
						rs.getString("rank"), rs.getString("team"), rs.getString("study"), rs.getString("studyNumber"), rs.getString("rustur"),
						rs.getInt("gender"), rs.getLong("barCode"), userProducts));
			}
			
			// Update how many products sold
			dbHandler.updateSoldProducts(productIDs, userProductAmount);

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	// Retrieves all column names in users database
	public ArrayList<String> getColumnNames() {
		String sql = "SELECT * FROM users";
		ArrayList<String> result = new ArrayList<String>();
		try (Connection conn = this.connect(); PreparedStatement pstmt = conn.prepareStatement(sql)) {
			ResultSet rs = pstmt.executeQuery();
			ResultSetMetaData rsmd = rs.getMetaData();
			int columnCount = rsmd.getColumnCount();
			for (int i = 1; i <= columnCount; i++) {
				result.add(rsmd.getColumnName(i));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	// Retrieving all products
	public ArrayList<Product> selectAllProducts() {
		String sql = "SELECT * FROM products";
		ArrayList<Product> result = new ArrayList<Product>();
		try (Connection conn = this.connect();
				Statement stmt = conn.createStatement();
				ResultSet rs = stmt.executeQuery(sql)) {

			while (rs.next()) {
				result.add(new Product(rs.getString("id"), rs.getString("name"), rs.getDouble("totalStock"),
						rs.getDouble("currentStock"), rs.getDouble("sold"), rs.getString("category"),
						rs.getDouble("price"), rs.getLong("barCode")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}

	// Changing setting entry
	public boolean editSettings(String row, String value) {
		String sql = "UPDATE settings SET value = ? Where name = ?";
		
		try (
			Connection conn = connect();
			PreparedStatement stmt = conn.prepareStatement(sql);){
			stmt.setString(1, value);
			stmt.setString(2, row);
			stmt.executeUpdate();
				
			return true;
		} catch (Exception e) {
			System.err.print(e.getClass().getName() + ": " + e.getMessage());
			return false;
		}
	}
	
	// Changing setting entry
	public boolean changeUserBarCodes(ArrayList<Long> difference) {
		String sql = "UPDATE users SET barCode = ? Where barCode = ?";
		return changeBarCodesChild(difference, sql);
	}
	public boolean changeProductBarCodes(ArrayList<Long> difference) {
		String sql = "UPDATE products SET barCode = ? Where barCode = ?";
		return changeBarCodesChild(difference, sql);
	}
	public boolean changeBarCodesChild(ArrayList<Long> difference, String sql) {
		try (Connection conn = connect();
				PreparedStatement stmt = conn.prepareStatement(sql);){
				for (int i = 0; i < difference.size(); i += 2) {
					stmt.setLong(1, difference.get(i + 1));
					stmt.setLong(2, difference.get(i));
					stmt.executeUpdate();
				}
					
				return true;
			} catch (Exception e) {
				System.err.print(e.getClass().getName() + ": " + e.getMessage());
				return false;
			}
	}
	
	// Edit users or products
	public boolean edits(String table, ArrayList<String> difference, ArrayList<String> ids) {
		String[] sql = new String[difference.size() / 2];
		for (int i = 0; i < difference.size(); i += 2) {
			sql[i / 2] = "UPDATE " + table + " SET " + "'" + difference.get(i) + "'" + " = ? Where id = ?";
		}
		try (Connection conn = connect();) {
			PreparedStatement[] stmt = new PreparedStatement[sql.length];
			conn.setAutoCommit(false);
			for (String id : ids) {
				for (int i = 0; i < sql.length; i ++) {
					stmt[i] = conn.prepareStatement(sql[i]);
					stmt[i].setString(1, difference.get(i * 2 + 1));
					stmt[i].setString(2, id);
					stmt[i].executeUpdate();
				}
			}
			for (PreparedStatement ps : stmt) {
				ps.close();
			}
			conn.commit();
				
			return true;
		} catch (Exception e) {
			System.err.print(e.getClass().getName() + ": " + e.getMessage());
			return false;
		}
	}

	// Insert products
	public boolean insertProducts(ArrayList<Product> products) {
		int newID = getFreeID("products");
		Long newBarCode = getFreeProductBarCode();
		
		// Validate barcode
		for (Product p : products) {
			p.setID("p" + newID);
			newID ++;
			if (p.getBarCode() <= 0) {
				p.setBarCode(newBarCode);
				newBarCode ++;
			}
		}
		
		// Inserting into products
		String sql1 = "INSERT INTO products (id, name, totalStock, currentStock, sold, category, price, barCode) VALUES(?,?,?,?,?,?,?,?)";
		try (Connection conn = connect(); PreparedStatement pstmt = conn.prepareStatement(sql1);) {
			conn.setAutoCommit(false);
			
			for (int i = 0; i < products.size(); i ++) {
				pstmt.setString(1, products.get(i).getID());
				pstmt.setString(2, products.get(i).getName());
				pstmt.setDouble(3, products.get(i).getTotalStock());
				pstmt.setDouble(4, products.get(i).getCurrentStock());
				pstmt.setDouble(5, products.get(i).getSold());
				pstmt.setString(6, products.get(i).getCategory());
				pstmt.setDouble(7, products.get(i).getPrice());
				pstmt.setLong(8, products.get(i).getBarCode());
				pstmt.executeUpdate();
			}
			// Inserting into users as userproduct
			PreparedStatement pstmt2[] = new PreparedStatement[products.size()];
			for (int i = 0; i < pstmt2.length; i ++) {
				String sql2 = "ALTER TABLE users ADD COLUMN " + "'" + products.get(i).getID() + "'" + " REAL DEFAULT 0";
				pstmt2[i] = conn.prepareStatement(sql2);
				pstmt2[i].executeUpdate();
				pstmt2[i].close();
			}
			
			conn.commit();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

	}

	// Importing users
	public boolean insertUsers(ArrayList<User> users) {
		String sql = "INSERT INTO users (name, callingName, id, rank, team, study, studyNumber, rustur, gender, barCode) VALUES(?,?,?,?,?,?,?,?,?,?)";
		
		try (Connection conn = this.connect(); PreparedStatement pstmt = conn.prepareStatement(sql);) {
			conn.setAutoCommit(false);
			for (int i = 0; i <users.size(); i ++) {
				pstmt.setString(1, users.get(i).getName());
				pstmt.setString(2, users.get(i).getCallingName());
				pstmt.setString(3, users.get(i).getID());
				pstmt.setString(4, users.get(i).getRank());
				pstmt.setString(5, users.get(i).getTeam());
				pstmt.setString(6, users.get(i).getStudy());				
				pstmt.setString(7, users.get(i).getStudyNumber());
				pstmt.setString(8, users.get(i).getRustur());
				pstmt.setInt(9, users.get(i).getGenderInt());
				pstmt.setLong(10, users.get(i).getBarCode());
				pstmt.executeUpdate();
			}
			conn.commit();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	// Creates new user
	public boolean insertUser(User user) {
		String sql = "INSERT INTO users (name, callingName, id, rank, team, study, studyNumber, rustur, gender, barCode) VALUES(?,?,?,?,?,?,?,?,?,?)";
		if (user.getID().isEmpty()) {
			String newID = getFreeUserID();
			user.setID(newID);
		}
		if (user.getBarCode() <= 0) {
			Long newBarCode = getFreeUserBarCode();
			user.setBarCode(newBarCode);
		}
//		loadUserProducts(user);
		
		try (Connection conn = this.connect(); PreparedStatement pstmt = conn.prepareStatement(sql)) {
			pstmt.setString(1, user.getName());
			pstmt.setString(2, user.getCallingName());
			pstmt.setString(3, user.getID());
			pstmt.setString(4, user.getRank());
			pstmt.setString(5, user.getTeam());
			pstmt.setString(6, user.getStudy());
			pstmt.setString(7, user.getStudyNumber());
			pstmt.setString(8, user.getRustur());
			pstmt.setInt(9, user.getGenderInt());
			pstmt.setLong(10, user.getBarCode());
			pstmt.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	// Change any number of entries in a transaction, all committed at once
	public boolean editMultiple(String table, ArrayList<ArrayList<String>> outer) {
		if (outer.size() <= 0) return true;
		String[] sql = new String[outer.get(0).size() / 2];
		for (int i = 1; i < outer.get(0).size(); i += 2) {
			sql[i / 2] = "UPDATE " + table + " SET " + "'" + outer.get(0).get(i) + "'" + " = ? Where id = ?";
		}
		try {
			Connection conn = connect();
			PreparedStatement[] stmt = new PreparedStatement[sql.length];
			conn.setAutoCommit(false);
			
			for (int i = 0; i < sql.length; i ++) {
				stmt[i] = conn.prepareStatement(sql[i]);
			}
			
			for (ArrayList<String> difference : outer) {
				for (int i = 0; i < sql.length; i ++) {
					stmt[i].setString(1, difference.get(i * 2 + 2));
					stmt[i].setString(2, difference.get(0));
					stmt[i].executeUpdate();
				}
			}
			for (PreparedStatement pstmt : stmt) {
				pstmt.close();
			}
			conn.commit();
				
			return true;
		} catch (Exception e) {
			System.err.print(e.getClass().getName() + ": " + e.getMessage());
			return false;
		}
	}

	// Setting deleted ranks to default rank "Unknown"
	public boolean nullRanks(ArrayList<String> oldRank) {
		String sql = "UPDATE users SET rank = 'Unknown' Where rank = ?";
		return nullCRT(oldRank, sql);
	}

	// Setting deleted teams to default team "Unknown"
	public boolean nullTeams(ArrayList<String> oldTeam) {
		String sql = "UPDATE users SET team = 'Unknown' Where team = ?";
		return nullCRT(oldTeam, sql);
	}

	// Setting deleted categories to default category "Unknown"
	public boolean nullCategories(ArrayList<String> oldCategory) {
		String sql = "UPDATE products SET category = 'Unknown' Where category = ?";
		return nullCRT(oldCategory, sql);
	}

	// Setting deleted CRT to default value "Unknown"
	private boolean nullCRT(ArrayList<String> oldCategory, String sql) {
		try (Connection conn = connect(); PreparedStatement pstmt = conn.prepareStatement(sql);) {
			conn.setAutoCommit(false);
			
			for (int i = 0; i < oldCategory.size(); i++) {
				pstmt.setString(1, oldCategory.get(i));
				pstmt.executeUpdate();
			}
			conn.commit();
			return true;
		} catch (SQLException e) {
			System.err.print(e.getClass().getName() + ": " + e.getMessage());
			return false;
		}
	}
	
	// Update category entry in settings
	public boolean updateCategories(String oldCategory, String newCategory) {
		String sql = "UPDATE products SET category = ? Where category = ?";
		return updateCRT(oldCategory, newCategory, sql);
	}

	// Update ranks entry in settings
	public boolean updateRanks(String oldRank, String newRank) {
		String sql = "UPDATE users SET rank = ? Where rank = ?";
		return updateCRT(oldRank, newRank, sql);
	}

	// Update teams entry in settings
	public boolean updateTeams(String oldTeam, String newTeam) {
		String sql = "UPDATE users SET team = ? Where team = ?";
		return updateCRT(oldTeam, newTeam, sql);
	}

	// Update CRT entry in settings
	private boolean updateCRT(String oldO, String newO, String sql) {
		try (Connection conn = connect(); PreparedStatement pstmt = conn.prepareStatement(sql)) {
			pstmt.setString(1, newO);
			pstmt.setString(2, oldO);
			pstmt.executeUpdate();
			return true;
		} catch (SQLException e) {
			System.err.print(e.getClass().getName() + ": " + e.getMessage());
			return false;
		}
	}
}
