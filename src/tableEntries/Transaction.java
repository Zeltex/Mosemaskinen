package tableEntries;

import importedCode.NumberFormatChecker;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;

public class Transaction {
	   private final SimpleStringProperty userName = new SimpleStringProperty("");
	   private final SimpleStringProperty productName = new SimpleStringProperty("");
	   private final SimpleDoubleProperty amount = new SimpleDoubleProperty(0.0);
	   private final SimpleBooleanProperty absolute = new SimpleBooleanProperty(false);
	   private final SimpleStringProperty productID = new SimpleStringProperty("");
	   private final SimpleStringProperty userID = new SimpleStringProperty("");
	   private final SimpleBooleanProperty deprecated = new SimpleBooleanProperty(false);
	   private final SimpleStringProperty date = new SimpleStringProperty("");
	   private final SimpleStringProperty time = new SimpleStringProperty("");
	   
	   public Transaction(String userName, String productName, double amount, boolean absolute, 
			   String productID, String userID, boolean deprecated, String date, String time) {
		   this.userName.set(userName);
		   this.productName.set(productName);
		   this.amount.set(amount);
		   this.absolute.set(absolute);
		   this.productID.set(productID);
		   this.userID.set(userID);
		   this.deprecated.set(deprecated);
		   this.date.set(date);
		   this.time.set(time);
	   }
	   public Transaction(String[] strings) {
		   userName.set(strings[0]);
		   productName.set(strings[1]);
		   amount.set((NumberFormatChecker.isDouble(strings[2]) ? Double.valueOf(strings[2]) : 0.0));
		   absolute.set(strings[3].equals("true"));
		   productID.set(strings[4]);
		   userID.set(strings[5]);
		   deprecated.set(strings[6].equals("true"));
		   date.set(strings[7]);
		   time.set(strings[8]);
	   }
	   public String getUserName() {
		   return userName.get();
	   }
	   public String getProductName() {
		   return productName.get();
	   }
	   public String getAmount() {
//		   return amount.get();
		   return String.format("%.2f", amount.get()).replace(',','.');
	   }
	   public boolean getAbsolute() {
		   return absolute.get();
	   }
	   public String getProductID() {
		   return productID.get();
	   }
	   public String getUserID() {
		   return userID.get();
	   }
	   public boolean getDeprecated() {
		   return deprecated.get();
	   }
	   public String getDate() {
		   return date.get();
	   }
	   public String getTime() {
		   return time.get();
	   }
	   public String toString(String separator) {
		   return userName.get() + separator + productName.get() + separator +  amount.get() + separator + absolute.get() + 
				   separator + productID.get() + separator + userID.get() + separator + deprecated.get() + separator + 
				   date.get() + separator + time.get();
	   }
}
