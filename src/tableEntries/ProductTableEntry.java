package tableEntries;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;

public class ProductTableEntry {
	 private final SimpleStringProperty name = new SimpleStringProperty("");
	   private final SimpleDoubleProperty totalStock = new SimpleDoubleProperty(0.0);
	   private final SimpleDoubleProperty currentStock = new SimpleDoubleProperty(0.0);
	   private final SimpleDoubleProperty sold = new SimpleDoubleProperty(0.0);
	   private final SimpleDoubleProperty waste = new SimpleDoubleProperty(0.0);
	   private final SimpleDoubleProperty price = new SimpleDoubleProperty(0.0);
	   public ProductTableEntry(String name, double totalStock, double currentStock, double sold, double waste, double price) {
		   this.name.set(name);
		   this.totalStock.set(totalStock);
		   this.currentStock.set(currentStock);
		   this.sold.set(sold);
		   this.waste.set(waste);
		   this.price.set(price);
	   }
	   public String getName() {
		   return name.get();
	   }
	   public String getTotalStock() { 
		   return String.format("%.2f", totalStock.get());
	   }
	   public String getCurrentStock() { 
		   return String.format("%.2f", currentStock.get());
	   }
	   public String getSold() { 
		   return String.format("%.2f", sold.get());
	   }
	   public String getWaste() { 
		   return String.format("%.2f", waste.get());
	   }
	   public String getPrice() { 
		   return String.format("%.2f", price.get());
	   }
}
