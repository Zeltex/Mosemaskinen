package tableEntries;

import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

public class UserTableEntry {

	   private final SimpleStringProperty id = new SimpleStringProperty("");
	   private final SimpleStringProperty name = new SimpleStringProperty("");
	   private final SimpleDoubleProperty amount = new SimpleDoubleProperty(0.0);
	   private final SimpleDoubleProperty price = new SimpleDoubleProperty(0.0);
	   private final SimpleDoubleProperty rounded = new SimpleDoubleProperty(0);
	   public UserTableEntry(String name, String id, double amount, double price) {
		   this.id.set(id);
		   this.name.set(name);
		   this.amount.set(amount);
		   this.price.set(price);
	   }
	   public String getName() {
		   return name.get();
	   } 
	   public String getId() {
		   return id.get();
	   }
	   public String getAmount() {
		   return String.format("%.2f", amount.get());
	   }
	   public String getPrice() {
		   return String.format("%.2f", price.get());
	   }
	   public void setPrice(double price) {
		   this.price.set(price);
	   }
	   public String getRounded() {
		   return rounded.get() + "";
	   }
	   public void setRounded(double price) {
		   rounded.set(price);
	   }
}
