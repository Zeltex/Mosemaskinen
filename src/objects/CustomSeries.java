package objects;

import java.util.ArrayList;

public class CustomSeries {
	public ArrayList<Double> data;
	ArrayList<String> categories;
	String name;
	
	public CustomSeries() {
		data = new ArrayList<Double>();
		categories = new ArrayList<String>();
		name = "";
	}
	
	public void add(String s, double d) {
		data.add(d);
		categories.add(s);
	}
	public void setName(String s) { 
		name = s;
	}
	public int getSize() {
		return data.size();
	}
	public double get(int i) {
		return data.get(i);
	}
	public String getName() {
		return name;
	}
	public String getIndexName(int i) {
		return categories.get(i);
	}
}
