package objects;

import java.util.ArrayList;

public class CustomList {
	ArrayList<String> strings;
	ArrayList<Double> doubles;
	
	final static int STRING = 0, DOUBLE = 1; 
	
	ArrayList<Integer> codes;
	
	public CustomList() {
		strings = new ArrayList<String>();
		doubles = new ArrayList<Double>();
	}
	
	public void add(String s) {
		strings.add(s);
		codes.add(STRING);
	}
	public void add(double d) {
		doubles.add(d);
		codes.add(DOUBLE);
	}
	
	public String get(int i) {
		return "";
	}
//	public double get(int i) {
//		return double;
//	}
}
