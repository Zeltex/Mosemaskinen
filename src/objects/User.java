package objects;

import java.util.ArrayList;

import database.GlobalVariables;
import importedCode.NumberFormatChecker;
import managers.DialogManager;

public class User {
	private String name, callingName, ID, rank, team, study, studyNumber, rustur;
	private ArrayList<UserProduct> products;
	double totalItems;
	int gender;
	Long barCode;
	String whatToShow;
	
	public User(String name, String callingName, String ID, String rank, 
			String team, String study, String studyNumber, String rustur, int gender, Long barCode, ArrayList<UserProduct> products) {
		this.name = name;
		this.callingName = callingName;
		this.ID = ID;
		this.rank = rank;
		this.team = team;
		this.study = study;
		this.studyNumber = studyNumber;
		this.rustur = rustur;
		this.gender = gender;
		this.barCode = barCode;
		whatToShow = name;
		
		if (products == null) products = new ArrayList<UserProduct>();
		else if (products.size() == 0) products = new ArrayList<UserProduct>();
		else {
			this.products = products;
			calculateTotal();
		}
	}
	
	public double[] getUserProductAmounts() {
		double[] d = new double[products.size()];
		for (int i = 0; i < d.length; i ++) {
			d[i] = products.get(i).getAmount();
		}
		return d;
	}
	
	public void setWhatToShow(String whatToShow) {
		this.whatToShow = whatToShow;
	}
	
	public String getWhatToShow() {
		return whatToShow;
	}
	@Override
	public String toString() {
		System.out.println("toString called: " + whatToShow);
		return whatToShow;
	}
	
	private void calculateTotal() {
		totalItems = 0;
		for (UserProduct userProduct : products) {
			totalItems += userProduct.getAmount();
			
		}
	}
	
	private void calculateFavorite() {
		
	}
	
	public void update(ArrayList<String> difference) {
		for (int i = 0; i < difference.size(); i += 2) {
			switch(difference.get(i)) {
			case "name": setName(difference.get(i + 1)); break;
			case "callingName": setCallingName(difference.get(i + 1)); break;
			case "rank": setRank(difference.get(i + 1)); break;
			case "team": setTeam(difference.get(i + 1)); break;
			case "study": setStudy(difference.get(i + 1)); break;
			case "studyNumber": setStudyNumber(difference.get(i + 1)); break;
			case "rustur": setRustur(difference.get(i + 1)); break;
			case "gender": if (NumberFormatChecker.isInteger(difference.get(i + 1))) {
				setGender(Integer.parseInt(difference.get(i + 1)));
			} break;
			
			
			case "barCode": setBarCode(difference.get(i + 1)); break;
			default: {
				if (products != null) {
					String compare = difference.get(i).split("\\;")[0];
					for (int j = 0; j < products.size(); j ++) {
						if (products.get(j).getProductID().equals(compare)) {
							if (NumberFormatChecker.isDouble(difference.get(i + 1))) {
								products.get(j).setAmount(Double.parseDouble(difference.get(i + 1)));
								calculateTotal();
								calculateFavorite();
							} else {
								DialogManager.alertError("Failed to parse double", null, "An input didn't have the correct number format");
							}
							
						}
					}
				}
			}
			}
		}
	}
	
	public static int confirmGender(String gender) {
		if (gender.equals(GlobalVariables.genderName(0))) return 0;
		else if (gender.equals(GlobalVariables.genderName(1))) return 1;
		else if (gender.equals(GlobalVariables.genderName(2))) return 2;
		else return -1;
	}
	
	public ArrayList<String> compare(String name, String callingName, 
			String rank, String team, String study, String studyNumber, String rustur, int gender, Long barCode) {
		ArrayList<String> result = new ArrayList<String>();
		if (!name.equals(this.name)) {result.add("name");result.add(name);}
		if (!callingName.equals(this.callingName)) {result.add("callingName");result.add(callingName);}
		if (!rank.equals(this.rank)) {result.add("rank");result.add(rank);}
		if (GlobalVariables.debugMode) System.out.println("Chosen user, current team " + this.team + ", new team " + team);
		if (!team.equals(this.team)) {result.add("team");result.add(team);}
		if (!study.equals(this.study)) {result.add("study");result.add(study);}
		if (!studyNumber.equals(this.studyNumber)) {result.add("studyNumber");result.add(studyNumber);}
		if (!rustur.equals(this.rustur)) {result.add("rustur");result.add(rustur);}
		if (gender != this.gender) {result.add("gender"); result.add(gender + "");}
		if (barCode != this.barCode) {result.add("barCode"); result.add(barCode + "");}
		return result;
	}
	
	public ArrayList<String> compareProducts(ArrayList<Double> compareStrings) {
		ArrayList<String> result = new ArrayList<String>();
		for (int i = 0; i < products.size(); i ++) {
			if (compareStrings.size() > i) {
				if (products.get(i).getAmount() != compareStrings.get(i)) {
					result.add(products.get(i).getProductID());
					result.add(compareStrings.get(i) + "");
				}
			}
		}
		return result;
	}
	
//	public double getProductFromID(String ID) throws ProductsWithSameIDException, ProductNotFoundException{
//		double result = -1;
//		for (UserProduct up : products) {
//			if (up.getProductID().equals(ID)) {
//				if (result == -1) result = up.getAmount();
//				else throw new ProductsWithSameIDException();
//			}
//		}
//		if (result == -1) throw new ProductNotFoundException();
//		else return result;
//	}
	
	public UserProduct getProductFromID(String ID) {
		for (UserProduct up : products) {
			if (up.getProductID().equals(ID)) return up;
		}
		return null;
	}
	
	public String getFavoriteItem() {
		
		double highest = 0;
		String highestName = "Unknown";
		if (products != null) {
		for (UserProduct userProduct : products) {
			if (userProduct.getAmount() > highest) {
				highest = userProduct.getAmount();
				highestName = userProduct.getProductName();
			}
		}
		}
		return highestName;
	}
	
	public double getCategoryItems(ArrayList<String> ids) {
		double amount = 0.0;
		for (UserProduct p : products) {
			if (ids.contains(p.getProductID())) {
				amount += p.getAmount();
			}
		}
		return amount;
	}
	
	public double getTotalItems() {return totalItems;}
	public Long getBarCode() {return barCode;}
	public int getGenderInt() {return gender;}
	
	public String getName() {return name;}
	public String getCallingName() {return callingName;}
	public String getID() {return ID;}
	public String getRank() {return rank;}
	public String getTeam() {return team;}
	public String getStudy() {return study == null ? "" : study;}
	public String getStudyNumber() {return studyNumber;}
	public String getRustur() {return rustur;}
	public String getGender() {return GlobalVariables.genderName(gender);}
	public ArrayList<UserProduct> getUserProducts() {
		if (products == null) products = new ArrayList<UserProduct>();
		return products;
		}
	
	public void setProducts(ArrayList<UserProduct> products) {this.products = products;}
	public void setID(String ID) {this.ID = ID;}
	public void setName(String name) {this.name = name;}
	public void setCallingName(String callingName) {this.callingName = callingName;}
	public void setRank(String rank) {this.rank = rank;}
	public void setTeam(String team) {this.team = team;}
	public void setStudy(String study) {this.study = study;}
	public void setStudyNumber(String studyNumber) {this.studyNumber = studyNumber;}
	public void setRustur(String rustur) {this.rustur = rustur;}
	public void setGender(int gender) {this.gender = gender;}
	public void setBarCode(Long barCode) {this.barCode = barCode;}
	public void setBarCode(String barCode) {if (NumberFormatChecker.isInteger(barCode)) {
		this.barCode = Long.valueOf(barCode);
	} else {
		DialogManager.alertError("Number format exception", null, "Tried to save a bar code that wasn't integer.");
	}
	}
}
