package objects;

import java.util.ArrayList;

public class Product {
	private String name, category, ID, whatToShow;
	private double totalStock, currentStock, sold, price;
	private Long barCode;
	
	public Product(String ID, String name, double totalStock, double currentStock, 
			double sold, String category, double price, Long barCode) {
		this.name = name;
		this.totalStock = totalStock;
		this.currentStock = currentStock;
		this.sold = sold;
		this.category = category;
		this.ID = ID;
		this.price = price;
		this.barCode = barCode;
		whatToShow = name;
		
	}
	
	public Product(String whatToShow) {
		this.whatToShow = whatToShow;
		ID = "";
	}
	
	public String getWhatToShow() {
		return whatToShow;
	}
	
	public void setWhatToShow(String whatToShow) {
		this.whatToShow = whatToShow;
	}
	
	@Override
	public String toString() {
		return whatToShow;
	}
	
	public void update(ArrayList<String> difference) {
		for (int i = 0; i < difference.size(); i += 2) {
			switch(difference.get(i)) {
			case "name": setName(difference.get(i + 1)); break;
			case "totalStock": setTotalStock(Double.parseDouble(difference.get(i + 1))); break;
			case "currentStock": setCurrentStock(Double.parseDouble(difference.get(i + 1))); break;
			case "sold": setSold(Double.parseDouble(difference.get(i + 1))); break;
			case "category": setCategory(difference.get(i + 1)); break;
			case "price": setPrice(Double.parseDouble(difference.get(i + 1))); break;
			case "barCode": setBarCode(Long.parseLong(difference.get(i + 1))); break;
			default: System.out.println("Tried to set unknown attribute");
			}
		}
	}
	
	public ArrayList<String> compare(String name, String totalStock, String currentStock, 
//			String sold, 
			String category, String price, String barCode) {
		ArrayList<String> result = new ArrayList<String>();
		if (!name.equals(this.name)) {result.add("name");result.add(name);}
		if (Double.parseDouble(totalStock) != this.totalStock) {result.add("totalStock");result.add(totalStock);}
		if (Double.parseDouble(currentStock) != this.currentStock) {result.add("currentStock");result.add(currentStock);}
//		if (Double.parseDouble(sold) != this.sold) {result.add("sold");result.add(sold);}
		if (!category.equals(this.category)) {result.add("category");result.add(category);}
		if (Double.parseDouble(price) != this.price) {result.add("price");result.add(price);}
		if (Long.parseLong(barCode) != this.barCode) {result.add("barCode"); result.add(barCode);}
		return result;
	}
	
	public double getTotalStock() {return totalStock;}
	public double getCurrentStock() {return currentStock;}
	public double getSold() {return sold;}
	public String getName() {return name;}
	public String getCategory() {return category;}
	public String getID() {return ID;}
	public double getPrice() {return price;}
	public Long getBarCode() {return barCode;}
	
	public String[] getDisplayInfo() {
		return new String[]{ID, name, totalStock + "", currentStock + "", sold + "", category, price + "", barCode + ""};
	}
	
	
	public void setTotalStock(double d) {totalStock = d;}
	public void addSold(double d) {sold += d;}
	public void setSold(double d) {sold = d;}
	public void setCurrentStock(double d) {currentStock = d;}
	public void setCategory(String s) {category = s;} 
	public void setID(String s) {ID = s;}
	public void setPrice(double p) {price = p;}
	public void setName(String n) {name = n;}
	public void setBarCode(Long b) {barCode = b;}
	
}
