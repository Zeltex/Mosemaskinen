package objects;

public class UserProduct {
	private String productID;
	private double amount;
	private String productName;
	
	public UserProduct(String productID, String productName, double amount) {
		this.productID = productID;
		this.amount = amount;
		this.productName = productName;
	}
	
	public String getProductID() {return productID;}
	public double getAmount() {return amount;}
	public String getProductName() {return productName;}
	
	public void setName(String newName) {productName = newName;}
	public void addAmount(double amount) {
		this.amount += amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getShoppingCartFormat() {
		return (int)amount + " X " + productName;
	}
}
