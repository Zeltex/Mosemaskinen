package objects;

import java.util.ArrayList;

public class UserShell {
	String ID;
	ArrayList<UserProduct> products;
	
	public UserShell(User user) {
		products = new ArrayList<UserProduct>();
		for (UserProduct up : user.getUserProducts()) {
			products.add(new UserProduct(up.getProductID(),up.getProductName(), 0));
		}
		ID = user.getID();
	}
	
	public void addTransaction(String id, double amount, boolean absolute) {
		for (UserProduct up : products) {
			if (up.getProductID().equals(id)) {
				if (absolute) {
					up.setAmount(amount);
				} else {
					up.addAmount(amount);
				}
			}
		}
	}
	public String getID() {
		return ID;
	}
	public boolean compare(ArrayList<UserProduct> userProducts) {
		int index = 0;
		for (UserProduct up : userProducts) {
			if (up.getAmount() != products.get(index).getAmount()) {
				return false;
			}
			index ++;
		}
		return true;
	}
}
