Economy

When entering the economy tab you'll be met be two tables, one containing information about the products, and one about the users. 
In the products table there are the usual variables plus on called waste, which is calculated by (total - (current + sold)) i.e. missing items. 
In the users table you can see the amount of items bought, and how much the person should pay. 

Handling waste:
You can choose which team/rank should pay for waste, or you can just mark them all, and have all pay. The waste can be distributed evenly or percentwise. If evenly is chosen, there will be added a flat amount to all the chosen users. If percentwise is chosen, all users will pay relative to what they have bought.
It is recommended to use the "Evenly" option, since it is not abnormal that one item has a lot of waste while another has been scanned too many times. So the "Evenly" options makes it more fair.

Handling payment: 
Tree numbers are shown; "To be collected" which is the sum of what everyone should pay. "Admin payed" which is calculated by the sum of ((total - current) * price) for each item. If handle waste was used, this number should be the same as to be collected.
There are also two options which can be used. The first rounds the amount to be collected, so that payment will be easier. The second will increase the amount people have to pay untill a given excess is reached. This can be used if extra money is needed to cover damages or unexpected charges. Both of these options will only be applied to the chosen users.

Export payment:
When you have handled the waste and payment, the final amount can be exported to a .csv file where you can see how much each person owes.