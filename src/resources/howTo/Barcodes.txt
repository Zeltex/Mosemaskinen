Exporting barcodes

By now you should have setup users and products. The option to export barcodes found under Settings -> Export barcodes, is used to generate sheets with all the necessary barcodes.

There are 4 different kinds of barcodes that will be printed:
1. User barcodes; They are the ones the users will use to login to their account in the system and will have 4 digits (1001 - 9999).

2. Product barcodes; They are the ones the users should scan to start a purchase after they have logged in to their account and will have atleast 6 digits (100001 - x).

3. Multiplier barcodes; They are the ones thes users should use to let the system know how many of an item they want to purchase.

4. Guest barcodes; These are generic user barcodes which can be given to users who lose their own barcode. They will also have 4 digits, and will be the smallest barcodes larger than the largest user barcode currently registered. e.g. if 10 guest barcodes are printed, and the largest user barcode is 1045, they will be 1046 - 1055.

During the export you can choose what should be printed on each type of barcode, and what size they should be. Unless you want something special, just use the default values. 
It should be noted that by default the "First & last name" option is used, so that middle names are ignored. If you want all names to be printed, use the "Name" option.
It should also be noted that the multipliers are setup so half and full cases can be scanned, and that scanning a product code again, will add one to the current number scanned, so that any number can be achieved. Also the 0 multiplier will cancel a product.
The user and guest barcodes are printed in a grid for easy cutting. The multipliers and products are printed in a vertical list with one product or multiplier per row.