Edit registered teams. Teams can also be added from the users tab. If a rank is renamed, all users registered with that rank will be updated.

Press 'Esc' to close