package controllerAssistants;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Pattern;

import database.GlobalVariables;
import importedCode.NumberFormatChecker;
import importedCode.VetoListener;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.control.ToolBar;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.util.converter.DoubleStringConverter;
import managers.DialogManager;
import objects.Product;
import objects.User;

public class SettingsTabAssistant {
	public void setButton(Button b, String imagePath, int width, ToolBar toolBar) {
		ImageView iw = new ImageView(new Image(this.getClass().getResourceAsStream(imagePath)));
		iw.setFitWidth(width);
		iw.setPreserveRatio(true);
		b.setGraphic(iw);
		b.getStyleClass().add("buttonOption");
		if (toolBar != null) toolBar.getItems().add(b);
	}
	
	public void setupAdvancedSettings(GridPane changeSettingsPane, Button sync, Button reset) {
		sync.setMinWidth(250);
		reset.setMinWidth(250);
		String tooltip = "Validate project synchronization: Rebuilds database from transaction history, to confirm "
				+ "the integrity of the database.\n"
				+ "Reset database: Clears everything in the database; settings, users, products, transactions..."
				+ "\n\nPress 'Esc' to close";
		changeSettingsPane.add(DialogManager.createTooltip(tooltip), 0, 0);
		changeSettingsPane.add(sync, 0, 1);
		changeSettingsPane.add(reset, 0, 2);
	}
	
	//Setting the logic that prevents duplicate choices in comboboxes for import/export
		public void preventDuplicateOptionChosen(ComboBox<String> c, ArrayList<ComboBox<String>> comboOptions) {
			c.getSelectionModel().selectedItemProperty().addListener(new VetoListener<String>(c.getSelectionModel()) {

				@Override
				protected boolean isInvalidChange(String oldValue, String newValue) {
					if (!newValue.toLowerCase().equals("none")) {
						int count = 0;
						for (int i = 0; i < comboOptions.size(); i++) {
							if (comboOptions.get(i).getValue() == null)
								continue;
							if (comboOptions.get(i).getValue().equals(newValue)) {
								count++;
							}
						}
						if (count > 1) {
							DialogManager.alertWarning("Non-unique selection", null,
									"Only one value can be loaded in to a variable. " + "Choice wasn't unique.");
							return true;
						}
					}
					return false;
				}
			});
		}

	
	// Backing up database by copypasting it
	public void backupDatabase() {
		File directory = new File(GlobalVariables.backupLocation);
		if (directory.exists() && directory.isDirectory()) {
			try (BufferedInputStream bs = new BufferedInputStream(new FileInputStream(GlobalVariables.databasePath));
					BufferedOutputStream bos = new BufferedOutputStream(
							new FileOutputStream(GlobalVariables.backupLocation + "\\Mosemaskinen" + getPlainDate() + ".db"));) {
				byte[] b = new byte[1024];
				int length;
				while ((length = bs.read(b)) > 0) {
					bos.write(b, 0, length);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			DialogManager.alertError("Backup error", null,
					"Failed to backup database. Confirm chosen directory exists:\n" + GlobalVariables.backupLocation
							+ "\n" + "Or else choose a new location for backup or disable backup (Set timer to 0)");
		}
	}
	
	//Returns current date, used to name new files
		public String getPlainDate() {
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmm");
			Date date = new Date();
			return dateFormat.format(date);
		}

	//Input listener that ensures field to contain only double values
	public void setDoubleRestriction(TextField t) {
		Pattern validDoubleText = Pattern.compile("-?((\\d*)|(\\d+\\.\\d*))");

		TextFormatter<Double> textFormatter = new TextFormatter<Double>(new DoubleStringConverter(), 0.0, change -> {
			String newText = change.getControlNewText();
			if (validDoubleText.matcher(newText).matches()) {
				return change;
			} else
				return null;
		});
		t.setTextFormatter(textFormatter);
	}
	
	//Input listener that ensure field to contain only integer values
	public void setIntegerRestriction(TextField t) {
		t.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d*")) {
					t.setText(newValue.replaceAll("[^\\d]", ""));
				}
			}
		});
	}
	
	//Detects which of the default separators occur most in a line, 
	//therefore identifying the wanted separator
	public String getSeparator(String line) {
		String separator = ";";
		int[] count = new int[4];
		char[] separators = new char[] { ';', ':', ',', '.' };
		for (int i = 0; i < line.length(); i++) {
			switch (line.charAt(i)) {
			case ';':
				count[0]++;
				break;
			case ':':
				count[1]++;
				break;
			case ',':
				count[2]++;
				break;
			case '.':
				count[3]++;
				break;
			}
		}
		int highest = -1;
		for (int j = 0; j < count.length; j++) {
			if (count[j] > highest) {
				highest = count[j];
				separator = separators[j] + "";
			}
		}
		return separator;
	}
	
	//Translates a product from csv format to java object
	public Product readNextProduct(String[] entries, String[] importValues) {
		String name = "", totalStock = "", currentStock = "", category = "", price = "", barCode = "";
		for (int i = 0; i < entries.length; i++) {
			if (importValues.length > i) {
			switch (importValues[i]) {
			case "name":
				name = entries[i];
				break;
			case "total stock":
				totalStock = entries[i];
				break;
			case "current stock": currentStock = entries[i];
				break;
			case "category":
				category = entries[i];
				break;
			case "price":
				price = entries[i];
				break;
			case "bar code":
				barCode = entries[i];
				break;
			}
			}
		}

		if (!NumberFormatChecker.isDouble(totalStock) || totalStock.isEmpty()) totalStock = "0.0";
		if (!NumberFormatChecker.isDouble(currentStock) || currentStock.isEmpty()) currentStock = "0.0";
		if (!NumberFormatChecker.isDouble(price) || price.isEmpty()) price = "0.0";
		if (!NumberFormatChecker.isInteger(barCode) || barCode.isEmpty()) barCode = "0";
		
		return new Product("", name, Double.parseDouble(totalStock), Double.parseDouble(currentStock), 
				0.0, category, Double.parseDouble(price), Long.parseLong(barCode));
	}
	
	//Translates a user from csv to java object
	public User readNextUser(String[] entries, String[] importValues, int userIDNumber, int barCodeImportErrors, int genderFormatErrors) {
		String fullName = "", callingName = "", team = "", study = "", rustur = "", 
				rank = "", studyNumber = "", firstName = "", lastName = "";
		Long barCode = 0L;
		int gender = 0;
		for (int i = 0; i < entries.length; i++) {
			if (importValues.length > i) {
				switch (importValues[i]) {
				case "full name":
					fullName = entries[i];
					break;
				case "first name":
					firstName = entries[i];
					break;
				case "last name":
					lastName = entries[i];
					break;
				case "calling name":
					callingName = entries[i];
					break;
				case "bar code":
					if (NumberFormatChecker.isInteger(entries[i])) {
						barCode = Long.parseLong(entries[i]);
					} else {
						barCodeImportErrors++;
					}
					break;
				case "team":
					team = entries[i];
					break;
				case "study":
					study = entries[i];
					break;
				case "rustur":
					rustur = entries[i];
					break;
				case "rank":
					rank = entries[i];
					break;
				case "study number":
					studyNumber = entries[i];
					break;
				case "gender": 
					if (NumberFormatChecker.isInteger(entries[i])) {
					gender = Integer.parseInt(entries[i]);
				} else if (entries[i].toLowerCase().equals("m")){
					gender = 1;
				} else if (entries[i].toLowerCase().equals("k") ||
						entries[i].toLowerCase().equals("f")){
					gender = 2;
				} else {
					genderFormatErrors++;
				}
					break;
				case "none":
					break;
				}
			}
		}
		if (fullName.isEmpty() && callingName.isEmpty() && team.isEmpty() && 
				study.isEmpty() && rustur.isEmpty() && rank.isEmpty() && 
				studyNumber.isEmpty() && firstName.isEmpty() && lastName.isEmpty()) return null;
		if (!firstName.isEmpty() && !lastName.isEmpty()) {
			fullName = firstName + " " + lastName;
		} else if (!fullName.isEmpty()) {
			
		} else {
			fullName = firstName + " " + lastName;
		}
		if (callingName.isEmpty())
			callingName = fullName;
		// TODO detect gender
		return new User(fullName, callingName, "u" + userIDNumber, rank, team, study, studyNumber, rustur, gender, barCode, null);
	}
	
	//Translate user from java object to csv format
	public void appendUser(StringBuilder sb, String[] fields, User user, String separator) {
		boolean first = true;
		for (String s : fields) {
    		if (first) first = false;
    		else sb.append(separator);
    		String toAdd = "";
    		switch(s) {
    		case "full name": toAdd = user.getName();	break;
			case "calling name": toAdd = user.getCallingName();	break;
			case "rank": toAdd = user.getRank();	break;
			case "team": toAdd = user.getTeam();	break;
			case "study number": toAdd = user.getStudyNumber();	break;
			case "rustur": toAdd = user.getRustur();	break;
			case "bar code": toAdd = user.getBarCode() + "";	break;
			case "gender": toAdd = user.getGenderInt() + ""; break;
			case "study": toAdd = user.getStudy(); break;
			case "default" : System.out.println("didnt recognize value while exporting users");;
			}
    		if (toAdd.isEmpty()) toAdd = "-";
    		sb.append(toAdd);
    		
    	}
//		for (double d : user.getUserProductAmounts()) {
//    		sb.append(separator + d);
//    	}
	}
	
	
	public void appendSavedFields(ArrayList<ComboBox<String>> options, boolean first, String[] fields, StringBuilder sb, int i, String separator) {
		if (options.get(i).getValue() != null) {
    		if (!first) {
    			sb.append("" + separator);
    		}
    		fields[i] = options.get(i).getValue().toLowerCase();
    		sb.append(fields[i]);
    	} else {
    		fields[i] = "";
    	}
	}
	
	public void appendProduct(StringBuilder sb, String[] fields, Product product, String separator) {
		boolean first = true;
    	for (String s : fields) {
    		System.out.println(s + ":" + product.getBarCode());
    		if (first) first = false;
    		else sb.append(separator);
    		String toAdd = "";
    		switch(s) {
    		case "name": toAdd = product.getName();	break;
			case "total stock": toAdd = product.getTotalStock() + "";	break;
			case "current stock": toAdd = product.getCurrentStock() + "";	break;
			case "category": toAdd = product.getCategory();	break;
			case "price": toAdd = product.getPrice() + "";	break;
			case "bar code": toAdd = product.getBarCode() + "";	break;
			case "default" : System.out.println("didnt recognize value while exporting products");;
			}
    		if (toAdd.isEmpty()) toAdd = "-";
    		sb.append(toAdd);
    	}
	}
	public void setupChangeSettingsGUI(GridPane changeSettingsPane, /*Button banner,*/ Button backupLocation, 
			Button passwordSave, Button backupTimerSave, TextField password, TextField backupTimer, ComboBox<String> theme,
			ComboBox<String> lockScanner, TextField email, Button emailSave, Button databaseLocation, String tooltipText) {
		int settingsWidth = 150, labelWidth = 130;
		Label /*bannerLabel,*/ backupLocationLabel;
		Label passwordLabel, backupTimerLabel;
		Label themeLabel, emailLabel, lockScannerLabel, databaseLocationLabel;
		
//		banner.setMinWidth(settingsWidth);
//		bannerLabel = new Label("Banner location");
//		bannerLabel.setMinWidth(labelWidth);
		databaseLocation.setMinWidth(settingsWidth);
		databaseLocationLabel = new Label("Database location");
		databaseLocationLabel.setMinWidth(labelWidth);
		backupLocation.setMinWidth(settingsWidth);
		backupLocationLabel = new Label("Backup location");
		backupLocationLabel.setMinWidth(labelWidth);
		password.setMaxWidth(settingsWidth);
		passwordLabel = new Label("Password");
		passwordLabel.setMinWidth(labelWidth);
		setButton(passwordSave, "/resources/images/save.png", 15, null);
		
		backupTimer.setMaxWidth(settingsWidth);
		setDoubleRestriction(backupTimer);
		backupTimerLabel = new Label("Backup timer (hrs)");
		backupTimerLabel.setMinWidth(labelWidth);
		setButton(backupTimerSave, "/resources/images/save.png", 15, null);
		
		email.setMaxWidth(settingsWidth);
		emailLabel = new Label("Recovery email");
		emailLabel.setMinWidth(labelWidth);
		setButton(emailSave, "/resources/images/save.png", 15, null);
		
		theme.getItems().addAll(GlobalVariables.getThemes());
		theme.setValue(GlobalVariables.themes[GlobalVariables.currentThemeInt]);
		theme.setMinWidth(settingsWidth);
		theme.getEditor().setAlignment(Pos.CENTER);
		themeLabel = new Label("Theme");
		themeLabel.setMinWidth(labelWidth);
		
		lockScanner.getItems().addAll("Yes","No");
		lockScanner.setValue(GlobalVariables.lockScan ? "Yes" : "No");
		lockScanner.setMinWidth(settingsWidth);
		lockScanner.getEditor().setAlignment(Pos.CENTER);
		lockScannerLabel = new Label("Lock scanner");
		lockScannerLabel.setMinWidth(labelWidth);
		changeSettingsPane.add(DialogManager.createTooltip(tooltipText),0,0);
//		changeSettingsPane.add(banner, 1, 1);
//		changeSettingsPane.add(bannerLabel, 0, 1);
		changeSettingsPane.add(databaseLocation, 1, 2);
		changeSettingsPane.add(databaseLocationLabel, 0, 2);
		changeSettingsPane.add(backupLocation, 1, 3);
		changeSettingsPane.add(backupLocationLabel, 0, 3);
		changeSettingsPane.add(passwordLabel, 0, 4);
		changeSettingsPane.add(password, 1, 4);
		changeSettingsPane.add(passwordSave, 2, 4);
		changeSettingsPane.add(backupTimerLabel, 0, 5);
		changeSettingsPane.add(backupTimer, 1, 5);
		changeSettingsPane.add(backupTimerSave, 2, 5);
		changeSettingsPane.add(emailLabel, 0, 6);
		changeSettingsPane.add(email, 1, 6);
		changeSettingsPane.add(emailSave, 2, 6);
		changeSettingsPane.add(theme, 1, 7);
		changeSettingsPane.add(themeLabel, 0, 7);
		changeSettingsPane.add(lockScanner, 1, 8);
		changeSettingsPane.add(lockScannerLabel, 0, 8);
	}
	
	
}
