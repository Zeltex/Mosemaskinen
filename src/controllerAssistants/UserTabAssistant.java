package controllerAssistants;

import java.util.ArrayList;
import java.util.regex.Pattern;

import database.GlobalVariables;
import importedCode.NumberFormatChecker;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.util.converter.DoubleStringConverter;
import managers.DialogManager;
import objects.User;
import objects.UserProduct;

public class UserTabAssistant {
	
	int userInfo4Pressed = 1, userInfo5Pressed = 1;
	
	public int getSortFilterValue(String newChoice) {
		int sortFilter = -1;
		if (newChoice != null) {
			switch (newChoice.toLowerCase()) {
			case "none":
				sortFilter = 0;
				break;
			case "name":
				sortFilter = 1;
				break;
			case "calling name":
				sortFilter = 2;
				break;
			case "rank":
				sortFilter = 3;
				break;
			case "team":
				sortFilter = 4;
				break;
			case "study":
				sortFilter = 11;
				break;
			case "studynumber":
				sortFilter = 5;
				break;
			case "rustur":
				sortFilter = 6;
				break;
			case "total items":
				sortFilter = 7;
				break;
			case "favorite item":
				sortFilter = 8;
				break;
			case "bar code":
				sortFilter = 9;
				break;
			case "gender":
				sortFilter = 10;
				break;
			}
		}
		return sortFilter;
	}

	public ArrayList<Double> getCompareStrings(ArrayList<TextField> userProductTextFields) {
		ArrayList<Double> compareStrings = new ArrayList<Double>();
		for (int i = 0; i < userProductTextFields.size(); i++) {
			if (userProductTextFields.get(i).isVisible()) {
				if (!NumberFormatChecker.isDouble(userProductTextFields.get(i).getText())) {
					DialogManager.alertError("Number format exception", null,
							"One or more fields doesn't comply with the number format");
					return null;
				} else {
					compareStrings.add(Double.parseDouble(userProductTextFields.get(i).getText()));
				}
			} else {
				break;
			}
		}
		return compareStrings;
	}
	public ArrayList<String> getNoneEmptyUserProductFields(ArrayList<TextField> userProductTextFields, 
			ArrayList<UserProduct> userProducts, boolean compareUserProducts) {
		ArrayList<String> difference = new ArrayList<String>();
		for (int i = 0; i < userProductTextFields.size(); i++) {
			if (userProductTextFields.get(i).isVisible() && !userProductTextFields.get(i).getText().isEmpty()) {
				
				if (!NumberFormatChecker.isDouble(userProductTextFields.get(i).getText())) {
					DialogManager.alertError("Number format exception", null,
							"One or more fields doesn't comply with the number format");
					return null;
				} else {
					if ((Double.valueOf(userProductTextFields.get(i).getText()) != 0.0 && !compareUserProducts) || 
							(Double.valueOf(userProductTextFields.get(i).getText()) != userProducts.get(i).getAmount() && compareUserProducts)) {
						difference.add(userProducts.get(i).getProductID());
						difference.add(userProductTextFields.get(i).getText());
					}
				}
			} 
		}
		return difference;
	}

	public void loadUserProducts(boolean empty, GridPane userProductInfoPane, ArrayList<UserProduct> userProducts,
			ArrayList<TextField> userProductTextFields, ArrayList<Label> userProductLabels) {
		userProductInfoPane.getChildren().clear();
		userProductLabels.clear();
		userProductTextFields.clear();
		for (int i = 0; i < userProducts.size(); i++) {
			TextField textField = new TextField();
			setDoubleRestriction(textField);
			textField.setText((empty ? "" : String.format("%.1f", userProducts.get(i).getAmount()).replace(',','.')));
			userProductLabels.add(new Label(userProducts.get(i).getProductName()));
			userProductLabels.get(i).setMinWidth(100);
			userProductTextFields.add(textField);
			userProductTextFields.get(i).setMaxWidth(65);
			userProductInfoPane.add(userProductLabels.get(i), 0, i);
			userProductInfoPane.add(userProductTextFields.get(i), 1, i);
		}

	}

	// Adds choices to the combo boxes
	public void setupComboBoxes(ArrayList<ComboBox<String>> userInfoCombo) {
		userInfoCombo.get(0).getItems().clear();
		userInfoCombo.get(0).getItems().addAll(GlobalVariables.getRanks());
		userInfoCombo.get(1).getItems().clear();
		userInfoCombo.get(1).getItems().addAll(GlobalVariables.getTeams());
		userInfoCombo.get(2).getItems().clear();
		userInfoCombo.get(2).getItems().addAll(GlobalVariables.getGendersAll());
	}

	public void resetComboBox(ArrayList<ComboBox<String>> userInfoCombo, User chosenUser) {
		if (chosenUser != null) {
			userInfoCombo.get(0).setValue(chosenUser.getRank());
			userInfoCombo.get(1).setValue(chosenUser.getTeam());
			userInfoCombo.get(2).setValue(chosenUser.getGender());
		} else setComboBoxesEmpty(userInfoCombo);
	}
	public void setComboBoxesEmpty(ArrayList<ComboBox<String>> userInfoCombo) {
		userInfoCombo.get(0).setValue("");
		userInfoCombo.get(1).setValue("");
		userInfoCombo.get(2).setValue("");
	}
	public void setTextUserInfoUneditable(boolean resetComboBoxes, TextField[] userInfoText, ArrayList<ComboBox<String>> userInfoCombo, User chosenUser) {
		int[] uneditable = new int[]{2,5,6};
		for (int i : uneditable){
			userInfoText[i].setEditable(false);
		}
		setFieldsEditable(false, resetComboBoxes, userInfoText, userInfoCombo, chosenUser, false);
	}
	
	public void setFieldsEditable(boolean editable, boolean resetComboBoxes, TextField[] userInfoText, ArrayList<ComboBox<String>> userInfoCombo, User chosenUser, boolean multiple) {
		//userInfo 1,2,6,7,11,12
		int[] editableFields;
		if (multiple) {
			editableFields = new int[]{0,1,3,4,8};
		} else {
			editableFields = new int[]{0,1,3,4,7,8};
		}
		for (int i : editableFields){
			userInfoText[i].setEditable(editable);
		}
		for (int i = 0; i < userInfoCombo.size(); i ++) {
			userInfoCombo.get(i).setEditable(editable);
			userInfoCombo.get(i).setDisable(!editable);
		}
		if (resetComboBoxes) resetComboBox(userInfoCombo, chosenUser);
	}
	
	public void clearUserInfoFields(ArrayList<ComboBox<String>> userInfoCombo, TextField[] userInfoText) {
		for (TextField t : userInfoText) {
			t.clear();
		}
		for (ComboBox<String> c : userInfoCombo) {
			c.setValue("");
		}
	}
	
	public void setTextUserProductInfoEditable(boolean editable, ArrayList<TextField> userProductTextFields) {
		for (int i = 0; i < userProductTextFields.size(); i++) {
			userProductTextFields.get(i).setEditable(editable);
		}
	}
	
	public boolean isUserInfoFieldEmpty(ArrayList<ComboBox<String>> userInfoCombo, TextField[] userInfoText) {
		for (ComboBox<String> c : userInfoCombo) {
			if (c.getValue() == null) return true;
		}
		return false;
		
		//userInfo 1,2,6,7,11
//		int[] editableFields = new int[]{0,1,3,4,7};
//		for (int i : editableFields) {
//			if (userInfoText[i].getText().isEmpty()) return true;
//		}
//		for (ComboBox<String> c : userInfoCombo) {
//			if (c.getValue() == null) return true;
//			if (c.getValue().isEmpty()) return true;
//		}
//		return false;
	}
	

	public void setInitialUserProductLayout(GridPane userProductInfoPane) {
		Label label = new Label();
		label.setMinWidth(100);
		TextField textField = new TextField();;
		textField.setMaxWidth(50);
		userProductInfoPane.add(label, 0, 0);
		userProductInfoPane.add(textField, 1, 0);
		label.setVisible(false);
		textField.setVisible(false);
	}
	
	public void setDoubleRestriction(TextField t) {
		Pattern validDoubleText = Pattern.compile("-?((\\d*)|(\\d+\\.\\d*))");

		TextFormatter<Double> textFormatter = new TextFormatter<Double>(new DoubleStringConverter(), 0.0, change -> {
			String newText = change.getControlNewText();
			if (validDoubleText.matcher(newText).matches()) {
				return change;
				
			} else
				return null;
		});
		t.setTextFormatter(textFormatter);
	}
	
	public int getFirstEmptyRow(GridPane gridPane) {
		ObservableList<Node> nodes = gridPane.getChildren();
		int highestIndex = -1;
		for (int i = 0; i < nodes.size(); i++) {
			int currentIndex = GridPane.getRowIndex(nodes.get(i));
			if (currentIndex > highestIndex) {
				highestIndex = currentIndex;
			}
		}
		return highestIndex + 1;
	}
	
	public boolean isUserProductInfoFieldEmpty(ArrayList<TextField> userProductTextFields) {
		for (int i = 0; i < userProductTextFields.size(); i++) {
			if (userProductTextFields.get(i).isVisible()) {
				if (userProductTextFields.get(i).getText().isEmpty()) {
					return true;
				}
			} else {
				return false;
			}
		}
		return false;
	}
	
	public ArrayList<Integer> getNoneEmptyUserFields(ArrayList<ComboBox<String>> userInfoCombo, TextField[] userInfoText) {
		ArrayList<Integer> result = new ArrayList<Integer>();
		if (!userInfoText[0].getText().isEmpty())
			result.add(1);
		if (!userInfoText[1].getText().isEmpty())
			result.add(2);
		if (userInfoCombo.get(0).getValue() != null)
			if (!userInfoCombo.get(0).getValue().isEmpty())
				result.add(4);
		if (userInfoCombo.get(1).getValue() != null)
			if (!userInfoCombo.get(1).getValue().isEmpty())
				result.add(5);
		if (!userInfoText[3].getText().isEmpty())
			result.add(6);
		if (!userInfoText[4].getText().isEmpty())
			result.add(7);
		if (userInfoCombo.get(2).getValue() != null)
			if (!userInfoCombo.get(2).getValue().isEmpty())
				result.add(10);
		if (!userInfoText[7].getText().isEmpty())
			result.add(11);
		if (!userInfoText[8].getText().isEmpty())
			result.add(12);
		return result;
	}
	
	public void setupComboBoxesBahaviour(ArrayList<ComboBox<String>> userInfoCombo){
		
		userInfoCombo.get(0).getEditor().setOnMousePressed(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				userInfo4Pressed = 1;
			}
		});
		userInfoCombo.get(0).setOnShown(new EventHandler<Event>() {
			@Override
			public void handle(Event event) {
				if (userInfo4Pressed < 2) {
					userInfoCombo.get(0).hide();
				} else {
					userInfo4Pressed = 0;
				}
			}
		});
		userInfoCombo.get(0).setOnMousePressed(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				userInfo4Pressed++;
				if (!userInfoCombo.get(0).isFocused())
					userInfo4Pressed = 2;
			}
		});
		userInfoCombo.get(1).getEditor().setOnMousePressed(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				userInfo5Pressed = 1;
			}
		});
		userInfoCombo.get(1).setOnShown(new EventHandler<Event>() {
			@Override
			public void handle(Event event) {
				if (userInfo5Pressed < 2) {
					userInfoCombo.get(1).hide();
				} else {
					userInfo5Pressed = 0;
				}
			}
		});
		userInfoCombo.get(1).setOnMousePressed(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				userInfo5Pressed++;
				if (!userInfoCombo.get(1).isFocused())
					userInfo5Pressed = 2;
			}
		});
	}
	
	public void showUserData(User user, ArrayList<ComboBox<String>> userInfoCombo, TextField[] userInfoText) {
		userInfoText[0].setText(user.getName());
		userInfoText[1].setText(user.getCallingName());
		userInfoText[2].setText(user.getID());
		userInfoCombo.get(0).setValue(user.getRank());
		userInfoCombo.get(1).setValue(user.getTeam());
		userInfoText[3].setText(user.getStudyNumber());
		userInfoText[4].setText(user.getRustur());
		userInfoText[5].setText(user.getFavoriteItem());
		userInfoText[6].setText(String.format("%.2f", user.getTotalItems()).replace(',','.'));
		userInfoCombo.get(2).setValue(user.getGender());
		userInfoText[7].setText(user.getBarCode() + "");
		userInfoText[8].setText(user.getStudy());

	}
}
