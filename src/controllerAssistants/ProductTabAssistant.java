package controllerAssistants;

import java.util.ArrayList;
import java.util.regex.Pattern;

import database.GlobalVariables;
import importedCode.NumberFormatChecker;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import objects.Product;

public class ProductTabAssistant {
	int productInfo6Pressed = 1;
	
	// Sets up items in combobox
	public void setupComboBox(ArrayList<ComboBox<String>> productInfoCombo) {
		productInfoCombo.get(0).getItems().clear();
		productInfoCombo.get(0).getItems().addAll(GlobalVariables.getCategories());
	}
	
	// Returns all the input fields that are not empty
	public ArrayList<Integer> getNoneEmptyUserFields(ArrayList<ComboBox<String>> productInfoCombo, TextField[] productInfoText) {
		ArrayList<Integer> result = new ArrayList<Integer>();
		int[] number = new int[]{2,3,4,7,8};
		int[] indices = new int[]{1,2,3,5,6};
		for (int i = 0; i < indices.length; i ++) {
			if (!productInfoText[indices[i]].getText().isEmpty()) result.add(number[i]);
		}
		number = new int[]{6};
		for (int i = 0; i < productInfoCombo.size(); i ++) {
			if (productInfoCombo.get(i) == null) continue;
			if (!productInfoCombo.get(i).getValue().isEmpty()) result.add(number[i]);
		}
		return result;
	}

	// Reset combobox
	public void resetComboBox(ArrayList<ComboBox<String>> productInfoCombo, Product chosenProduct) {
		if (chosenProduct != null) {
			productInfoCombo.get(0).setValue(chosenProduct.getCategory());
		} else setComboboxEmpty(productInfoCombo);
	}
	
	// Empty combobox
	public void setComboboxEmpty(ArrayList<ComboBox<String>> productInfoCombo) {
		productInfoCombo.get(0).setValue("");
	}
	
	// Check format of fields
	public boolean validateInputFields(TextField[] productInfoText) {
		if (!NumberFormatChecker.isDouble(productInfoText[2].getText()))
			return false;
		if (!NumberFormatChecker.isDouble(productInfoText[3].getText()))
			return false;
		if (!NumberFormatChecker.isDouble(productInfoText[5].getText()))
			return false;
		if (!NumberFormatChecker.isInteger(productInfoText[6].getText()))
			return false;
		return true;
	}
	
	// Changes editability of comboboxes and textfields
	public void setFieldsEditable(boolean editable, boolean resetComboBox, ArrayList<ComboBox<String>> productInfoCombo, TextField[] productInfoText, Product chosenProduct, boolean multiple) {
		//productInfo 2,3,4,7,8
		int[] editableFields;
		if (multiple){
			editableFields = new int[]{1,2,3,5};			
		} else {
			editableFields = new int[]{1,2,3,5,6};
		}
		for (int i : editableFields) {
			productInfoText[i].setEditable(editable);
		}
		for (ComboBox<String> c : productInfoCombo) {
			c.setEditable(editable);
			c.setDisable(!editable);
		}
		if (resetComboBox)
			resetComboBox(productInfoCombo, chosenProduct);
	}
	
	// Sets all comboboxes and textfields uneditable
	public void setTextProductInfoUneditable(boolean resetComboBox, ArrayList<ComboBox<String>> productInfoCombo, TextField[] productInfoText, Product chosenProduct) {
		for (TextField t : productInfoText) {
			t.setEditable(false);
		}
		for (ComboBox<String> c : productInfoCombo) {
			c.setEditable(false);
			c.setDisable(true);
		}
		if (resetComboBox)
			resetComboBox(productInfoCombo, chosenProduct);
	}
	
	// Displays the info of a product
	public void showProductData(Product product, ArrayList<ComboBox<String>> productInfoCombo, TextField[] productInfoText) {
		String[] info = product.getDisplayInfo();
		int[] textInfo = new int[]{0,1,2,3,4,6,7};
		for (int i = 0; i < productInfoText.length; i ++) {
			if (i == 4) {
				if (NumberFormatChecker.isDouble(info[textInfo[i]]))
				productInfoText[i].setText(String.format("%.2f", Double.valueOf(info[textInfo[i]])).replace(',','.'));
			}	else {
				productInfoText[i].setText(info[textInfo[i]]);
			}
		}
		int[] comboInfo = new int[]{5};
		for (int i = 0; i < productInfoCombo.size(); i ++) {
			productInfoCombo.get(i).setValue(info[comboInfo[i]]);
		}
	}
	
	// Empties fields
	public void clearFields(ArrayList<ComboBox<String>> productInfoCombo, TextField[] productInfoText) {
		for (ComboBox<String> c : productInfoCombo) {
			c.setValue("");
		}
		for (TextField t : productInfoText) {
			t.setText("");
		}
	}
	
	// Checks if new input in textfield complies with double or integer format 
	public void setDoubleRestriction(TextField t) {
		Pattern validDoubleText = Pattern.compile("-?((\\d*)|(\\d+\\.\\d*))");
		t.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldText, String newText) {
				// Double restriction
				if (validDoubleText.matcher(newText).matches()) {
					t.setText(newText);
				} else {
					// Integer restriction
					boolean change = true;
					if (newText.length() > 0) {
						if (newText.substring(0, 1).equals(".")) {
							if (newText.substring(1,newText.length()).matches("\\d*")) {
								Platform.runLater(() -> t.setText(newText.substring(1, newText.length())));
								change = false;
							}
						}
					}
					if (change) t.setText(oldText);;
				}
			}
		});
	}

	// Checks if new input in textfield complies with integer format 
	public void setIntegerRestriction(TextField text) {
		text.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d*")) {
					text.setText(newValue.replaceAll("[^\\d]", ""));
				}
			}
		});
	}
	
	// Translates the sort text with a number to be used elsewhere
	public int getSortFilter(String newChoice) {
		int sortFilter = -1;
		switch (newChoice.toLowerCase()) {
		case "none":
			sortFilter = 0;
			break;
		case "name":

			sortFilter = 1;
			break;
		case "current stock":
			sortFilter = 2;
			break;
		case "sold":
			sortFilter = 3;
			break;
		case "category":
			sortFilter = 4;
			break;
		case "price":
			sortFilter = 5;
			break;
		case "barcode":
			sortFilter = 6;
			break;
		}
		return sortFilter;
	}
	
	// Sets combobox behavior so that clicking on a combobox while it is expanded will collapse it
	public void setComboBehaviour(ArrayList<ComboBox<String>> productInfoCombo) {
		productInfoCombo.get(0).getEditor().setOnMousePressed(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				productInfo6Pressed = 1;
			}
		});
		productInfoCombo.get(0).setOnShown(new EventHandler<Event>() {
			@Override
			public void handle(Event event) {
				if (productInfo6Pressed < 2) {
					productInfoCombo.get(0).hide();
				} else {
					productInfo6Pressed = 0;
				}
			}
		});
		productInfoCombo.get(0).setOnMousePressed(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				productInfo6Pressed++;
				if (!productInfoCombo.get(0).isFocused())
					productInfo6Pressed = 2;
			}
		});
	}
}
