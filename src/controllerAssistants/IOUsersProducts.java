package controllerAssistants;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import controllers.MainTabController;
import controllers.SettingsTabController;
import database.DatabaseHandler;
import database.GlobalVariables;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import managers.DialogManager;
import objects.Product;
import objects.User;

public class IOUsersProducts {
	
	Label linesToSkipImportLabel;
	TextField linesToSkipImport;
	String[] importValues;
	int linesToSkip;
	boolean showLinesToSkip;
	int barCodeImportErrors = 0, numberFormatImportErrors = 0, genderFormatErrors = 0;
	String separator = ";";
	ArrayList<ComboBox<String>> comboOptions;
	String[] userOptionStrings = new String[] { "First name", "Last name", "Full name", "Calling name", "Bar code", "Team", "Study", "Rustur", "Rank",
			"Study number", "Gender", "none" },
			productOptionStrings = new String[] { "Name", "Total stock", "Current stock", "Category", "Price", "Bar code", "none" };
	Button startUserImport, startUserExport, startProductImport, startProductExport;
	int startIOWidth = 270;
	
	GridPane changeSettingsPane;
	SettingsTabAssistant sta;
	MainTabController mtc;
	DatabaseHandler dbHandler;
	SettingsTabController stc;
	
	public IOUsersProducts(GridPane changeSettingsPane, SettingsTabAssistant sta, MainTabController mtc, 
			DatabaseHandler dbHandler, SettingsTabController stc) {
		this.changeSettingsPane = changeSettingsPane;
		this.sta = sta;
		this.mtc = mtc;
		this.dbHandler = dbHandler;
		this.stc = stc;
	}
	
	public void init() {

		comboOptions = new ArrayList<ComboBox<String>>();
	}
	
	public void packOptions() {
//		changeSettingsPane.getChildren().removeAll(comboOptions);
		changeSettingsPane.getChildren().clear();
		stc.deleteButtons.clear();
//		ObservableList<Node> nodes = FXCollections.observableArrayList();
//				nodes.addAll(changeSettingsPane.getChildren());
//		for (Node n  : nodes) {
//			if (n.getClass().equals(Label.class)) changeSettingsPane.getChildren().remove(n);
//		}
		changeSettingsPane.add(stc.toolBar, 0, 0, 2, 1);
		int offset = 0;
		if (showLinesToSkip) {
			offset = 1;
			setUpLinesToSkip(160);
		}
		for (int i = 0; i < comboOptions.size(); i ++) {
			Label label = new Label("Value " + (i + 1));
			label.setMinWidth(100);
			changeSettingsPane.add(label, 0, i + 1 + offset);
			changeSettingsPane.add(comboOptions.get(i), 1, i + 1 + offset);
			stc.addDelete(comboOptions.get(i), i + offset, 2);
		}
		int row = comboOptions.size() + 1;
		switch(stc.showContent) {
		case 2: changeSettingsPane.add(startUserImport, 0, row + 1, 2, 1); break;
		case 3: changeSettingsPane.add(startUserExport, 0, row + 1, 2, 1); break;
		case 4: changeSettingsPane.add(startProductImport, 0, row + 1, 2, 1); break;
		case 5: changeSettingsPane.add(startProductExport, 0, row + 1, 2, 1); break;
		}
	}
	
	//Reading .csv file and loading new products into program and database
		public boolean readProductImportFile(File importFile) {
			setImportStartVariables();
			String separator = ";";
			try (BufferedReader br = new BufferedReader(
					   new InputStreamReader(
			                      new FileInputStream(importFile), "UTF8"));) {
				String line;
				for (int i = 0; i < linesToSkip; i++) {
					br.readLine();
				}
				boolean first = true;
				numberFormatImportErrors = 0;
				boolean barCodePromptFirst = true;
				boolean confirm = false;
				Long barCodeNumber = dbHandler.getFreeProductBarCodeNumber();
				ArrayList<Product> products = new ArrayList<Product>();
				ArrayList<String> existingProducts = dbHandler.getProductNames();
				ArrayList<Long> existingBarCodes = dbHandler.getProductBarCodes();
				ArrayList<Long> justAddedBarCodes = new ArrayList<Long>();
				ArrayList<Long> barCodesToChange = new ArrayList<Long>();
				while ((line = br.readLine()) != null) {
					// Detect separator and confirm import form
					if (first) {
						separator = sta.getSeparator(line);
						first = false;
						int importFileColumns = line.split("\\" + separator).length;
						if (importFileColumns != importValues.length) {
							boolean confirmFields = DialogManager.alertConfirmation("Import fields mismatch", null, "Found " + importValues.length + 
									" fields in import settings, but " + importFileColumns + " in import file. Cancel import and "
								+ "fix import settings to match import file, or lowest denominator will be used.");
							if (!confirmFields) {
								return false;
							}
						}
					}
					String[] entries = line.split("\\" + separator);
					
					Product newProduct = sta.readNextProduct(entries, importValues);
					if (!existingProducts.contains(newProduct.getName())) {
						products.add(newProduct);
						existingProducts.add(newProduct.getName());
					}
					// Make sure next barCode is unique
					while(existingBarCodes.contains(barCodeNumber) || justAddedBarCodes.contains(barCodeNumber)) {
						barCodeNumber++;
					}
					// Handle barCode duplicate
					if (existingBarCodes.contains(newProduct.getBarCode()) && newProduct.getBarCode() > 0) {
						if (barCodePromptFirst) {
							confirm = DialogManager.alertConfirmation("Bar code duplicate", null, "Import file contains one or more bar codes which "
								+ "already exists in the program. Press 'OK' to assign new bar codes to imported products, or 'cancel' "
								+ "to assign new bar codes to existing products.");
							barCodePromptFirst = false;
						}
						// Overwrite import barCode
						if (confirm) {
							newProduct.setBarCode(barCodeNumber);
							justAddedBarCodes.add(barCodeNumber);
							barCodeNumber++;
						// Overwrite existing barCode
						} else {
							barCodesToChange.add(newProduct.getBarCode());
							barCodesToChange.add(barCodeNumber);
							barCodeNumber ++;
						}
					} else if(justAddedBarCodes.contains(newProduct.getBarCode()) || newProduct.getBarCode() > 999999 || 
							(newProduct.getBarCode() > 0 && newProduct.getBarCode() < 100000)) {
						newProduct.setBarCode(barCodeNumber);
						justAddedBarCodes.add(newProduct.getBarCode());
						barCodeNumber ++;
					// Assign new barCode
					} else if (newProduct.getBarCode() <= 0) {
						newProduct.setBarCode(barCodeNumber);
						existingBarCodes.add(barCodeNumber);
						barCodeNumber++;
					} else {
						justAddedBarCodes.add(newProduct.getBarCode());
					}
					
				}
				if (!dbHandler.changeProductBarCodes(barCodesToChange)) {
					return false;
				}
				if (!dbHandler.addProducts(products)) {
					return false;
				}
				mtc.load();
				if (numberFormatImportErrors != 0) {
					DialogManager.alertError("Number input wrong format", null, "Found " + numberFormatImportErrors
							+ " format errors for products. They will be assigned 0.0.");
				}
				return true;
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				return false;
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}
		}
		//Reading .csv file and loading new users into program and database
		public boolean readUserImportFile(File importFile) {
			setImportStartVariables();
			String separator = ";";
//			try (BufferedReader br = new BufferedReader(new FileReader(importFile))) {
			try (BufferedReader br = new BufferedReader(
			   new InputStreamReader(
	                      new FileInputStream(importFile), "UTF8"));) {
				String line;
				for (int i = 0; i < linesToSkip; i++) {
					br.readLine();
				}
				boolean first = true;
				barCodeImportErrors = 0; 
				genderFormatErrors = 0;
				boolean barCodePromptFirst = true;
				boolean confirm = false;
				int userIDNumber = dbHandler.getFreeUserIDNumber();
				Long barCodeNumber = dbHandler.getFreeUserBarCodeNumber();
				ArrayList<User> users = new ArrayList<User>();
				ArrayList<Long> existingBarCodes = dbHandler.getUserBarCodes();
				ArrayList<Long> barCodesToChange = new ArrayList<Long>();
				ArrayList<Long> justAddedBarCodes = new ArrayList<Long>();
				while ((line = br.readLine()) != null) {
					
					// Detect separator and confirm import format
					if (first) {
						separator = sta.getSeparator(line);
						first = false;
						String[] temp = line.split("\\" + separator);
						int importFileColumns = temp.length;
						if (importFileColumns != importValues.length) {
							boolean confirmFields = DialogManager.alertConfirmation("Import fields mismatch", null, "Found " + importValues.length + 
									" fields in import settings, but " + importFileColumns + " in import file. Cancel import and "
								+ "fix import settings to match import file, or lowest denominator will be used.");
							if (!confirmFields) {
								return false;
							}
						}
					}
					// Handle user import
					String[] entries = line.split("\\" + separator);
					User newUser = sta.readNextUser(entries, importValues, userIDNumber, barCodeImportErrors, genderFormatErrors);
					if (newUser == null) continue;
					// Make sure next barCode is unique
					while(existingBarCodes.contains(barCodeNumber) || justAddedBarCodes.contains(barCodeNumber)) {
						barCodeNumber++;
					}
					
					// Handle barCode duplicate
					if (existingBarCodes.contains(newUser.getBarCode()) && newUser.getBarCode() > 0) {
						if (barCodePromptFirst) {
							confirm = DialogManager.alertConfirmation("Bar code duplicate", null, "Import file contains one or more bar codes which "
								+ "already exists in the program. Press 'OK' to assign new bar codes to imported users, or 'cancel' "
								+ "to assign new bar codes to existing users.");
							barCodePromptFirst = false;
						}
						// Overwrite import barCode
						if (confirm) {
							newUser.setBarCode(barCodeNumber);
							justAddedBarCodes.add(barCodeNumber);
							barCodeNumber++;
						// Overwrite existing barCode
						} else {
							barCodesToChange.add(newUser.getBarCode());
							barCodesToChange.add(barCodeNumber);
							barCodeNumber ++;
						}
					} else if(justAddedBarCodes.contains(newUser.getBarCode()) || newUser.getBarCode() > 9999 || 
							(newUser.getBarCode() > 0 && newUser.getBarCode() < 1000)) {
						newUser.setBarCode(barCodeNumber);
						justAddedBarCodes.add(newUser.getBarCode());
						barCodeNumber ++;	
					// Assign new barCode
					} else if (newUser.getBarCode() <= 0) {
						newUser.setBarCode(barCodeNumber);
						justAddedBarCodes.add(barCodeNumber);
						barCodeNumber++;
					}
					
					users.add(newUser);
					userIDNumber++;
				}
				if (!dbHandler.changeUserBarCodes(barCodesToChange)) {
					return false;
				}
				if (!dbHandler.addUsers(users)) {
					return false;
				}
				mtc.load();
				if (barCodeImportErrors != 0) {
					DialogManager.alertError("Barcode wrong format", null, "Found " + barCodeImportErrors
							+ " format errors for user bar code. They have been assigned new bar codes.");
				}
				if (genderFormatErrors != 0) {
					DialogManager.alertError("Gender wrong format", null, "Found " + genderFormatErrors
							+ " format errors for user gender. Genders are 0:Unkown, 1:Male, 2:Female, and digits should"
							+ "be used to specify gender. All wrongly formatted genders has been set to 'Unknown'");
				}
				return true;
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				return false;
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}
		}
		
		//Creating the .csv file during export of users
		public boolean generateExportUserFile(File directory) {
			try (Writer out = new BufferedWriter(new OutputStreamWriter(
				    new FileOutputStream(directory.getAbsolutePath()), "UTF-8"));) {
				StringBuilder sb = new StringBuilder();
				boolean first = true;
				String[] fields = new String[comboOptions.size()];
				// Append user info column names
				for (int i = 0; i < comboOptions.size(); i++) {
					sta.appendSavedFields(comboOptions, first, fields, sb, i, separator);
					if (first) first = false;
				}
//				// Append product info column names
//				for (UserProduct up : dbHandler.getAUser().getUserProducts()) {
//					sb.append(separator + up.getProductID());
//				}
				sb.append("\n");
				// Append users
				for (User user : dbHandler.getUsers()) {
					sta.appendUser(sb, fields, user, separator);
					sb.append("\n");
				}
				out.append(sb);
				return true;
			} catch (IOException e) {
				return false;
			}
		}

		//Creating the .csv file during export of products
		public boolean generateExportProductFile(File directory) {
			try (Writer out = new BufferedWriter(new OutputStreamWriter(
				    new FileOutputStream(directory.getAbsolutePath()), "UTF-8"));){
				
		        StringBuilder sb = new StringBuilder();
		        boolean first = true;
		        String[] fields = new String[comboOptions.size()];
		        // Append product info column names
		        for (int i = 0; i < comboOptions.size(); i ++) {
		        	if (comboOptions.get(i).getValue() != null) {
		        		if (!first) {
		        			sb.append("" + separator);
		        		} else {
		        			first = false;
		        		}
		        		fields[i] = comboOptions.get(i).getValue().toLowerCase();
		        		sb.append(fields[i]);
		        	} else {
		        		fields[i] = "";
		        	}
		        }
		        sb.append("\n");
		        // Append columns
		        for (Product product : dbHandler.getProducts()) {
		        	sta.appendProduct(sb, fields, product, separator);
		            sb.append("\n");
		        }
		        out.append(sb);
		        return true;
				} catch(IOException e) {
					DialogManager.alertError("IOException", null, "LOL");
					return false;
				}
		}
		
		//Prompting user to choose a .csv file for import
		public File getCSV() {
			File importFile = DialogManager.getExistingFile(new String[] { "csv" },
					new File(GlobalVariables.databasePath).getParent());
			if (importFile != null) {
				int length = importFile.getName().length();
				if (length > 4) {
					if (!importFile.getName().substring(length - 4, length).equals(".csv")) {
						String[] nameSplit = importFile.getName().split("\\.");
						String extension = nameSplit[nameSplit.length - 1];
						DialogManager.alertError("Wrong file format", null,
								"Chosen file has wrong extension. Expected: " + ".csv, received: " + extension);
					} else {
						return importFile;
					}
				}
			}
			return null;
		}
		

		//Setup basic layout for export/import of users/products
		public int setupImportExportUI(boolean showLinesToSkip, String[] optionStrings, int[] initialValues) {
			changeSettingsPane.getChildren().clear();
			int row = 1;
			this.showLinesToSkip = showLinesToSkip;
			int width = 160;
			if (showLinesToSkip) {
				setUpLinesToSkip(width);
				row = 2;
			}
			comboOptions = new ArrayList<ComboBox<String>>();
			// Constructing rows
			for (int i = 0; i < initialValues.length; i++) {
				Label label = new Label("Value " + (i + 1));
				label.setMinWidth(100);
				comboOptions.add(new ComboBox<String>());
				comboOptions.get(i).getItems().addAll(optionStrings);
				if (stc.showContent == 3) comboOptions.get(i).getItems().removeAll("First name", "Last name");
				comboOptions.get(i).getSelectionModel().select(optionStrings[initialValues[i]]);
				comboOptions.get(i).setMinWidth(width);
//				sta.preventDuplicateOptionChosen(comboOptions.get(i), comboOptions);
				changeSettingsPane.add(label, 0, row);
				changeSettingsPane.add(comboOptions.get(i), 1, row);
				stc.addDelete(comboOptions.get(i), row - 1, 2);
				row ++;
			}
			return row;
		}
		
		//Adds the lineToSkip label and textField
		public void setUpLinesToSkip(int width) {
			linesToSkipImport = new TextField("1");
			sta.setIntegerRestriction(linesToSkipImport);
			linesToSkipImportLabel = new Label("Lines to skip");
			linesToSkipImportLabel.setMinWidth(100);
			linesToSkipImport.setMaxWidth(width);

			changeSettingsPane.add(linesToSkipImportLabel, 0, 1);
			changeSettingsPane.add(linesToSkipImport, 1, 1);
		}
		
		//Add field pressed
		public void addPressed(int highest){
			int width = 160;
			ComboBox<String> newComb = new ComboBox<String>();
			newComb.getSelectionModel().select("None");
			newComb.setMinWidth(width);
//			sta.preventDuplicateOptionChosen(newComb, comboOptions);
			comboOptions.add(newComb);
			if (stc.showContent <= 3) {
				comboOptions.get(comboOptions.size() - 1).getItems().addAll(userOptionStrings);
				if (stc.showContent == 3) comboOptions.get(comboOptions.size() - 1).getItems().removeAll("First name", "Last name");
			}
			else comboOptions.get(comboOptions.size() - 1).getItems().addAll(productOptionStrings);
//			stc.addDelete(comboOptions.get(comboOptions.size() - 1), highest - 2, 2);
			packOptions();
		}
		
		
		public void deletePressed(Node node, Button delete) {
			comboOptions.remove(node);
			changeSettingsPane.getChildren().remove(node);
			changeSettingsPane.getChildren().remove(delete);
			packOptions();
		}
		
		//Setting some start variables for import. Same code was used twice, so i threw it in a method
		public void setImportStartVariables() {
			linesToSkip = Integer.parseInt(linesToSkipImport.getText());
			importValues = new String[comboOptions.size()];
			for (int i = 0; i < importValues.length; i++) {
				importValues[i] = (comboOptions.get(i).getValue() == null ? "None" : comboOptions.get(i).getValue().toLowerCase());
			}
		}
		//Button press "export users" -> "start export"
		public void exportUsersListener() {
			startUserExport.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent event) {
					if (!checkDuplicates()) {
						DialogManager.showPopup("Failed to export users");
						return;
					}
					File importFile = DialogManager.saveFile(new String[] { "csv" },
							new File(GlobalVariables.databasePath).getParent());
					if (importFile != null) {
						if (generateExportUserFile(importFile)) {
							DialogManager.showPopup("User export successfull");
							changeSettingsPane.getChildren().clear();
							stc.showContent = -1;
							stc.setButtonHighlight();
						} else {
							DialogManager.showPopup("Failed to export users");
						}
					} else {
						DialogManager.showPopup("Failed to export users");
					}
		};});}
		
		//Button press "import users" -> "start import"
		public void importUsersListener() {
			startUserImport.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent event) {
					if (!checkDuplicates()) {
						DialogManager.showPopup("Failed to import users");
						return;
					}
					File file = getCSV();
					if (file != null) {
						if (readUserImportFile(file)) {
							DialogManager.showPopup("User import successfull");
							changeSettingsPane.getChildren().clear();
							stc.showContent = -1;
							stc.setButtonHighlight();
						} else {
							DialogManager.showPopup("Failed to import users");
						}
					} else {
						DialogManager.showPopup("Failed to import users");
					}
		};});}
		
		//Button press "import products" -> "start import"
		public void importProductsListener() {
			startProductImport.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent event) {
					if (!checkDuplicates()) {
						DialogManager.showPopup("Failed to import products");
						return;
					}
					File file = getCSV();
					if (file != null) {
						if (readProductImportFile(file)) {
							DialogManager.showPopup("Product import successfull");
							changeSettingsPane.getChildren().clear();
							stc.showContent = -1;
							stc.setButtonHighlight();
						} else {
							DialogManager.showPopup("Failed to import products");
						}
					} else {
						DialogManager.showPopup("Failed to import products");
					}
		};});}
		
		//Button press "export products" -> "start export"
		public void exportProductsListener() {
			startProductExport.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent event) {
					if (!checkDuplicates()) {
						DialogManager.showPopup("Failed to export products");
						return;
					}
					File importFile = DialogManager.saveFile(new String[]{"csv"}, new File(GlobalVariables.databasePath).getParent());
					if (importFile != null) {
						if (generateExportProductFile(importFile)) {
							DialogManager.showPopup("Product export successfull");
							changeSettingsPane.getChildren().clear();
							stc.showContent = -1;
							stc.setButtonHighlight();
						} else {
							DialogManager.showPopup("Failed to export products");
						}
					} else {
						DialogManager.showPopup("Failed to export products");
					}
		};});}
		
		private boolean checkDuplicates() {
			List<String> dub = new ArrayList<String>();
			for(ComboBox<String> c : comboOptions) {
				if (c.getValue() != null) {
					if (!c.getValue().isEmpty()) {
						if (dub.contains(c.getValue())) {
							return DialogManager.alertConfirmation("Duplicate field", null, "Detected one or more duplicate fields. "
									+ "This is usually not pratical, are you sure you want to continue?");
						}
						dub.add(c.getValue());
					}
				}
			}
			return true;
		}
		
		//Button press, "import products"
		public void importProducts() {
			stc.showContent = 4;
			stc.setButtonHighlight();
			int row = setupImportExportUI(true,productOptionStrings, new int[] { 0, 1, 2, 3, 4});
			stc.setToolBar(0, -1, null, true, false, true, DialogManager.loadText("/resources/tooltips/settings/ImportProducts.txt"));
			startProductImport = new Button("Start import");
			changeSettingsPane.add(startProductImport, 0, row + 1, 2, 1);
			startProductImport.setPrefWidth(232);
			startProductImport.setMinWidth(startIOWidth);
			importProductsListener();
		}

		//Button press, "export users"
		public void exportUsers() {
			stc.showContent = 3;
			stc.setButtonHighlight();
			int row = setupImportExportUI(false, Arrays.copyOfRange(userOptionStrings, 0, userOptionStrings.length - 1), new int[] { 2, 3, 4, 5, 6, 7, 8});
			stc.setToolBar(0, -1, null, true, false, true, DialogManager.loadText("/resources/tooltips/settings/ExportUsers.txt"));
			startUserExport = new Button("Start export");
			changeSettingsPane.add(startUserExport, 0, row + 1, 2, 1);
			startUserExport.setPrefWidth(232);
			startUserExport.setMinWidth(startIOWidth);
			exportUsersListener();

		}

		//Button press, "export products"
		public void exportProducts() {
			stc.showContent = 5;
			stc.setButtonHighlight();
			int row = setupImportExportUI(false, Arrays.copyOfRange(productOptionStrings, 0, productOptionStrings.length - 1), new int[] { 0, 1, 2, 3, 4});
			stc.setToolBar(0, -1, null, true, false, true, DialogManager.loadText("/resources/tooltips/settings/ExportProducts.txt"));
			startProductExport = new Button("Start export");
			changeSettingsPane.add(startProductExport, 0, row + 1, 2, 1);
			startProductExport.setPrefWidth(232);
			startProductExport.setMinWidth(startIOWidth);
			exportProductsListener();
		}

		//Button press, "import users"
		public void importUsers() {
			stc.showContent = 2;
			stc.setButtonHighlight();
			int row = setupImportExportUI(true, userOptionStrings, new int[] { 9, 11, 6, 7, 10, 0, 1});
			stc.setToolBar(0, -1, null, true, false, true, DialogManager.loadText("/resources/tooltips/settings/ImportUsers.txt"));
			startUserImport = new Button("Start import");
			changeSettingsPane.add(startUserImport, 0, row + 1, 2, 1);
			startUserImport.setPrefWidth(232);
			startUserImport.setMinWidth(startIOWidth);
			importUsersListener();

		}
}
