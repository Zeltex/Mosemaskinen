package controllerAssistants;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType0Font;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import controllers.MainTabController;
import controllers.SettingsTabController;
import database.DatabaseHandler;
import database.GlobalVariables;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Dimension2D;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import managers.DialogManager;
import objects.Product;
import objects.User;

public class BarCodeExport {
	// int[] textSize, barCodeSize;
	public List<TextField> barCodeText;
	List<TextField>	barCodeSizes;
	final String[][] barCodeOptions = new String[][]{{"Bar code number","Bar code","First & Last name", "Name",
		"Calling name","Rank","Team","Rustur","Gender"},{"Bar code number","Bar code","Name",
			"Category","Price"}};
	public List<List<ComboBox<String>>> barCodeCombo;
	List<List<Integer>> defaultOptions;
	int[] textSize, barCodeSize;
	String[] titles;
	ToolBar[] barCodeToolBar;
	String[] barCodeToolTips;
	Button exportBarCodes;
	TextField barCodeGuest;
	float gridYOffset;
	
	GridPane changeSettingsPane;
	SettingsTabAssistant sta;
	MainTabController mtc;
	DatabaseHandler dbHandler;
	SettingsTabController stc;
	
	public BarCodeExport(GridPane changeSettingsPane, SettingsTabAssistant sta, MainTabController mtc, 
			DatabaseHandler dbHandler, SettingsTabController stc) {
		this.changeSettingsPane = changeSettingsPane;
		this.sta = sta;
		this.mtc = mtc;
		this.dbHandler = dbHandler;
		this.stc = stc;
	}
	
	private void setDefaultBarCodeVariables() {
		textSize = new int[]{10,16,16};
		barCodeSize = new int[]{45,52,52};
		titles = new String[]{"Users","Products","Multipliers"};
		barCodeToolBar = new ToolBar[3];
		barCodeCombo = new ArrayList<List<ComboBox<String>>>();
		barCodeText = new ArrayList<TextField>();
		for (int i = 0; i < 3; i ++) {
			barCodeToolBar[i] = new ToolBar();
			barCodeCombo.add(new ArrayList<ComboBox<String>>());
		}
		defaultOptions = new ArrayList<List<Integer>>();
		Integer[] tempDefault = new Integer[]{0,1,2};
		defaultOptions.add(new ArrayList<Integer>(Arrays.asList(tempDefault)));
		tempDefault = new Integer[]{2,1};
		defaultOptions.add(new ArrayList<Integer>(Arrays.asList(tempDefault)));
		tempDefault = new Integer[]{0,2,3,4,6,12,15,24,30};
		defaultOptions.add(new ArrayList<Integer>(Arrays.asList(tempDefault)));
		barCodeSizes = new ArrayList<TextField>();
		barCodeToolTips = new String[]{};
	}

	public void deletePressed(Node node) {
		if (barCodeCombo.get(0).contains(node)) {
			barCodeCombo.get(0).remove(node);
		} else if (barCodeCombo.get(1).contains(node)) {
			barCodeCombo.get(1).remove(node);
		} else if (barCodeText.contains(node)) {
			barCodeText.remove(node);
		}	packBarCodes();

	}
	
	public void addPressed(int id) {
		if (id == 0 || id == 1) {
			ComboBox<String> k = new ComboBox<String>();
			k.getItems().addAll(barCodeOptions[id]);
			k.setValue("");
			k.setMaxWidth(175);
			k.setMinWidth(175);
			barCodeCombo.get(id).add(k);
		} else if (id == 2) {
			TextField t = new TextField();
			t.setMaxWidth(175);
			sta.setIntegerRestriction(t);
			barCodeText.add(t);
		}
		packBarCodes();
	}
	
		private void setupBarCodesBasic(int which) {
			String tooltip = DialogManager.loadText("/resources/tooltips/settings/BarCodes" + which + ".txt");
			stc.setToolBar(0, which, barCodeToolBar[which], true, false, which == 1 ? false : true, tooltip);
			changeSettingsPane.getChildren().remove(barCodeToolBar[which]);
			int width = 170;
			// 0 = Users, 1 = Products
			if (which == 0 || which == 1) {
			for (int i : defaultOptions.get(which)) {
				ComboBox<String> k = new ComboBox<String>();
				k.getItems().addAll(barCodeOptions[which]);
				k.setValue(barCodeOptions[which][i]);
				k.setMaxWidth(width);
				k.setMinWidth(width);
				barCodeCombo.get(which).add(k);
			}
			// 2 = Multipliers
			} else if (which == 2) {
				for (int i : defaultOptions.get(which)) {
					TextField t = new TextField(i + "");
					t.setMaxWidth(width);
					sta.setIntegerRestriction(t);
					barCodeText.add(t);
				}
			}
			TextField text = new TextField(textSize[which] + ""), barCode = new TextField(barCodeSize[which] + "");
			sta.setIntegerRestriction(text);
			sta.setIntegerRestriction(barCode);
			text.setMaxWidth(width);
			barCode.setMaxWidth(width);
			barCodeSizes.add(text);
			barCodeSizes.add(barCode);
			exportBarCodes = new Button("Export bar codes");
			exportBarCodes.setMinWidth(278);
			exportBarCodes.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent event) {
					File directory = DialogManager.getUserChosenDirectory(new File(GlobalVariables.databasePath).getParent());
					if (directory == null) return;
					int[] sizes = new int[6];
					int index = 0;
					boolean barCodePresentTemp = false, barCodePresent = true;
					for (TextField t : barCodeSizes) {
						if (t.getText() == null) {DialogManager.showPopup("Empty size field");return;}
						if (t.getText().isEmpty()) {DialogManager.showPopup("Empty size field");return;}
						sizes[index] = Integer.parseInt(t.getText());
						index ++;
					}
					ArrayList<String> users = new ArrayList<String>();
					for (ComboBox<String> t : barCodeCombo.get(0)) {
						if (t.getValue().isEmpty()) continue; 
						if (users.contains(t.getValue())) DialogManager.showPopup("Duplicate user field");
						else users.add(t.getValue());
						if (t.getValue().equals("Bar code")) barCodePresentTemp = true;
					}
					if (!barCodePresentTemp) barCodePresent = false;
					ArrayList<String> products = new ArrayList<String>();
					for (ComboBox<String> t : barCodeCombo.get(1)) {
						if (t.getValue().isEmpty()) continue; 
						if (products.contains(t.getValue())) DialogManager.showPopup("Duplicate product field");
						else products.add(t.getValue());
						if (t.getValue().equals("Bar code")) barCodePresentTemp = true;
					}
					if (!barCodePresentTemp) barCodePresent = false;
					ArrayList<String> multipliers = new ArrayList<String>();
					for (TextField t : barCodeText) {
						if (t.getText().isEmpty()) continue; 
						if (multipliers.contains(t.getText())) DialogManager.showPopup("Duplicate multiplier field");
						else multipliers.add(t.getText());
					}
					if (!barCodePresent) {
						if (!DialogManager.alertConfirmation("Missing bar code", null, "Either the user or product export is missing the scannable 'Bar code' field.\n"
								+ "Are you sure you want to continue?")) {
							return;
						}
					}
					int guests = (barCodeGuest.getText().isEmpty() ? 0 : Integer.parseInt(barCodeGuest.getText()));
					executeExportBarCodes(sizes, users, products, multipliers, directory, guests);
				}
			});
			barCodeGuest = new TextField("10");
			sta.setIntegerRestriction(barCodeGuest);
		}
		
		private int packBarCodesChild(int index, int which) {
			Label title = new Label(titles[which]);
			if (!changeSettingsPane.getChildren().contains(exportBarCodes)) {
				changeSettingsPane.add(exportBarCodes,0,0,4,1);
			}
			changeSettingsPane.add(title, 0, index);
			changeSettingsPane.add(barCodeToolBar[which], 1, index, 2, 1);
			index += 1;
			int valueIndex = 1;
			if (which == 0 || which == 1) {
				for (ComboBox<String> c : barCodeCombo.get(which)) {
					if (which != 1) stc.addDelete(c, index - 1, 2);
					changeSettingsPane.add(c, 1, index);
					Label label = new Label("Value " + valueIndex);
					label.setMinWidth(89);
					changeSettingsPane.add(label, 0, index);
					index ++; valueIndex ++;
				}
			} else if (which == 2) {
				for (TextField t : barCodeText) {
					stc.addDelete(t, index - 1, 2);
					changeSettingsPane.add(t, 1, index);
					Label label = new Label("Value " + valueIndex);
					label.setMinWidth(89);
					changeSettingsPane.add(label, 0, index);
					index ++; valueIndex ++;
				}
			}
			Label textLabel = new Label("Text size"), barCodeLabel = new Label("Barcode size");
			changeSettingsPane.add(textLabel, 0, index);
			changeSettingsPane.add(barCodeLabel, 0, index + 1);
			changeSettingsPane.add(barCodeSizes.get(which * 2), 1, index);
			changeSettingsPane.add(barCodeSizes.get(which * 2 + 1), 1, index + 1);
			index +=2;
			Pane spacing = new Pane();
			spacing.setMinHeight(50);
			changeSettingsPane.add(spacing, 0, index);
			index ++;
			
			return index;
		}
		
		public void packBarCodes() {
			changeSettingsPane.getChildren().clear();
			int index = 1;
			for (int i = 0; i < 3; i ++) {
				index = packBarCodesChild(index, i);
			}
			changeSettingsPane.add(new Label("Guests"), 0, index);
			changeSettingsPane.add(barCodeGuest, 1, index);
		}

		public void exportBarCodes() {
			
			setDefaultBarCodeVariables();
			for (int i = 0; i < 3; i ++) {
				setupBarCodesBasic(i);
			}
			packBarCodes();
		}
		
		private void executeExportBarCodes(int[] sizes, ArrayList<String> users, ArrayList<String> products, ArrayList<String> multipliers, File directoryFile, int guests) {
			String directory = directoryFile.getAbsolutePath();
			directory += "\\MosemaskinenBarCodes";
			File f = new File(directory);
			f.mkdirs();
			if (!exportBarCodesChild(getUserBarCodeExportText(users), getBarCodeIndex(users), PDType1Font.COURIER, sizes[0], sizes[1], directory + "\\UserBarCodes.pdf", false)) {
				DialogManager.showPopup("Failed to export bar codes");
			} else if (!exportBarCodesChild(getProductBarCodeExportText(products), getBarCodeIndex(products), PDType1Font.COURIER, sizes[2], sizes[3], directory + "\\ProductBarCodes.pdf", true)) {
				DialogManager.showPopup("Failed to export bar codes");
			} else if (!exportBarCodesChild(getMultiplierBarCodeExportText(multipliers), 1, PDType1Font.COURIER, sizes[4], sizes[5], directory + "\\MultiplierBarCodes.pdf", true)) {
				DialogManager.showPopup("Failed to export bar codes");
			} else if (!exportBarCodesChild(getGuests(guests), 1, PDType1Font.COURIER, sizes[0], sizes[1], directory + "\\GuestBarCodes.pdf", false)) {
				DialogManager.showPopup("Failed to export bar codes");
//			} else if (!exportBarCodesChild(getTestBarCodeExportText(), 1, PDType1Font.COURIER, 16, 34, directory + "\\TestBarCodes.pdf", true)) {
//				DialogManager.showPopup("Failed to export bar codes");
					
				
			} else {
				DialogManager.showPopup("Successfully exported bar codes");
			}
			
		}
		
		private int getBarCodeIndex(ArrayList<String> entries) {
			int index = 0;
			for (String s : entries) {
				if (s.length() < 2) {
					continue;
				} else {
					if (s.equals("Bar code")) {
						return index;
					}
				}
				index ++;
			}
			return 0;
		}
		
		private ArrayList<ArrayList<String>> getGuests(int guests) {
			Long barCode = dbHandler.getFreeUserBarCodeNumber();
			ArrayList<ArrayList<String>> result = new ArrayList<ArrayList<String>>();
			for (int i = 0; i < guests; i ++) {
				ArrayList<String> temp = new ArrayList<String>();
				temp.add(barCode + "");
				temp.add("*" + barCode + "*");
				temp.add("Guest #" + i);
				result.add(temp);
				barCode ++;
			}
			return result;
		}
		
		private ArrayList<ArrayList<String>> getUserBarCodeExportText(ArrayList<String> entries) {
			ArrayList<ArrayList<String>> text = new ArrayList<ArrayList<String>>();
			for (User user : dbHandler.getUsers()) {
				ArrayList<String> temp = new ArrayList<String>();
				for (String s : entries) {
					switch(s) {
					case  "Bar code number": temp.add(user.getBarCode() + ""); break;
					case "Bar code": temp.add("*" + user.getBarCode() + "*"); break;
					case "First & Last name": {
						String[] split = user.getName().split("\\s+");
						if (split.length == 1) temp.add(split[0]);
						else temp.add(split[0] + " " + split[split.length - 1]);
					} break;
					case "Name": temp.add(user.getName()); break;
					case "Calling name": temp.add(user.getCallingName()); break;
					case "Rank": temp.add(user.getRank());break;
					case "Team": temp.add(user.getTeam()); break;
					case "Rustur": temp.add(user.getRustur());break;
					case "Gender": temp.add(user.getGender()); break;
					}
				}
				text.add(temp);
			}
			return text;
		}
		private ArrayList<ArrayList<String>> getProductBarCodeExportText(ArrayList<String> entries) {
			ArrayList<ArrayList<String>> text = new ArrayList<ArrayList<String>>();
			for (Product product : dbHandler.getProducts()) {
				ArrayList<String> temp = new ArrayList<String>();
				for (String s : entries) {
					switch(s) {
					case  "Bar code number": temp.add(product.getBarCode() + ""); break;
					case  "Bar code": temp.add("*" + product.getBarCode() + "*"); break;
					case  "Name": temp.add(product.getName()); break;
					case  "Category": temp.add(product.getCategory()); break;
					case  "Price": temp.add(product.getPrice() + ""); break;
					}
				}
				text.add(temp);
			}
			return text;
		}
		private ArrayList<ArrayList<String>> getMultiplierBarCodeExportText(ArrayList<String> multipliers) {
			ArrayList<ArrayList<String>> text = new ArrayList<ArrayList<String>>();
			for (String s : multipliers) {
				ArrayList<String> temp = new ArrayList<String>();
				temp.add((s.equals("0") ? "Cancel product" : s));
				temp.add("*" + (s.length() > 1 ? "" : "0") + s + "*");
				text.add(temp);
			}
			return text;
		}
		@SuppressWarnings("unused")
		private ArrayList<ArrayList<String>> getTestBarCodeExportText() {
			ArrayList<ArrayList<String>> text = new ArrayList<ArrayList<String>>();
			for (int i = 0; i < 60; i ++) {
				ArrayList<String> temp = new ArrayList<String>();
				temp.add(i + "");
				temp.add("*1254*");
				text.add(temp);
			}
			return text;
		}
		
		// Button press "export bar codes"
		private boolean exportBarCodesChild(ArrayList<ArrayList<String>> text, int barCodeIndex, PDFont textFont, int textSize, int barCodeSize, String path, boolean notGrid) {
			PDPageContentStream contentStream = null;
			try (PDDocument document = new PDDocument();){
				PDFont barCodeFont = PDType0Font.load(document, this.getClass().getResourceAsStream("/resources/images/BarCode.TTF"));
				PDPage page = new PDPage();
				document.addPage( page );

			contentStream = new PDPageContentStream(document, page);
			float maxX = 610, maxY = 770;
			float x = (notGrid ? maxX / 2 : 0) ,y = maxY;
//			float width = (float) (maxX / Math.floor(maxX / (getMaxWidth(nameFont, nameFontSize) + 10))), height = 100;
			Dimension2D d = getCellDimension(text, barCodeIndex, textFont, barCodeFont, textSize, barCodeSize, maxX);
			float width = (float) d.getWidth(), height = (float) d.getHeight();
//			if (debug) {
//				width = maxX /3 - 5;
//				height = maxY / 9;
//			}
				boolean first = true;
				float entryHeight = 0;
				for (ArrayList<String> entry : text) {
					if (!first) {
						if (notGrid) {
							y -= height;
						} else {
							x += width;
						}
					} else
						first = false;
					// New line
					if (x + width > maxX || notGrid) {
						if (!notGrid) {
							x = 0;
							y -= height;
						}
						// New page
						if (y < entryHeight) {
							contentStream.close();
							page = new PDPage();
							document.addPage(page);
							contentStream = new PDPageContentStream(document, page);
							y = maxY;
						}
					}
					// Print info
					float tempY = y;
					for (int i = 0; i < entry.size(); i++) {
						if (i == barCodeIndex) {
							tempY = printLine(contentStream, barCodeFont, barCodeSize, entry.get(i),
									x + (notGrid ? 15 : center(width, entry.get(i), barCodeFont, barCodeSize)), notGrid ? y :tempY);
						} else {
							tempY = printLine(contentStream, textFont, textSize, entry.get(i),
									x + (notGrid ? rightAlign(entry.get(i), textFont, textSize) : center(width, entry.get(i), textFont, textSize)), notGrid ? y - gridYOffset : tempY);
						}
					}
					entryHeight = y - tempY;
				}
			contentStream.close();
			document.save(path);
			return true;
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			} 
		}
		
		private float rightAlign(String text, PDFont font, int fontSize) throws IOException {
			float textWidth = (font.getStringWidth(text) / 1000f) * fontSize;
			return  -textWidth - 15;
		}
		
		public float printLine(PDPageContentStream contentStream, PDFont font, int fontSize, String text, float x, float y) throws IOException {
			int verticalOffset = 8;
			float textHeight = font.getFontDescriptor().getFontBoundingBox().getHeight() / 1000 * fontSize;
			contentStream.beginText();
			contentStream.setFont( font, fontSize );
			contentStream.newLineAtOffset( x, (float) (y - verticalOffset - textHeight));
			contentStream.showText(text);
			contentStream.endText();
			return y - verticalOffset - textHeight;
		}
		
		public Dimension2D getCellDimension(PDFont font, int fontSize, float maxX, float maxY) throws IOException {
			float max = 0;
			for (User user : dbHandler.getUsers()) {
				float textWidth = (font.getStringWidth(user.getName()) / 1000f) * fontSize;
				if (textWidth > max) max = textWidth;
			}
			float width = (float) (maxX / Math.floor(maxX / (max + 10)));
			return  new Dimension2D(width, 100);
//			return new Dimension(width, 100);
		}
		
		public Dimension2D getCellDimension(ArrayList<ArrayList<String>> text, int barCodeIndex, PDFont textFont, PDFont barCodeFont, int textSize, int barCodeSize, float maxX) throws IOException {
			int internalVerticalOffset = 8, externalVerticalOffset = 15;
			float max = 0, height = 0;
			for (ArrayList<String> entry : text) {
				for (int i = 0; i < entry.size(); i ++) {
					float textWidth = 0;
					if (i == barCodeIndex) textWidth = (barCodeFont.getStringWidth(entry.get(i)) / 1000f) * barCodeSize;
					else textWidth = (textFont.getStringWidth(entry.get(i)) / 1000f) * textSize;
					if (textWidth > max) max = textWidth;
				}
			}
			float tHeight = 0, bHeight = 0;
			if (text.size() > 0) {
				for (int i = 0; i < text.get(0).size(); i ++) {
					float textHeight = 0;
					if (i == barCodeIndex) {
						textHeight = barCodeFont.getFontDescriptor().getFontBoundingBox().getHeight() / 1000 * barCodeSize;
						bHeight = textHeight;
					} else {
						textHeight = textFont.getFontDescriptor().getFontBoundingBox().getHeight() / 1000 * textSize;
						tHeight = textHeight;
					}
					height += textHeight + internalVerticalOffset;
					
				}
				height -= internalVerticalOffset;
				height += externalVerticalOffset;
			}
			gridYOffset = Math.abs((bHeight - tHeight) / 2);
			float width = (float) (maxX / Math.floor(maxX / (max + 10)));
			return  new Dimension2D(width, height);
		}
		
		public float center(double width, String text, PDFont font, int fontSize) throws IOException {
			float textWidth = (font.getStringWidth(text) / 1000f) * fontSize;
			return (float) ((width - textWidth) / 2);
		}
}
