package managers;

import java.util.ArrayList;

import controllers.UserTabController;
import database.DatabaseHandler;
import database.GlobalVariables;
import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

public class MultiEntrySaver {
	DatabaseHandler dbHandler;
	public void init (DatabaseHandler dbHandler) {
		this.dbHandler = dbHandler;
	}
	
	public boolean confirmGRT(ArrayList<Integer> noneEmpty, ArrayList<ComboBox<String>> userInfoCombo, DatabaseHandler dbHandler, UserTabController utc) {
		for (int i = 0; i < noneEmpty.size(); i++) {
			switch (noneEmpty.get(i)) {
			case 4:
				if (!userMultiConfirmRank(userInfoCombo.get(0), dbHandler, utc)) return false;
				break;
			case 5:
				if (!userMultiConfirmTeam(userInfoCombo.get(1), dbHandler, utc)) return false;
				break;
			
			case 10:
				if (!userMultiConfirmGender(userInfoCombo.get(2).getValue(), dbHandler)) return false;
				break;

			}
		}
	return true;
	}
	
	public boolean userMultiConfirmRank(ComboBox<String> combo, DatabaseHandler dbHandler, UserTabController utc) {
		String text = combo.getValue();
		if (text == null) return false;
		if (!GlobalVariables.confirmRank(text) && !text.isEmpty()) {
			boolean answer = DialogManager.alertConfirmation("Unknown rank", null, "The given rank " + '"'
					+ text + '"' + "is not registered. Do you want to register it?");
			if (answer) {
				GlobalVariables.addRank(text);
				if (!dbHandler.editSettings("ranks", GlobalVariables.getSaveStrings("ranks"))) {
					return false;
				}
				utc.updateComboBoxes();
				combo.setValue(text);
			} else {
				return false;
			}
		} 
		return true;
	}
	public boolean userMultiConfirmTeam(ComboBox<String> combo, DatabaseHandler dbHandler, UserTabController utc) {
		String text = combo.getValue();
		if (text == null) return false;
		if (!GlobalVariables.confirmTeam(text) && !text.isEmpty()) {
			boolean answer = DialogManager.alertConfirmation("Unknown team", null, "The given team " + '"'
					+ text + '"' + "is not registered. Do you want to register it?");
			if (answer) {
				GlobalVariables.addTeam(text);
				if (!dbHandler.editSettings("teams", GlobalVariables.getSaveStrings("teams"))) {
					return false;
				}
				utc.updateComboBoxes();
				combo.setValue(text);
			} else {
				return false;
			}
		} 
		return true;
	}
	public boolean userMultiConfirmGender(String text, DatabaseHandler dbHandler) {
		if (text == null) return false;
		if (!GlobalVariables.confirmGender(text)) {
			return false;
		} 
		return true;
	}
	
	public ArrayList<String> getUserChanges(ArrayList<Integer> noneEmpty, ObservableList<Integer> indices, ArrayList<ComboBox<String>> userInfoCombo, TextField[] userInfoText) {
		ArrayList<String> changes = new ArrayList<String>();
			for (int i = 0; i < noneEmpty.size(); i++) {
				switch (noneEmpty.get(i)) {
				case 1:
					userMultiChangeName(changes, indices.size(), userInfoText[0].getText());
					break;
				case 2:
					userMultiChangeCallingName(changes, indices.size(), userInfoText[1].getText());
					break;
				case 4:
					userMultiChangeRank(changes, indices.size(), userInfoCombo.get(0).getValue());
					break;
				case 5:
					userMultiChangeTeam(changes, indices.size(), userInfoCombo.get(1).getValue());
					break;
				case 6:
					userMultiChangeStudyNumber(changes, indices.size(), userInfoText[3].getText());
					break;
				case 7:
					userMultiChangeRustur(changes, indices.size(), userInfoText[4].getText());
					break;
				case 10:
					userMultiChangeGender(changes, indices.size(), userInfoCombo.get(2).getValue());
					break;
				case 11:
					userMultiChangeBarCode(changes, indices.size(), userInfoText[7].getText());
					break;
				case 12:
					userMultiChangeStudy(changes, indices.size(), userInfoText[8].getText());
					break;

				}
			}
		return changes;
	}
	
	
	
	public void userMultiChangeName(ArrayList<String> changes, int amount, String text) {
		boolean confirm = DialogManager.alertConfirmation("Renaming users", null, 
				"You are about to rename " + amount + " users to the same name: " + text +
				". In most cases this is impractical, are you sure you want to do it?");
		if (confirm) {
			changes.add("name"); 
			changes.add(text);
		}
	}
	public void userMultiChangeCallingName(ArrayList<String> changes, int amount, String text) {
		boolean confirm = DialogManager.alertConfirmation("Renaming users", null, 
				"You are about to rename " + amount + " users to the same name: " + text +
				". In most cases this is impractical, are you sure you want to do it?");
		if (confirm) {
			changes.add("callingName"); 
			changes.add(text);
		}
	}
	public void userMultiChangeRank(ArrayList<String> changes, int amount, String text) {
		if (text == null) return;
		if (!GlobalVariables.confirmRank(text)) {
			boolean answer = DialogManager.alertConfirmation("Unknown rank", null, "The given rank " + '"'
					+ text + '"' + "is not registered. Do you want to register it?");
			if (answer) {
				GlobalVariables.addRank(text);
				dbHandler.editSettings("ranks", GlobalVariables.getSaveStrings("ranks"));
			} else {
				return;
			}
		} 
//		boolean confirm = DialogManager.alertConfirmation("Ranking users", null, 
//				"You are about to set the rank of " + amount + " users to the same rank: " + text +
//				". Are you sure?");
//		if (confirm) {
			changes.add("rank"); 
			changes.add(text);
//		}
	}
	public void userMultiChangeTeam(ArrayList<String> changes, int amount, String text) {
		if (text == null) return;
		if (!GlobalVariables.confirmTeam(text)) {
			boolean answer = DialogManager.alertConfirmation("Unknown team", null, "The given team " + '"'
					+ text + '"' + "is not registered. Do you want to register it?");
			if (answer) {
				GlobalVariables.addTeam(text);
				dbHandler.editSettings("teams", GlobalVariables.getSaveStrings("teams"));
				
			} else {
				return;
			}
			
		} 
//		boolean confirm = DialogManager.alertConfirmation("Setting team", null, 
//				"You are about to set the team of " + amount + " users to the same team: " + text +
//				". Are you sure?");
//		if (confirm) {
			changes.add("team"); 
			changes.add(text);
//		}
	}
	public void userMultiChangeStudyNumber(ArrayList<String> changes, int amount, String text) {
		boolean confirm = DialogManager.alertConfirmation("Setting study number", null, 
				"You are about to set study number of " + amount + " users to the same study number: " + text +
				". In most cases this is impractical, are you sure you want to do it?");
		if (confirm) {
			changes.add("studyNumber"); 
			changes.add(text);
		}
	}
	public void userMultiChangeRustur(ArrayList<String> changes, int amount, String text) {
//		boolean confirm = DialogManager.alertConfirmation("Setting rustur", null, 
//				"You are about to set rustur of " + amount + " users to the same rustur: " + text +
//				". Are you sure?");
//		if (confirm) {
			changes.add("rustur"); 
			changes.add(text);
//		}
	}
	public void userMultiChangeGender(ArrayList<String> changes, int amount, String text) {
		if (text == null) return;
		if (!GlobalVariables.confirmGender(text)) {
			DialogManager.alertError("Unknown gender", null, "Gender wasn't recognized, and not saved.");
			return;
		} 
//		boolean confirm = DialogManager.alertConfirmation("Setting gender", null, 
//				"You are about to set the gender of " + amount + " users to the same gender: " + text +
//				". Are you sure?");
//		if (confirm) {
			changes.add("gender"); 
			changes.add("" + GlobalVariables.getGenderInt(text));
//		}
	}
	public void userMultiChangeBarCode(ArrayList<String> changes, int amount, String text) {
		boolean confirm = DialogManager.alertConfirmation("Setting barCode", null, 
				"You are about to set bar code of " + amount + " users to the same bar code: " + text +
				".  Barcodes have to be unique, so this is very impratical");
		if (confirm) {
			changes.add("barCode"); 
			changes.add(text);
		}
	}
	
	public void userMultiChangeStudy(ArrayList<String> changes, int amount, String text) {
//		boolean confirm = DialogManager.alertConfirmation("Setting Study", null, 
//				"You are about to set bar code of " + amount + " users to the same bar code: " + text +
//				".  Barcodes have to be unique, so this is very impratical");
//		if (confirm) {
			changes.add("study"); 
			changes.add(text);
//		}
	}
	
	public ArrayList<String> getProductChanges(ArrayList<Integer> noneEmpty, ObservableList<Integer> indices, ArrayList<ComboBox<String>> productInfoCombo, TextField[] productInfoText) {
		ArrayList<String> changes = new ArrayList<String>();
			for (int i = 0; i < noneEmpty.size(); i++) {
				switch (noneEmpty.get(i)) {
				case 2:
					productMultiChangeName(changes, indices.size(), productInfoText[1].getText());
					break;
				case 3:
					productMultiChangeTotalStock(changes, indices.size(), productInfoText[2].getText());
					break;
				case 4:
					productMultiChangeCurrentStock(changes, indices.size(), productInfoText[3].getText());
					break;
				case 6:
					productMultiChangeCategory(changes, indices.size(), productInfoCombo.get(0).getValue());
					break;
				case 7:
					productMultiChangePrice(changes, indices.size(), productInfoText[5].getText());
					break;
				}
			}
		return changes;
	}
	public void productMultiChangeName(ArrayList<String> changes, int amount, String text) {
		boolean confirm = DialogManager.alertConfirmation("Setting name", null, 
				"You are about to set the name of " + amount + " prodcuts to the same name: " + text +
				".  In most cases this is impractical, are you sure you want to do it?");
		if (confirm) {
			changes.add("name"); 
			changes.add(text);
		}
	}
	public void productMultiChangeTotalStock(ArrayList<String> changes, int amount, String text) {
		boolean confirm = DialogManager.alertConfirmation("Setting total stock", null, 
				"You are about to set the total stock of " + amount + " products to the same total stock: " + text +
				".  In most cases this is impractical, are you sure you want to do it?");
		if (confirm) {
			changes.add("totalStock"); 
			changes.add(text);
		}
	}
	public void productMultiChangeCurrentStock(ArrayList<String> changes, int amount, String text) {
		boolean confirm = DialogManager.alertConfirmation("Setting current stock", null, 
				"You are about to set the current stock of " + amount + " products to the same current stock: " + text +
				".  In most cases this is impractical, are you sure you want to do it?");
		if (confirm) {
			changes.add("currentStock"); 
			changes.add(text);
		}
	}
	public void productMultiChangeCategory(ArrayList<String> changes, int amount, String text) {
		if (text == null) return;
//		if (!GlobalVariables.confirmCategory(text)) {
//			boolean registerIt = DialogManager.alertConfirmation("Unknown category", null, "The given category " + '"'
//					+ text + '"' + "is not registered. Do you want to register it?");
//			if (registerIt) {
//				
//			}
//		} 
		boolean confirm = DialogManager.alertConfirmation("Setting category", null, 
				"You are about to set the category of " + amount + " products to the same category: " + text +
				". Are you sure?");
		if (confirm) {
			changes.add("category"); 
			changes.add(text);
		}
	}
	public void productMultiChangePrice(ArrayList<String> changes, int amount, String text) {
		boolean confirm = DialogManager.alertConfirmation("Setting price", null, 
				"You are about to set price of " + amount + " products to the same price: " + text +
				".  Are you sure?");
		if (confirm) {
			changes.add("price"); 
			changes.add(text);
		}
	}
}
