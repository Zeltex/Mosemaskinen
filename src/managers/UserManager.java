package managers;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import database.DatabaseHandler;
import database.GlobalVariables;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListView;
import objects.User;
import objects.UserProduct;

public class UserManager {
	
	ArrayList<User> users;
	ArrayList<Integer> shownUsers;
	DatabaseHandler dbHandler;
	
	public UserManager(DatabaseHandler dbHandler) {
		users = new ArrayList<User>();
		shownUsers = new ArrayList<Integer>();
		this.dbHandler = dbHandler;
	}
	
	public String getUserNameFromID(String ID) {
		for (User user : users) {
			if (user.getID().equals(ID)) return user.getName();
		}
		return null;
	}
	
	public int getShownUserIndex(String ID) {
		for (int i = 0; i < shownUsers.size(); i ++) {
			if (users.get(shownUsers.get(i)).getID().equals(ID)){
				return i;
			}
		}
		return -1;
	}
	
	public void setUsers(ArrayList<User> users) {
		ArrayList<String> unregisteredRanks = new ArrayList<String>();
		ArrayList<String> unregisteredTeams = new ArrayList<String>();
		ArrayList<String> ranks = GlobalVariables.getRanks();
		ArrayList<String> teams = GlobalVariables.getTeams();
		for (User user : users) {
			if (!ranks.contains(user.getRank()) && !user.getRank().isEmpty()) {
				if(!unregisteredRanks.contains(user.getRank())) {
					unregisteredRanks.add(user.getRank());
				}
			}
			if (!teams.contains(user.getTeam()) && !user.getTeam().isEmpty()) {
				if (!unregisteredTeams.contains(user.getTeam())) {
					unregisteredTeams.add(user.getTeam());
				}
			}
		}
		if (unregisteredRanks.size() + unregisteredTeams.size() > 0) {
			boolean confirm = DialogManager.alertConfirmation("Unregistered ranks and teams", null, "Found " + 
					unregisteredRanks.size() + " unregistered ranks, and " + unregisteredTeams.size() + 
					" unregistered teams. Do you want to register them, or else they will be set to 'Unknown'.");
			if (confirm) {
				GlobalVariables.addRank(unregisteredRanks);
				GlobalVariables.addTeam(unregisteredTeams);
				dbHandler.editSettings("ranks", GlobalVariables.getSaveStrings("ranks"));
				dbHandler.editSettings("teams", GlobalVariables.getSaveStrings("teams"));
				dbHandler.updateUserComboBoxes();
				
			} else {
				for (User user : users) {
					if (!ranks.contains(user.getRank()) && !user.getRank().isEmpty()) 
						user.setRank("Unknown");
					if (!teams.contains(user.getTeam()) && !user.getTeam().isEmpty()) 
						user.setTeam("Unknown");
				}
				dbHandler.nullRanks(unregisteredRanks);
				dbHandler.nullTeams(unregisteredTeams);
				//TODO update database
			}
		}
			this.users = users;
		
	}
	
	public void updateUser(ArrayList<String> difference, String id) {
		for (User user : users) {
			if(user.getID().equals(id)) {
				user.update(difference);
				break;
			}
		}
	}
	public void updateUserProductName(String ID, String name) {
		for (User user : users) {
			UserProduct up = user.getProductFromID(ID);
			if (up != null) up.setName(name);
		}
	}
	
	public void updateUserProductNames(ArrayList<String> IDs, String name) {
		for (User user : users) {
			for (String ID : IDs) {
				UserProduct up = user.getProductFromID(ID);
				if (up != null) up.setName(name);
			}
		}
		
	}
	
	public double getProductSold(String ID) {
		double amount = 0;
		for (int i = 0; i < users.size(); i ++) {
			UserProduct up = users.get(i).getProductFromID(ID);
			if (up != null) amount += up.getAmount();
		}
		return amount;
		
	}
	
	public void updateUserListType(ListView<User> userList, boolean realNames, int sort) {
		List<User> stringList = new ArrayList<>();
		ObservableList<User> observableList = FXCollections.observableArrayList();
		for (int i : shownUsers) {
			String name;
			if (realNames) name = users.get(i).getName();
			else name = users.get(i).getCallingName();
			
			String entry = getExtraUserInfoString(users.get(i), sort, realNames);
			users.get(i).setWhatToShow(entry + name);
			stringList.add(users.get(i));
		}
		observableList.setAll(stringList);

		userList.getItems().clear();
		userList.setItems(observableList);
	}
	
	public void changeDirection(ListView<User> userList, boolean realNames, int sort) {
		List<User> stringList = new ArrayList<>();
		ObservableList<User> observableList = FXCollections.observableArrayList();
		
		
		ArrayList<Integer> temp = new ArrayList<Integer>(shownUsers.size());
		for (int i = 0; i < shownUsers.size(); i ++) {
			temp.add(shownUsers.get(shownUsers.size() - i - 1));
		}
		shownUsers = new ArrayList<Integer>();
		for (int i = 0; i < temp.size(); i ++) {
			shownUsers.add(temp.get(i));
			String name;
			if (realNames) name = users.get(shownUsers.get(i)).getName();
			else name = users.get(shownUsers.get(i)).getCallingName();
			
			String entry = getExtraUserInfoString(users.get(shownUsers.get(i)), sort, realNames);
			users.get(shownUsers.get(i)).setWhatToShow(entry + name);
			stringList.add(users.get(shownUsers.get(i)));
			
		}
		
		observableList.setAll(stringList);

		userList.getItems().clear();
		userList.setItems(observableList);
	}
	
	public void updateUserList(ListView<User> userList, String filter, boolean realNames, int sort) {
		if (users != null) {
			List<User> stringList = new ArrayList<>();
			ObservableList<User> observableList = FXCollections.observableArrayList();
			boolean isFiltered = !filter.equals("");
			shownUsers = new ArrayList<Integer>();
			filter = filter.toLowerCase();
			for (int i = 0; i < users.size(); i ++) {
				if (!isFiltered || users.get(i).getName().toLowerCase().contains(filter) || 
						users.get(i).getCallingName().toLowerCase().contains(filter)) {
					String name;
					if (realNames) name = users.get(i).getName();
					else name = users.get(i).getCallingName();
					
					String entry = getExtraUserInfoString(users.get(i), sort, realNames);
					users.get(i).setWhatToShow(entry + name);
					stringList.add(users.get(i));
					shownUsers.add(i);
				}
			}
			observableList.setAll(stringList);
			userList.getItems().clear();
			userList.setItems(observableList);
		}
	}
	
	public User getUserByIndex(int index) {
		if (shownUsers != null) {
			if (shownUsers.size() > index  && index >= 0) 
				return users.get(shownUsers.get(index));
		}
		return null;
	}
	
	public User getUserFromBarCode(long barCode) {
		for (User user : users) {
			if (user.getBarCode() == barCode) {
				return user;
			}
		}
		return null;
	}
	
	private String getExtraUserInfoString(User user, int sort, boolean realNames) {
		String entry = "";
		switch(sort) {
		case 1: if (!realNames) entry = user.getName() + " - "; break;
		case 2: if (realNames) entry = user.getCallingName() + " - "; break;
		case 3: entry = user.getRank() + " - "; break;
		case 4: entry = user.getTeam() + " - "; break;
		case 5: entry = user.getStudyNumber() + " - "; break;
		case 6: entry = user.getRustur() + " - "; break;
		case 7: entry = user.getTotalItems() + " - "; break;
		case 8: entry = user.getFavoriteItem() + " - "; break;
		case 9: entry = user.getBarCode() + " - "; break;
		case 10: entry = user.getGender() + " - "; break;
		case 11: entry = user.getStudy() + " - "; break;
		}
		return entry;
	}
	
	public void sortUsers(int sort) {
		switch(sort) {
		case 1: users.sort(Comparator.comparing(User::getName)); break;
		case 2: users.sort(Comparator.comparing(User::getCallingName)); break;
		case 3: users.sort(Comparator.comparing(User::getRank));break;
		case 4: users.sort(Comparator.comparing(User::getTeam));break;
		case 5: users.sort(Comparator.comparing(User::getStudyNumber));break;
		case 6: users.sort(Comparator.comparing(User::getRustur));break;
		case 7: users.sort(Comparator.comparing(User::getTotalItems));break;
		case 8: users.sort(Comparator.comparing(User::getFavoriteItem));break;
		case 9: users.sort(Comparator.comparing(User::getBarCode));break;
		case 10: users.sort(Comparator.comparing(User::getGenderInt));break;
		case 11: users.sort(Comparator.comparing(User::getStudy));break;
		}
	}
	
	public ArrayList<User> getUsers() {
		return users;
	}
	
	public String[] getFilteredUsers() {
		return null;
	}
	
	public User getAUser() {
		if (users.size() > 0) {
			return users.get(0);
		} else return null;
	}
	
	public void addUser(User user) {
		users.add(user);
	}
	public void addUsers(ArrayList<User> users) {
		users.addAll(users);
	}
	
	public void deleteUsers(String[] ID) {
		for (int j = 0; j < ID.length; j ++) {
			for (int i = 0; i < users.size(); i ++) {
				if (users.get(i).getID().equals(ID[j])) users.remove(i);
			}
		}
	}
	
	
}
