package managers;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import database.DatabaseHandler;
import database.GlobalVariables;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ListView;
import objects.Product;

public class ProductManager {
	ArrayList<Product> products;
	ArrayList<Integer> shownProducts;
	DatabaseHandler dbHandler;
	
	public ProductManager(DatabaseHandler dbHandler) {
		this.dbHandler = dbHandler;
		products = new ArrayList<Product>();
		shownProducts = new ArrayList<Integer>();
	}
	
//	public void setProducts(ArrayList<Product> products) {
//		this.products = products;
//	}
	
	public void setProducts(ArrayList<Product> products) {
		ArrayList<String> unregisteredCategories = new ArrayList<String>();
		ArrayList<String> categories = GlobalVariables.getCategories();
		for (Product product : products) {
			if (!categories.contains(product.getCategory()) && !product.getCategory().isEmpty()) {
				if(!unregisteredCategories.contains(product.getCategory())) {
					unregisteredCategories.add(product.getCategory());
				}
			}
		}
		if (unregisteredCategories.size() > 0) {
			boolean confirm = DialogManager.alertConfirmation("Unregistered categories", null, "Found " + 
					unregisteredCategories.size() + " unregistered categories. "
							+ "Do you want to register them, else they will be set to 'Unknown'.");
			if (confirm) {
				GlobalVariables.addCategory(unregisteredCategories);
				
				
			} else {
				for (Product product : products) {
					if (!categories.contains(product.getCategory()) && !product.getCategory().isEmpty()) 
						product.setCategory("Unknown");
				}
				if (!GlobalVariables.getCategories().contains("Unknown")) {
					GlobalVariables.addCategory("Unknown");
				}
				
				dbHandler.nullCategories(unregisteredCategories);
				//TODO update database
			}
			dbHandler.editSettings("categories", GlobalVariables.getSaveStrings("categories"));
			dbHandler.updateProductComboBoxes();
		}
			this.products = products;
		
	}
	
	public int getShownProductIndex(String ID) {
		for (int i = 0; i < shownProducts.size(); i ++) {
			if (products.get(shownProducts.get(i)).getID().equals(ID)){
				return i;
			}
		}
		return -1;
	}
	
	public String getProductNameFromID(String ID) {
		for (int i = 0; i < products.size(); i ++) {
			if (products.get(i).getID().equals(ID)){
				return products.get(i).getName();
			}
		}
		return null;
	}
	
	public void updateSoldProducts(ArrayList<String> ID, ArrayList<Double> amount) {
		for (int i = 0; i < ID.size(); i ++) {
			for (int j = 0; j < products.size(); j ++) {
				if (ID.get(i).equals(products.get(j).getID())) {
					products.get(j).setSold(amount.get(i));
				}
			}
		}
	}
	
	public void updateProduct(ArrayList<String> difference, String id) {
		for (Product product : products) {
			if(product.getID().equals(id)) {
				product.update(difference);
				break;
			}
		}
	}
	
	public void updateProducts(ArrayList<String> difference, ArrayList<String> ids) {
		
		for (Product product : products) {
			for (String id : ids) {
				if(product.getID().equals(id)) {
					product.update(difference);
					break;
				}
			}
		}
	}
	
	public void refreshProductList(ListView<Product> productList, int sort) {
		List<Product> stringList = new ArrayList<>();
		ObservableList<Product> observableList = FXCollections.observableArrayList();
		for (int i : shownProducts) {
			String name = products.get(i).getName();
			
			String entry = getExtraProductInfoString(products.get(i), sort);
			products.get(i).setWhatToShow(entry + name);
			stringList.add(products.get(i));
		}
		observableList.setAll(stringList);
		
		productList.getItems().clear();
		productList.setItems(observableList);
	}
	
	public void updateProductList(ListView<Product> productList, String filter, int sort) {
		if (products != null) {
			List<Product> stringList = new ArrayList<>();
			ObservableList<Product> observableList = FXCollections.observableArrayList();
			
			boolean isFiltered = !filter.equals("");
			shownProducts = new ArrayList<Integer>();
			filter = filter.toLowerCase();
			for (int i = 0; i < products.size(); i ++) {
				if (!isFiltered || products.get(i).getName().toLowerCase().contains(filter)) {
					String entry = getExtraProductInfoString(products.get(i), sort);
					products.get(i).setWhatToShow(entry + products.get(i).getName());
					stringList.add(products.get(i));
					shownProducts.add(i);
				}
			}
			observableList.setAll(stringList);
			productList.getItems().clear();
			productList.setItems(observableList);
		}
	}
	
	public void changeDirection(ListView<Product> productList, int sort) {
		List<Product> stringList = new ArrayList<>();
		ObservableList<Product> observableList = FXCollections.observableArrayList();
		
		ArrayList<Integer> temp = new ArrayList<Integer>(shownProducts.size());
		for (int i = 0; i < shownProducts.size(); i ++) {
			temp.add(shownProducts.get(shownProducts.size() - i - 1));
		}
		shownProducts = new ArrayList<Integer>();
		for (int i = 0; i < temp.size(); i ++) {
			shownProducts.add(temp.get(i));
			String  name = products.get(shownProducts.get(i)).getName();
			
			String entry = getExtraProductInfoString(products.get(shownProducts.get(i)), sort);
			products.get(shownProducts.get(i)).setWhatToShow(entry + name);
			stringList.add(products.get(shownProducts.get(i)));
			
		}
		
		observableList.setAll(stringList);
		productList.getItems().clear();
		productList.setItems(observableList);
	}
	
	private String getExtraProductInfoString(Product product, int sort) {
		String entry = "";
		switch(sort) {
		case 1: entry = ""; break;
		case 2: entry = product.getCurrentStock() + " - "; break;
		case 3: entry = product.getSold() + " - "; break;
		case 4: entry = product.getCategory() + " - "; break;
		case 5: entry = product.getPrice() + " - "; break;
		case 6: entry = product.getBarCode() + " - "; break;
		}
		return entry;
	}
	
	public void sortProducts(int sort) {
		switch(sort) {
		case 1: products.sort(Comparator.comparing(Product::getName));break;
		case 2: products.sort(Comparator.comparing(Product::getCurrentStock));break;
		case 3: products.sort(Comparator.comparing(Product::getSold));break;
		case 4: products.sort(Comparator.comparing(Product::getCategory));break;
		case 5: products.sort(Comparator.comparing(Product::getPrice));break;
		case 6: products.sort(Comparator.comparing(Product::getBarCode));break;
		}
	}
	
	public Product getProductByIndex(int index) {
		if (shownProducts != null) {
			if (shownProducts.size() > index  && index >= 0) 
				return products.get(shownProducts.get(index));
		}
		return null;
	}
	public Product getProductFromID(String ID) {
		for (Product p : products) {
			if (p.getID().equals(ID)) return p;
		}
		return null;
	}
	public Product getProductFromBarCode(long barCode) {
		for (Product p : products) {
			if (p.getBarCode() == barCode) return p;
		}
		return null;
	}
	
	public void deleteProducts(String[] IDs) {
		for (int j = 0; j < IDs.length; j ++) {
			for (int i = 0; i < products.size(); i ++ ){
				if (products.get(i).getID().equals(IDs[j])) {
					products.remove(i);
					break;
				}
			}
		}
	}
	
	public ArrayList<Product> getProducts() {return products;}

	public void addProducts(ArrayList<Product> products) {products.addAll(products);}
	
//	public void addProduct(Product product, boolean acceptDuplicateName) throws DuplicateNameException{
//		if (!acceptDuplicateName && checkDuplicateName(product.getName())) {
//			throw new DuplicateNameException();
//		}
//		products.add(product);
//		
//	}
//	
//	public boolean checkDuplicateName(String name) {
//		for(Product p : products) {
//			if (p.getName().equals(name)) return true;
//		}
//		return false;
//	}
//	
//	public Product getProduct(String ID) throws NoProductMatchIDException{
//		for (Product p : products) {
//			if(p.getID().equals(ID)) return p;
//		}
//		throw new NoProductMatchIDException();
//	}
//	
//	public double getTotalStartStock() {
//		double amount = 0;
//		for (Product p: products) {
//			amount += p.getTotalStock();
//		}
//		return amount;
//	}
//	
//	public int getAmountOfDifferentProducts() {
//		return products.size();
//	}
//	
//	public String[] getProductNames() {
//		String[] names = new String[products.size()];
//		for (int i = 0; i < names.length; i ++) {
//			names[i] = products.get(i).getName();
//		}
//		return names;
//	}
//	public String[] getProductIDs() {
//		String[] IDs = new String[products.size()];
//		for (int i = 0; i < IDs.length; i ++) {
//			IDs[i] = products.get(i).getID();
//		}
//		return IDs;
//	}
//	public void deleteProduct(String ID, String name) throws ProductNotFoundException{
//		for (int i = 0; i < products.size(); i ++) {
//			if (products.get(i).getName().equals(name) && 
//					products.get(i).getID().equals(ID)) {
//				products.remove(i);
//				return;
//			}
//		}
//		throw new ProductNotFoundException();
//	}
}
