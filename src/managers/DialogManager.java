package managers;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Optional;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import database.GlobalVariables;
import javafx.animation.PauseTransition;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Popup;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import javafx.stage.WindowEvent;
import javafx.util.Duration;
import main.Main;

public class DialogManager {
	
	static Window owner;
	static Main main;
	
	public static void setOwner(Window owner) {
		DialogManager.owner = owner;
	}
	
	public static void setMain(Main main) {
		DialogManager.main = main;
	}
	
	public static void alertError(String title, String header, String content) {
		Alert alert = new Alert(AlertType.ERROR);
		alert.setTitle(title);
		alert.setHeaderText(header);
		alert.setContentText(content);
		alert.initStyle(StageStyle.UNDECORATED);
		alert.getDialogPane().getStylesheets().add(main.getStyleSheet());
		alert.initOwner(owner);
		alert.showAndWait();
	}
	
	public static void alertWarning(String title, String header, String content) {
		Alert alert = new Alert(AlertType.WARNING);
		alert.setTitle(title);
		alert.setHeaderText(header);
		alert.setContentText(content);
		alert.initStyle(StageStyle.UNDECORATED);
		alert.getDialogPane().getStylesheets().add(main.getStyleSheet());
		alert.initOwner(owner);
		alert.showAndWait();
	}
	
	public static boolean alertConfirmation(String title, String header, String content) {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle(title);
		alert.setHeaderText(header);
		alert.setContentText(content);
		alert.initStyle(StageStyle.TRANSPARENT);
		alert.getDialogPane().getStylesheets().add(main.getStyleSheet());
		alert.initOwner(owner);
		
		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == ButtonType.OK) return true;
		else return false;
	}
	
	public static File getExistingDatabase(String fileExtension, String startFolder) {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Choose file containing existing database");
		File defaultDirectory = new File(startFolder);
		fileChooser.setInitialDirectory(defaultDirectory);
		FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(fileExtension + 
				" files (*." + fileExtension + ")", "*." + fileExtension);
		
		fileChooser.getExtensionFilters().add(extFilter);
		File selectedFile = fileChooser.showOpenDialog(owner);
		return selectedFile;
	}
	
	public static File getExistingFile(String[] fileExtension, String startFolder) {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Choose file containing existing database");
		File defaultDirectory = new File(startFolder);
		fileChooser.setInitialDirectory(defaultDirectory);
		FileChooser.ExtensionFilter[] extFilters = new FileChooser.ExtensionFilter[fileExtension.length];
		for (int i = 0; i < extFilters.length; i ++) {
			extFilters[i] = new FileChooser.ExtensionFilter(fileExtension[i] + 
					" files (*." + fileExtension[i] + ")", "*." + fileExtension[i]);
		}
		fileChooser.getExtensionFilters().addAll(extFilters);

		File selectedFile = fileChooser.showOpenDialog(owner);
		return selectedFile;
	}
	
	public static File saveFile(String[] fileExtension, String startFolder) {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Save file");
		fileChooser.setInitialDirectory(new File(startFolder));
		FileChooser.ExtensionFilter[] extFilters = new FileChooser.ExtensionFilter[fileExtension.length];
		for (int i = 0; i < extFilters.length; i ++) {
			extFilters[i] = new FileChooser.ExtensionFilter(fileExtension[i] + 
					" files (*." + fileExtension[i] + ")", "*." + fileExtension[i]);
		}
		fileChooser.getExtensionFilters().addAll(extFilters);
		
		File selectedFile = fileChooser.showSaveDialog(owner);
		return selectedFile;
	}
	
	
	public static File getUserChosenDirectory(String startFolder) {
		DirectoryChooser chooser = new DirectoryChooser();
		chooser.setTitle("Choose folder for database");

		File defaultDirectory = new File(startFolder);
		chooser.setInitialDirectory(defaultDirectory);
		File selectedDirectory = chooser.showDialog(owner);
		return selectedDirectory;
	}
	
	public static int chooseDatabasePathDialog() {

//		System.out.println("reached2");
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Database location");
		alert.setHeaderText("No database was detected");
		alert.setContentText("A database is needed to save data. This can be changed at any time. "
				+ "If you're in doubt, choose 'Use default folder'" + 
				" Default folder is: " + '"' + "c:/sqlite/db" + '"');

		ButtonType chooseFile = new ButtonType("Choose existing database");
		ButtonType chooseFolder = new ButtonType("Choose folder for new database");
		ButtonType defaultFolder = new ButtonType("Use default folder");
		
		alert.getButtonTypes().setAll(chooseFile, chooseFolder, defaultFolder);
		
		Button defaultButton = (Button) alert.getDialogPane().lookupButton( defaultFolder );
	    defaultButton.setDefaultButton( true );
		alert.initOwner(owner);
		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == defaultFolder) {
			return 3;
		} else if (result.get() == chooseFile){
		   return 2;
		} else if (result.get() == chooseFolder){
		   return 1;
		} else {
			return 0;
		}
	}
	
	public static String getTextInput(String title, String header, String content, String defaultValue) {
		TextInputDialog dialog = new TextInputDialog(defaultValue);
		dialog.setTitle(title);
		dialog.setHeaderText(header);
		dialog.setContentText(content);
		dialog.initOwner(owner);

		Optional<String> result = dialog.showAndWait();
		if (result.isPresent()) {
			return result.get();
		} else {
			return null;
		}
	}
	
	public static String getPassword(String title, String header, String content, String defaultValue, Window owner) {
		TextInputDialog dialog = new TextInputDialog();
		dialog.setTitle(title);
		dialog.setHeaderText(header);
		dialog.setContentText(content);
		dialog.initOwner(owner);
//		dialog.getEditor().promptTextProperty().set("Lel");
		dialog.getEditor().setPromptText("Kay");
//		dialog.getEditor().set

		Optional<String> result = dialog.showAndWait();
		if (result.isPresent()) {
			return result.get();
		} else {
			return null;
		}
	}
	
	public static boolean passwordDialog(Window owner, String password, String styleSheet) {
		Dialog<ButtonType> dialog = new Dialog<>();
		
		dialog.getDialogPane().getStylesheets().add(styleSheet);
		
		GridPane content = new GridPane();
		content.setVgap(10);
//		Label label = new Label(
		dialog.setHeaderText("Login as admin");
//		dialog.setGraphic(new ImageView(new Image(DialogManager.class.getResourceAsStream("/resources/images/unlock.ico"))));
		dialog.setGraphic(new ImageView(new Image(DialogManager.class.getResourceAsStream("/resources/images/unlock2.png"))));
		
		PasswordField pf = new PasswordField();
		pf.setPromptText("Password");
		content.add(pf, 0, 0, 2, 1);
		
		ButtonType btnType = new ButtonType("Login", ButtonData.OK_DONE);
		dialog.getDialogPane().getButtonTypes().addAll(btnType, ButtonType.CANCEL);
		Button btn = (Button) dialog.getDialogPane().lookupButton(btnType);
		
		TextArea textArea = new TextArea();
		textArea.setVisible(false);
		textArea.setWrapText(true);
		textArea.setPrefSize(250, 75);
		textArea.setEditable(false);
		textArea.setStyle("-fx-background-color:transparent;");
		content.add(textArea,0,1,2,1);
		
		Hyperlink hyperlink = new Hyperlink("Forgot password?");
		hyperlink.getStyleClass().add("password-link");
		content.add(hyperlink, 0, 2, 1, 1);
		hyperlink.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				String email = GlobalVariables.getRecoveryEmail();
				if (email == null) {
					textArea.setText("No recovery email was registered");
//				} else if (email.equals("mosemaskinen@gmail.com") || email.isEmpty()) {
//					textArea.setText("No recovery email was registered");
				} else {
					if (isConnected()) {
						if (sendEmail(GlobalVariables.getRecoveryEmail(), "Mosemaskinen password recovery",
								"The password for Mosemaskinen is: \n" + GlobalVariables.getPassword())) {
							textArea.setText("Email has been sent to: " + GlobalVariables.getRecoveryEmail());
						} else {
							textArea.setText("Failed to send mail");
						}
					} else {
						textArea.setText("No internet connection");
					}
				}
				textArea.setVisible(true);
				
				
			}
			
		});
		String toolTipText = "If you have forgotten the password, an email containing the password can be sent to the registered"
				+ "recovery email: " + GlobalVariables.getRecoveryEmail() + 
				"\n\n Press 'Esc' to close";
		Button toolTip = new Button();
		ImageView iw = new ImageView(new Image(DialogManager.class.getResourceAsStream("/resources/images/tooltip2.png")));
		iw.setFitWidth(20);
		iw.setPreserveRatio(true);
		toolTip.setGraphic(iw);
		toolTip.getStyleClass().add("buttonOption");
		Tooltip temp = new Tooltip(toolTipText);
		temp.setWrapText(true);
		temp.setMaxWidth(250);
		toolTip.setTooltip(temp);
		toolTip.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (toolTip.getTooltip().isShowing()) {
					toolTip.getTooltip().hide();
				} else {
					toolTip.getTooltip().show(toolTip.getScene().getWindow());
				}
			}

		});
		content.add(toolTip, 1, 2, 1, 1);
		
		pf.textProperty().addListener((observable, oldValue, newValue) -> {
		    btn.setDisable(newValue.trim().isEmpty());
		});
		
		Platform.runLater(() -> pf.requestFocus());
		
		btn.addEventFilter(ActionEvent.ACTION, 	event ->{
			if (!pf.getText().equals(password)) {
				event.consume();
			}
		}
		);
		dialog.setResultConverter(dialogButton -> {
			if (dialogButton.getButtonData() == btnType.getButtonData()) {
				return btnType;
			}
			return null;
		});
		
		dialog.setTitle("Login");
		dialog.initStyle(StageStyle.TRANSPARENT);
		dialog.getDialogPane().setContent(content);
		dialog.initOwner(owner);
		Optional<ButtonType> result = dialog.showAndWait();
		if (result.isPresent()) {
			if (result.get().getButtonData() == btnType.getButtonData()) {
				return true;
			} 
		}
		return false;
		
		
	}
	
	// Code from https://www.mkyong.com/java/javamail-api-sending-email-via-gmail-smtp-example/
	public static boolean sendEmail(String email, String subject, String text) {
		final String username = "mosemaskinen@gmail.com";
		final String password = "RecoverPassword";

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("mosemaskinen@gmail.com"));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
			message.setSubject(subject);
			message.setText(text);

			Transport.send(message);
			return true;
		} catch (MessagingException e) {
			return false;
		}
	}
	
	public static boolean isConnected() {
		InetSocketAddress addr = new InetSocketAddress("www.google.com", 80);
		try (Socket socket = new Socket();){
			socket.connect(addr, 3000);
			return true;
		} catch (IOException e) {
			return false;
		}
	}
	
	public static void showPopup(String text) {
		 	Popup popup = getPopup(main.getStage(), text);
		    PauseTransition delay = new PauseTransition(Duration.seconds(2.5));
		    delay.setOnFinished(e -> {popup.hide();});
		    popup.show(main.getAdminWindow());
		    delay.play();
	}
	
	public static Popup getPopup(Stage stage, String text) {
		final Popup popup = new Popup();
//		popup.setStyle("-fx-background-color:black;");
		
	    popup.setAutoFix(true);
	    popup.setHideOnEscape(true);
	    Label label = new Label(text);
	    label.getStyleClass().add("toast");
	    label.getStylesheets().add(main.getStyleSheet());
	    popup.getContent().add(label);
	    popup.setOnShown(new EventHandler<WindowEvent>() {
	        @Override
	        public void handle(WindowEvent e) {
	            popup.setX(stage.getX() + stage.getWidth()/2 - popup.getWidth()/2);
	            popup.setY(stage.getY() + stage.getHeight()/2 - popup.getHeight()/2);
	        }
	    });   
	    return popup;
	}
	
	public static Button createTooltip(String text) {
		Button toolTip = new Button();
		ImageView iw = new ImageView(new Image(DialogManager.class.getResourceAsStream("/resources/images/tooltip2.png")));
		iw.setFitWidth(20);
		iw.setPreserveRatio(true);
		toolTip.setGraphic(iw);
		toolTip.getStyleClass().add("buttonOption");
		Tooltip temp = new Tooltip(text);
		temp.setStyle("-fx-background-color:rgba(0,0,0,1);");
		temp.setWrapText(true);
		temp.setMaxWidth(250);
		toolTip.setTooltip(temp);
		toolTip.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (toolTip.getTooltip().isShowing()) {
					toolTip.getTooltip().hide();
				} else {
					toolTip.getTooltip().show(toolTip.getScene().getWindow());
				}
			}

		});
		return toolTip;
	}

	public static String loadText(String path) {
		String text = "";
		try (InputStream is = new BufferedInputStream(DialogManager.class.getResourceAsStream(path));) {
			byte[] c = new byte[2048];
			while (is.read(c) != -1) {
				for (byte cha : c) {
//					System.out.print((char) cha);
					text += (char) cha;
					if (cha == 0) break;
				}
			}
			return text;
		} catch (IOException e) {

		} finally {
		}
		return text;
	}
	
	
	
	
	
	

}
