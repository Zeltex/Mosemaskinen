package main;
import java.io.IOException;

import controllers.MainTabController;
import controllers.UserPageController;
import database.DatabaseHandler;
import database.GlobalVariables;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.concurrent.Task;
import javafx.concurrent.Worker;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;
import javafx.stage.WindowEvent;
import managers.DialogManager;

public class Main extends Application{
	
//	Scene adminScene, userScene;
	Scene scene;
	
	double width, height;
	DatabaseHandler dbHandler;
	Stage mainStage;
	
	UserPageController upc;
	MainTabController mtc;
    private String base = getClass().getResource("/style/Base.css").toExternalForm();
//    private String theme0 = getClass().getResource("/style/DefaultStyle.css").toExternalForm();
//    private String theme1 = getClass().getResource("/style/Style.css").toExternalForm();
    private String[] themes = new String[]{"DefaultStyle.css","Style.css"};
    
    Parent userRoot, adminRoot;
	
	Long lastTime = System.currentTimeMillis();
	String barCodeInput = "";
    
	//Splash screen
	VBox splashLayout;
	Stage initStage;
	
	public static void main(String[] args) {
		
		launch(args);
	}
	
	public Window getUserWindow() {
//		return userScene.getWindow();
		return scene.getWindow();
	}
	
	public Window getAdminWindow() {
//		return adminScene.getWindow();
		return scene.getWindow();
	}
	
	public Stage getStage() {
		return mainStage;
	}
	
	public void updateLists() {
		mtc.updateLists();
	}
	// Load splash screen
	 @Override
	    public void init() {
			ImageView splash = new ImageView(new Image(this.getClass().getResourceAsStream("/resources/images/craft-beers.jpg")));
			splash.setPreserveRatio(true);
			splash.setFitWidth(100);
			ProgressIndicator progress = new ProgressIndicator();
			progress.setProgress(ProgressIndicator.INDETERMINATE_PROGRESS);
	        splashLayout = new VBox();
	        splashLayout.getChildren().addAll(progress);
	        splashLayout.setStyle(
	                "-fx-padding: 5; " +
	                "-fx-background-color: cornsilk;"
	        );
	        splashLayout.setEffect(new DropShadow());
	    }

	 // Show splash screen
	private void showSplash(final Stage initStage, Task<?> task,
            InitCompletionHandler initCompletionHandler) {
        task.stateProperty().addListener((observableValue, oldState, newState) -> {
            if (newState == Worker.State.SUCCEEDED) {
            	try {
					show();
				} catch (IOException e) {
					e.printStackTrace();
				}
                initStage.toFront();
                initCompletionHandler.complete();
                	
            }
        });

        Scene splashScene = new Scene(splashLayout, Color.TRANSPARENT);
        final Rectangle2D bounds = Screen.getPrimary().getBounds();
        
        
        initStage.setScene(splashScene);
        initStage.setX(bounds.getMinX() + bounds.getWidth() / 2 - 100 / 2);
        initStage.setY(bounds.getMinY() + bounds.getHeight() / 2 - 50 / 2);
        initStage.initStyle(StageStyle.TRANSPARENT);
        initStage.setAlwaysOnTop(true);
        initStage.show();
    }
	//show main
	 private void showMainStage(ReadOnlyObjectProperty<DatabaseHandler> dbHandler) {
		 this.dbHandler = dbHandler.get();
			mainStage.show();
			initStage.hide();
	 }
	 public interface InitCompletionHandler {
	        void complete();
	 }
	
	@Override
	public void start(Stage initStage) throws Exception {
		// Attempt to prevent two instances from running
//		try {
////			ServerSocket ss = new ServerSocket(65535, 1, InetAddress.getLocalHost());
////			ss.bind(, 65535);
//		} catch (UnknownHostException e) {
//			DialogManager.alertError("Multiple instances running", null, 
//					"An instance of the program is already running");
//			System.exit(0);
//		} catch (IOException e) {
//			DialogManager.alertError("Multiple instances running", null, 
//					"An instance of the program is already running");
//			System.exit(0);
//		}
		final Task<DatabaseHandler> loadTask = new Task<DatabaseHandler>() {
            @Override
            protected DatabaseHandler call() throws InterruptedException {
            	DatabaseHandler dbHandler = new DatabaseHandler();
            	GlobalVariables.init();
            	try {
//					show();
            		setupAdminScene(dbHandler);
            		setupUserScene(dbHandler);
				} catch (IOException e) {
					e.printStackTrace();
				}
				return dbHandler; 
            	
            }
		};
		this.initStage = initStage;
		showSplash(initStage, loadTask, () -> showMainStage(loadTask.valueProperty()));
		new Thread(loadTask).start();
	}
	public void show() throws IOException{
		mainStage = new Stage();
		Rectangle2D screenBounds = Screen.getPrimary().getBounds();
		mainStage.setFullScreenExitKeyCombination(KeyCombination.NO_MATCH);
		scene = new Scene(adminRoot, screenBounds.getWidth(), screenBounds.getHeight(), Color.TRANSPARENT);
		mainStage.setScene(scene);
		DialogManager.setOwner(scene.getWindow());
		DialogManager.setMain(this);
		loadThemes();
		setBase();
		setTheme(GlobalVariables.currentThemeInt);
		scene.setFill(Color.TRANSPARENT);
		mainStage.initStyle(StageStyle.TRANSPARENT);
		mainStage.setX(0);
		mainStage.setY(0);
		mainStage.setFullScreen(true);
		Platform.setImplicitExit(false);
		mtc.initBackup();
		mtc.animate();
		upc.animate();
		scene.addEventFilter(KeyEvent.KEY_RELEASED, new EventHandler<KeyEvent>() 
	    {
		    @Override
		    public void handle(KeyEvent event) 
		    {
		    	if (System.currentTimeMillis() - lastTime > 50 && GlobalVariables.lockScan) {
		    		barCodeInput = "";
		    	} else if (System.currentTimeMillis() - lastTime > 1500){
		    		barCodeInput = "";
		    	}
		    	lastTime = System.currentTimeMillis();
		    if(event.getCode()==KeyCode.WINDOWS) {
		             event.consume();                   
		             scene.getFocusOwner().requestFocus();
		    }
		    switch(event.getCode()) { 
		    case WINDOWS: break;
		    case DIGIT1: barCodeInput += "1"; break;
		    case DIGIT2: barCodeInput += "2"; break;
		    case DIGIT3: barCodeInput += "3"; break;
		    case DIGIT4: barCodeInput += "4"; break;
		    case DIGIT5: barCodeInput += "5"; break;
		    case DIGIT6: barCodeInput += "6"; break;
		    case DIGIT7: barCodeInput += "7"; break;
		    case DIGIT8: barCodeInput += "8"; break;
		    case DIGIT9: barCodeInput += "9"; break;
		    case DIGIT0: barCodeInput += "0"; break;

		    case NUMPAD1: barCodeInput += "1"; break;
		    case NUMPAD2: barCodeInput += "2"; break;
		    case NUMPAD3: barCodeInput += "3"; break;
		    case NUMPAD4: barCodeInput += "4"; break;
		    case NUMPAD5: barCodeInput += "5"; break;
		    case NUMPAD6: barCodeInput += "6"; break;
		    case NUMPAD7: barCodeInput += "7"; break;
		    case NUMPAD8: barCodeInput += "8"; break;
		    case NUMPAD9: barCodeInput += "9"; break;
		    case NUMPAD0: barCodeInput += "0"; break;
		    case ENTER: {
		    	upc.barCodeInput(barCodeInput);
		    	barCodeInput = "";
		    } break;
			default:
				break;
		    }
		    }});
		
			
		scene.addEventFilter(MouseEvent.MOUSE_RELEASED, (EventHandler<MouseEvent>) event -> {
			if (GlobalVariables.backupInProgress) {
				event.consume();
			}
		});
		mainStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
		    @Override
		    public void handle(WindowEvent event) {
		    	if (GlobalVariables.isScreenLocked()) event.consume();
		        else System.exit(0);
		    }
		});
		
		// Force program on top
		new Thread(new Runnable() {
	        @Override
	        public void run() {
	            while(true){
	                try {
	                    Thread.sleep(50);
	                } catch (InterruptedException e) {}

	                Platform.runLater(()->{
	                    if (GlobalVariables.isScreenLocked()) mainStage.toFront();
	                });
	            }
	        }
	    }).start();
		
	}
	
	private void setupUserScene(DatabaseHandler dbHandler) throws IOException {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/UserPage.fxml"));		
		userRoot = (Parent) fxmlLoader.load();
		upc = fxmlLoader.<UserPageController>getController();
		upc.setMain(this);
		upc.init(dbHandler);
	}
	
	private void setupAdminScene(DatabaseHandler dbHandler) throws IOException {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/view/TabLayout.fxml"));		
		adminRoot = (Parent) fxmlLoader.load();
		mtc = fxmlLoader.<MainTabController>getController();
		mtc.setDBHandler(dbHandler);
		dbHandler.setMainTabController(mtc);
		mtc.setMain(this);
		mtc.init();
	}
	
	public void setAdminScene() {
		GlobalVariables.setLockScreen(false);
		mainStage.getScene().setRoot(adminRoot);
		setTheme(GlobalVariables.currentThemeInt);
	}
	
	public void setUserScene() {
		GlobalVariables.setLockScreen(true);
		mainStage.getScene().setRoot(userRoot);
		mainStage.setAlwaysOnTop(true);
		upc.update();
		upc.isVisible();
		Platform.runLater(() -> setTheme(GlobalVariables.currentThemeInt));
	}
	
	private void setBase() {
		scene.getStylesheets().add(base);
	}
	
	private void loadThemes() {
		for (int i = 0; i < themes.length; i ++) {
			themes[i] = getClass().getResource("/style/" + themes[i]).toExternalForm();
		}
		
	}
	
	public String getStyleSheet() {
		return themes[GlobalVariables.currentThemeInt];
	}
	
	public void setTheme(int theme) {
		setThemeRoot(adminRoot, theme);
		setThemeRoot(userRoot, theme);
	}
	
	public void setThemeRoot(Parent root, int theme) {
		for (String s : themes) {
				if (!s.equals(themes[theme])) {
					root.getStylesheets().remove(s);
				}
		}
		if (!root.getStylesheets().contains(themes[theme])) root.getStylesheets().add(themes[theme]);
	}
	
//	public void setThemeRoot(Parent root, int theme) {
//		root.getStylesheets().add(themes[theme]);
//		for (String s : themes) {
//				if (!s.equals(themes[theme])) {
//					root.getStylesheets().remove(s);
//				}
//		}
//	}
	
	public void addTransaction(String userName, String productName, double amount, boolean absolute, String productID, 
			String userID, boolean deprecated) {
		mtc.addTransaction(userName, productName, amount, absolute, productID, userID, deprecated);
	}
	
}
