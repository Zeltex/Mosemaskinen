package controllers;

import java.util.ArrayList;

import controllerAssistants.ProductTabAssistant;
import database.DatabaseHandler;
import database.GlobalVariables;
import importedCode.NumberFormatChecker;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.control.ToolBar;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import managers.DialogManager;
import managers.MultiEntrySaver;
import objects.Product;

public class ProductTabController {
	@FXML ImageView banner;
	@FXML ListView<String> ProductList;
	@FXML TextField fieldSearch;
	@FXML ImageView buttonCloseSearchImg;
	@FXML HBox showProduct;
	@FXML VBox optionWrapper;
	@FXML HBox productPane;
	@FXML GridPane theGridPane;
	@FXML TextField productInfo1, productInfo2, productInfo3, productInfo4, productInfo5, productInfo7, productInfo8;
	TextField[] productInfoText;
	@FXML ToolBar toolBar;
	@FXML ComboBox<String> sortChooser, productInfo6;
	ArrayList<ComboBox<String>> productInfoCombo;

	@FXML Button cancelProductInformation, editProductInformation, saveProductInformation, deleteProductInformation,
			productInformationTooltip;

	int sortFilter;
	String showFilter;

	Product chosenProduct;
	DatabaseHandler dbHandler;
	MainTabController mtc;
	ProductTabAssistant pta;
	MultiEntrySaver mes;

	int productInfo6Pressed;

	boolean productInfoEdit;

	boolean listDirectionRising;
	@FXML Label directionLabel;
	@FXML ListView<Product> productList;

	public void init(DatabaseHandler dbHandler, MainTabController mtc) {
		this.dbHandler = dbHandler;
		this.mtc = mtc;
		resetVariables();
		setTooltip();
		productList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		refreshButtons();
		pta.setTextProductInfoUneditable(false, productInfoCombo, productInfoText, chosenProduct);
		setListeners();
		pta.clearFields(productInfoCombo, productInfoText);
		theGridPane.setDisable(true);
		pta.setupComboBox(productInfoCombo);
	}

	private void resetVariables() {
		pta = new ProductTabAssistant();
		mes = new MultiEntrySaver();
		productInfoText = new TextField[] { productInfo1, productInfo2, productInfo3, productInfo4, productInfo5,
				productInfo7, productInfo8 };
		productInfoCombo = new ArrayList<ComboBox<String>>();
		productInfoCombo.add(productInfo6);
		productInfoEdit = false;
		sortFilter = 0;
		showFilter = "";
		productInfo6Pressed = 1;
		listDirectionRising = true;
	}

	// Set main tooltip
	private void setTooltip() {
		String tooltipText = "Explanations of product fields\n"
				+ "ID: (Read-only) Unique identifier used internally in the program\n" + "Name: Name of the product\n"
				+ "Total stock: How many items of the given product there were in the beginning\n"
				+ "Current stock: How many items of the given product there are left\n"
				+ "Sold: (Read-Only) How many items of the given product have been sold\n"
				+ "Category: What kind of product it is, used for statistics\n"
				+ "Price: Price per item, can be changed at any time" + "\n\n" + "Press 'Esc' to close.";
		toolBar.getItems().add(DialogManager.createTooltip(tooltipText));
		//		Platform.runLater(() -> productInformationTooltip = DialogManager.createTooltip(tooltipText));
	}
	
//	// Handle tooltip show and hide
//	@FXML void productInformationTooltip() {
//		if (productInformationTooltip.getTooltip().isShowing()) {
//			productInformationTooltip.getTooltip().hide();
//		} else {
//			productInformationTooltip.getTooltip().show(productInformationTooltip.getScene().getWindow());
//		}
//	}

	// Delete products
	@FXML void deleteProductInformation() {
		double count = 0;
		ObservableList<Integer> indices = productList.getSelectionModel().getSelectedIndices();
		String[] IDs = new String[indices.size()];
		String names = "";
		
		// Get up to 10 names
		for (int i = 0; i < indices.size(); i++) {
			Product temp = dbHandler.getProductByIndex(indices.get(i));
			count += dbHandler.getCount(temp);
			IDs[i] = temp.getID();
			if (i < 10) {
				if (i != 0)
					names += ", ";
				names += temp.getName();
			}
		}
		if (indices.size() >= 10)
			names += "...";
		// Confirm deletion
		String text = "You are about to delete " + IDs.length + " product" + (IDs.length > 1 ? "s" : "") + ": " + names
				+ ". There are in told registered " + count + " sold units. Are you sure you want to do it?";
		boolean confirm = DialogManager.alertConfirmation("Deleting product" + (IDs.length > 1 ? "s" : ""), null, text);
		// Delete them
		if (confirm) {
			dbHandler.deleteProducts(IDs);
			mtc.setDepricated(new String[0], IDs);
			mtc.load();
			productInfoEdit = false;
			refreshButtons();
			dbHandler.cleanupRTC();
		}
	}

	// Update productlist
	public void load() {
		dbHandler.updateProductList(productList, showFilter, sortFilter);
	}

	// Pull data from database and update lists
	@FXML void pullData() {
		mtc.load();
	}

	// Change direction of sorting in productlist
	@FXML void changeDirection() {
		listDirectionRising = !listDirectionRising;
		directionLabel.setText((listDirectionRising ? "Rising" : "Falling"));
		dbHandler.changeProductDirection(productList, sortFilter);
	}

	// Search field cleared
	@FXML void clearSearch() {
		fieldSearch.clear();
		dbHandler.updateProductList(productList, "", sortFilter);
	}

	// Change in the search field, productlist updated
	@FXML void searchFieldUpdated() {
		dbHandler.updateProductList(productList, fieldSearch.getText(), sortFilter);
	}

	// Start edit of product
	@FXML void editProductInformation() {
		pta.setFieldsEditable(true, true, productInfoCombo, productInfoText, chosenProduct, false);
		productInfoEdit = true;
		refreshButtons();
	}

	// Cancel the edit of product
	@FXML void cancelEditProductInformation() {
		if (productList.getSelectionModel().getSelectedIndices().size() > 1)
			productList.getSelectionModel().clearSelection();
		int index = productList.getSelectionModel().getSelectedIndex();
		updateChosenProduct(index);
		productInfoEdit = false;
		refreshButtons();
		pta.setFieldsEditable(false, true, productInfoCombo, productInfoText, chosenProduct, false);
	}

	// Save one or more products
	@FXML void saveProductInformation() {
		ObservableList<Integer> indices = productList.getSelectionModel().getSelectedIndices();
		if (indices.size() > 1) {
			saveMultipleProducts(indices);
		} else {
			saveSingleProduct();
		}
		dbHandler.cleanupRTC();
	}
	
	// Save more than one product
	public void saveMultipleProducts(ObservableList<Integer> indices){
		ArrayList<Integer> noneEmpty = pta.getNoneEmptyUserFields(productInfoCombo, productInfoText);
		if (noneEmpty.size() == 0) {
			DialogManager.alertWarning("Nothing to save", null, "All input fields were empty, nothing was saved");
			return;
		}
		// Confirm category
		if (!checkListEntry()) {
			return;
		}
		
		// Setup variables
		ArrayList<String> changes = mes.getProductChanges(noneEmpty, indices, productInfoCombo, productInfoText);
		ArrayList<String> IDs = new ArrayList<String>();
		for (int i : indices) {
			IDs.add(dbHandler.getProductByIndex(i).getID());
		}
		// Save to database
		if (!dbHandler.editInformation("products", changes, IDs)) {
			DialogManager.alertError("SQLError", null, "Something went wrong while saving information. "
					+ "Application data will be refreshed from database.");
			mtc.load();
			return;
		}
		dbHandler.updateProducts(changes, IDs);

		for (int i = 0; i < changes.size(); i += 2) {
			if (changes.get(i).equals("name")) {
				dbHandler.updateUserProductNames(IDs, changes.get(i + 1));
				mtc.updateUserList();
			}
		}
		dbHandler.updateProductList(productList, showFilter, sortFilter);
	}
	
	// Save one product
	public void saveSingleProduct() {
		
		if (emptyproductInfoField()) { 
			DialogManager.alertWarning("Empty information field", null,
					"Product information cannot be saved when there is an empty field");
		} else {
			// Check format of fields
			if (!pta.validateInputFields(productInfoText)) {
				DialogManager.alertError("Format error", null,
						"One or more fields didn't comply with the double format. " + "Correct format is dot separated."
								+ " e.g. (20.5)");
				return;
			}
			// Confirm category
			if (!checkListEntry()) {
				return;
			}
			ArrayList<String> difference = chosenProduct.compare(productInfo2.getText(), productInfo3.getText(),
					productInfo4.getText(), productInfo6.getValue(), productInfo7.getText(), productInfo8.getText());

			
			
			// Check and handle duplicate barcode
			for (int i = 0; i < difference.size(); i += 2) {
				if (difference.get(i).equals("barCode")) {
					if (NumberFormatChecker.isInteger(difference.get(i + 1))) {
						long newOne = Long.parseLong(difference.get(i + 1));
						Long highest = 100000L;
						boolean assignNew = false;
						for (Long barCode : dbHandler.getProductBarCodes()) {
							if (newOne == barCode) {
								DialogManager.alertWarning("Barcode duplicate", null,
										"The new barcode was already occupied by another product. "
												+ "Assigning new barcode");
								assignNew = true;
							}
							if (barCode > highest)
								highest = barCode;
						}
						if (assignNew) {
							difference.set(i + 1, (highest + 1) + "");
						}
					}
				}
			}
			
			// Save to database
						if (!dbHandler.editInformation("products", difference, chosenProduct.getID())) {
							DialogManager.alertError("SQLError", null, "Something went wrong while saving information. "
									+ "Application data will be refreshed from database.");
							mtc.load();
							return;
						}
			
			// If name changed, userproduct reference need to be updated
			for (int i = 0; i < difference.size(); i += 2) {
				if (difference.get(i).equals("name")) {
					dbHandler.updateUserProductName(chosenProduct.getID(), difference.get(i + 1));
					mtc.updateUserList();
				}
			}
			// Update session stuff
			dbHandler.updateProduct(difference, chosenProduct.getID());
			String savedID = chosenProduct.getID();
			dbHandler.updateProductList(productList, showFilter, sortFilter);
			int index = dbHandler.getShownProductIndex(savedID);
			if (index != -1) {
				productList.getSelectionModel().clearAndSelect(index);

			}
			productInfoEdit = false;
			refreshButtons();
			pta.setFieldsEditable(false, true, productInfoCombo, productInfoText, chosenProduct, false);
		}
	}

	// Check chosen category
	private boolean checkListEntry() {
		if (!GlobalVariables.confirmCategory(productInfo6.getValue()) && !productInfo6.getValue().isEmpty()) {
			boolean answer = DialogManager.alertConfirmation("Unknown category", null, "The given category " + '"'
					+ productInfo6.getValue() + '"' + "is not registered. Do you want to register it?");
			if (answer) {
				GlobalVariables.addCategory(productInfo6.getValue());
				dbHandler.editSettings("categories", GlobalVariables.getSaveStrings("categories"));
				chosenProduct.setCategory(productInfo6.getValue());
				pta.setupComboBox(productInfoCombo);
				pta.resetComboBox(productInfoCombo, chosenProduct);
			} else {
				return false;
			}
		}
		return true;
	}
	
	// Update values in combobox, category has been changed
	public void updateComboBoxes() {
		pta.setupComboBox(productInfoCombo);
		pta.resetComboBox(productInfoCombo, chosenProduct);
	}

	// Check if fields are empty
	private boolean emptyproductInfoField() {
		if (productInfo6.getValue() == null)
			return false;
		return (productInfo2.getText().isEmpty() || productInfo3.getText().isEmpty() || productInfo4.getText().isEmpty()
		// || productInfo5.getText().isEmpty()
				|| productInfo6.getValue().isEmpty() || productInfo7.getText().isEmpty() || productInfo8.getText().isEmpty());
	}

	// Update what product's info is being shown
	private void updateChosenProduct(int index) {
		chosenProduct = dbHandler.getProductByIndex(index);
		// Clear info shown
		if (chosenProduct == null) {
			theGridPane.setDisable(true);
			pta.clearFields(productInfoCombo, productInfoText);
			pta.setTextProductInfoUneditable(false, productInfoCombo, productInfoText, chosenProduct);
		// Show info
		} else {
			showProduct.setVisible(true);
			theGridPane.setDisable(false);
			pta.showProductData(chosenProduct, productInfoCombo, productInfoText);
		}
	}

	// Go to user page/front screen
	@FXML
	void back(ActionEvent event) {
		mtc.setUserScene();
	}

	// Closes application
	@FXML void exit() {
		System.exit(0);
	}
	
	// Creates and selects new product
	@FXML void createNewProduct() {
		Product newProduct = new Product("","New product", 0.0, 0.0, 0.0, "Unknown", 0.0,0L);
		if (dbHandler.addProduct(newProduct)) {
			// Add default category 'Unknown' if doesnt exist
			if (!GlobalVariables.getCategories().contains("Unknown")) {
				GlobalVariables.addCategory("Unknown");
				dbHandler.editSettings("categories", GlobalVariables.getSaveStrings("categories"));
				mtc.updateProductComboBoxes();
			}
			String id = newProduct.getID();
			mtc.load();
			Product p = dbHandler.getProductFromID(id);
			productList.getSelectionModel().select(p);
			productList.scrollTo(p);
			editProductInformation();
			DialogManager.showPopup("Created new product");
//			dbHandler.updateProductList(productList, showFilter, sortFilter);
		} else {

			DialogManager.showPopup("Failed to create new product");
		}
	}

	// Settings listeners
	private void setListeners() {
		fieldSearch.textProperty().addListener((obj, oldVal, newVal) -> {
			showFilter = newVal.trim();
			dbHandler.updateProductList(productList, showFilter, sortFilter);

		});
		sortChooser.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> selected, String oldChoice, String newChoice) {
				sortSelectionChanged(newChoice);
			}
		});
		productList.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Product>() {
			@Override
			public void changed(ObservableValue<? extends Product> arg0, Product arg1, Product arg2) {
				productSelectionChanged();
			}
		});
		// productInfo 3,4,5,7
		int[] doubleRestrictions = new int[] { 2, 3, 4, 5 };
		for (int i = 0; i < doubleRestrictions.length; i++) {
			pta.setDoubleRestriction(productInfoText[doubleRestrictions[i]]);
		}
		// productInfo 8
		pta.setIntegerRestriction(productInfoText[6]);
		pta.setComboBehaviour(productInfoCombo);
	}

	// Change in sort list
	private void sortSelectionChanged(String newChoice) {
		if (newChoice != null) {
			sortFilter = pta.getSortFilter(newChoice);
			dbHandler.sortProducts(productList, showFilter, sortFilter, listDirectionRising);
		}
	}

	// Selection in productlist changed
	private void productSelectionChanged() {
		ObservableList<Integer> indices = productList.getSelectionModel().getSelectedIndices();
		// Multiple products selected, clears fields
		if (indices.size() > 1) {
			productInfoEdit = true;
			showProduct.setVisible(true);
			theGridPane.setDisable(false);
			pta.clearFields(productInfoCombo, productInfoText);
			pta.setFieldsEditable(true, true, productInfoCombo, productInfoText, chosenProduct, true);
			pta.setComboboxEmpty(productInfoCombo);
			refreshButtons();
		// Single product selected, shows product info
		} else {
			int index = productList.getSelectionModel().getSelectedIndex();
			updateChosenProduct(index);
			productInfoEdit = false;
			refreshButtons();
		}
	}

	// Updates whether the buttons are disabled or not
	private void refreshButtons() {
		if (productInfoEdit) {
			cancelProductInformation.setDisable(false);
			editProductInformation.setDisable(true);
			saveProductInformation.setDisable(false);
			deleteProductInformation.setDisable(false);
		} else {
			cancelProductInformation.setDisable(true);
			editProductInformation.setDisable(false);
			saveProductInformation.setDisable(true);
			deleteProductInformation.setDisable(false);
		}
	}

}
