package controllers;

import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

import controllerAssistants.UserTabAssistant;
import database.DatabaseHandler;
import database.GlobalVariables;
import importedCode.NumberFormatChecker;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TextField;
import javafx.scene.control.ToolBar;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import managers.DialogManager;
import managers.MultiEntrySaver;
import objects.Product;
import objects.User;
import objects.UserProduct;
import tableEntries.Transaction;

public class UserTabController implements Initializable {
	@FXML ImageView banner;
	@FXML ListView<User> userList;
	@FXML TextField fieldSearch;
	@FXML ImageView buttonCloseSearchImg;
	@FXML HBox showUser;
	@FXML VBox optionWrapper;
	@FXML HBox userPane;
	@FXML GridPane theGridPane, userProductInfoPane, userInfoPane, userProductInfoContainer;
	@FXML TextField userInfo1, userInfo2, userInfo3, userInfo6, userInfo7, userInfo8, userInfo9, userInfo11, userInfo12;
	TextField[] userInfoText;
	@FXML ComboBox<String> userInfo4, userInfo5, userInfo10;
	ArrayList<ComboBox<String>> userInfoCombo;
	@FXML CheckBox nameToggle;
	@FXML ToolBar toolBar1, toolBar2;
	@FXML ComboBox<String> sortChooser;
	@FXML Button cancelUserInformation, editUserInformation, saveUserInformation, deleteUserInformation,
			cancelProductInformation, editProductInformation, saveProductInformation, userInformationTooltip,
			userProductInformationTooltip;
	int sortFilter;
	String showFilter;

	User chosenUser;
	DatabaseHandler dbHandler;
	MainTabController mtc;
	MultiEntrySaver mes;
	UserTabAssistant uta;

	boolean showRealNames, userInfoEdit, userProductEdit;

	int userInfo4Pressed, userInfo5Pressed;

	boolean listDirectionRising, multipleSelected;
	@FXML Label directionLabel;

	ArrayList<Label> userProductLabels;
	ArrayList<TextField> userProductTextFields;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
	}

	public void init(DatabaseHandler dbHandler, MainTabController mtc) {
		nameToggle.setSelected(true);
		showRealNames = true;
		this.dbHandler = dbHandler;
		this.mtc = mtc;
		resetVariables();
		setTooltips();
		userList.getItems().clear();
		userList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		uta.setInitialUserProductLayout(userInfoPane);
		setListeners();
		disableUserInfoStuff();
		uta.setupComboBoxes(userInfoCombo);
	}
	
	private void resetVariables() {
		userInfoText = new TextField[]{userInfo1, userInfo2, userInfo3, userInfo6, 
				userInfo7, userInfo8, userInfo9, userInfo11, userInfo12};
		userInfoCombo = new ArrayList<ComboBox<String>>();
		userInfoCombo.add(userInfo4);
		userInfoCombo.add(userInfo5);
		userInfoCombo.add(userInfo10);
		userProductTextFields = new ArrayList<TextField>();
		userProductLabels = new ArrayList<Label>();
		mes = new MultiEntrySaver();
		uta = new UserTabAssistant();
		mes.init(dbHandler);
		showRealNames = true;
		userInfoEdit = false;
		userProductEdit = false;
		userInfo4Pressed = 1;
		multipleSelected = false;
		showFilter = "";
		listDirectionRising = true;
	}
	
	// Go to user page/fron screen
	@FXML void setUserScene() {
		mtc.setUserScene();
	}
	
	// Close application
	@FXML void exit() {
		System.exit(0);
	}
	
	// Tooltips for user information and user products
	private void setTooltips() {
		String tooltipText = "Explanations of user fields:\n"
				+ "Real name: The name of the user\n"
				+ "Calling name: Nickname of the user, by default set to same value as 'Real name'\n"
				+ "ID: (Read-only) Unique identifier used internally in the program\n"
				+ "Rank: The status of the user, a type of category to sort users\n"
				+ "Team: Which team the user belongs to, also a category, main sorting method for stats\n"
				+ "Study number: The studyNumber of the user\n"
				+ "Rustur: Which trip the user is on, not used for anything atm\n"
				+ "Favorite drink: (Read-only) Which drink the user has bought most of\n"
				+ "Total items: (Read-only) Total amount of drinks the user has bought"
				+ "Gender: Gender of the user, used for stats\n"
				+ "Bar code: The identifier for user in the scanning system\n\n"
				+ "Press 'Esc' to close.";
		toolBar1.getItems().add(DialogManager.createTooltip(tooltipText));
		String tooltipText2 = "Fields show how much the user has bought of each product."
				+ " Should only be used in specialcases. \n"
				+ "Standard way of registering purchases is"
				+ " through scanning system.\n"
				+ "Shown numbers are rounded to 1 digit after decimal point\n\n"
				+ "Press 'Esc' to close.";
		
		toolBar2.getItems().add(DialogManager.createTooltip(tooltipText2));
	}
	
	// Adds a transaction of random amount 0:5, of every product to every user 
	@FXML void debugAddPurchases() {
		// Setup names and IDS
		ArrayList<String> IDS = new ArrayList<String>(),
				productNames = new ArrayList<String>();
		for (Product p : dbHandler.getProducts()) {
			IDS.add(p.getID());
			productNames.add(p.getName());
		}
		ArrayList<ArrayList<String>> outer = new ArrayList<ArrayList<String>>();
		String date = getDate();
		String time = getTime();
		ArrayList<Transaction> t = new ArrayList<Transaction>();
		
		// Loop and add transactions
		for (User user : dbHandler.getUsers()) {
			String userName = user.getName();
			ArrayList<String> inner = new ArrayList<String>();
			inner.add(user.getID());
			for (int i = 0; i < IDS.size(); i ++) {
				double amount = Math.round(Math.random() * 500) / 100;
				inner.add(IDS.get(i));
				inner.add(amount + "");
				t.add(new Transaction(userName, productNames.get(i), amount,
						true, IDS.get(i), user.getID(), false, date, time));
			}
			outer.add(inner);
		}
		// Save transaction
		if (!dbHandler.editMultiple("users", outer)) {
			return;
		}
		mtc.addTransactions(t);
		for (ArrayList<String> diff : outer) {
			String id = diff.get(0);
			diff.remove(0);
			dbHandler.getUserFromID(id).update(diff);;
		}
		dbHandler.sortUsers(userList, showFilter, showRealNames, sortFilter, listDirectionRising);
		dbHandler.updateAllProductSold();
		mtc.updateProductList();
	}

	// Selection in userList changed
	private void userListChanged() {

		ObservableList<Integer> indices = userList.getSelectionModel().getSelectedIndices();
		if (indices.size() > 1) {
			userInfoEdit = true;
			userProductEdit = true;
			multipleSelected = true;
			showUser.setVisible(true);
			theGridPane.setDisable(false);
			uta.clearUserInfoFields(userInfoCombo, userInfoText);
			clearUserProductFields();
			uta.setTextUserProductInfoEditable(true, userProductTextFields);
			uta.setFieldsEditable(true, true, userInfoText, userInfoCombo, chosenUser, true);
			uta.setComboBoxesEmpty(userInfoCombo);
			refreshButtonStatus();
		} else {
			multipleSelected = false;
			int index = userList.getSelectionModel().getSelectedIndex();
			
//			userList.getSelectionModel().get
			updateChosenUser(index, true, true);
			userInfoEdit = false;
			userProductEdit = false;
			refreshButtonStatus();
			
		}

	}

	// The selection in the combobox for sorting changed
	private void sortChooserChanged(String newChoice) {
		
		if (newChoice != null) {
			sortFilter = uta.getSortFilterValue(newChoice);
		}
		System.out.println("sort: " + newChoice + " : " + sortFilter);
		dbHandler.sortUsers(userList, showFilter, showRealNames, sortFilter, listDirectionRising);
	}

	// Create a new empty user, called from button in left menu
	@FXML void createNewUser() {
		User newUser = new User("New user", "Fresh", "", "Unknown", "Unknown", "Unknown", "", "", 0, 0L, null);
		
		// Add the new user
		if (dbHandler.addUser(newUser)) {
			dbHandler.updateUserList(userList, showFilter, showRealNames, sortFilter);
			userList.getSelectionModel().select(newUser);
			userList.scrollTo(newUser);
			
			// Confirm ranks contains 'Unknown'
			if (!GlobalVariables.getRanks().contains("Unknown")) {
				GlobalVariables.addRank("Unknown");
				dbHandler.editSettings("ranks", GlobalVariables.getSaveStrings("ranks"));
				mtc.updateUserComboBoxes();
			}
			
			// Confirm teams contains 'Unknown'
			if (!GlobalVariables.getTeams().contains("Unknown")) {
				GlobalVariables.addTeam("Unknown");
				dbHandler.editSettings("teams", GlobalVariables.getSaveStrings("teams"));
				mtc.updateUserComboBoxes();
			}
			editUserInformation();
			DialogManager.showPopup("Created new user");
		} else {
			DialogManager.alertError("Unable to create new user", null,
					"An error acurred when trying to create new user");
		}
	}

	// Enable editing of user information
	@FXML void editUserInformation() {
		userInfoEdit = true;
		refreshButtonStatus();
		uta.setFieldsEditable(true, true, userInfoText, userInfoCombo, chosenUser, false);
	}

	// Cancel editing user information
	@FXML void cancelEditUserInformation() {
		userInfoEdit = false;
		if (userList.getSelectionModel().getSelectedIndices().size() > 1) userList.getSelectionModel().clearSelection();
		refreshButtonStatus();
		int index = userList.getSelectionModel().getSelectedIndex();
		updateChosenUser(index, true, false);
		uta.setFieldsEditable(false, true, userInfoText, userInfoCombo, chosenUser, false);
	}
	
//	// Logic for showing and hiding tooltip
//	@FXML void userInformationTooltip() {
//		if (userInformationTooltip.getTooltip().isShowing()) {
//			userInformationTooltip.getTooltip().hide();
//		} else {
//			userInformationTooltip.getTooltip().show(userInformationTooltip.getScene().getWindow());
//		}
//	}
	
//	// Logic for showing and hiding tooltip
//	@FXML void userProductInformationTooltip() {
//		if (userProductInformationTooltip.getTooltip().isShowing()) {
//			userProductInformationTooltip.getTooltip().hide();
//		} else {
//			userProductInformationTooltip.getTooltip().show(userProductInformationTooltip.getScene().getWindow());
//		}
//	}
	
	// Save user information
	@FXML void saveUserInformation() {
		ObservableList<Integer> indices = userList.getSelectionModel().getSelectedIndices();
		if (indices.size() > 1) {
			saveMultipleUsers(indices);
		} else {
			saveSingleUser();
		}
		dbHandler.cleanupRTC();
	}
	
	// Save more than one user
	private void saveMultipleUsers(ObservableList<Integer> indices) {
		ArrayList<Integer> noneEmpty = uta.getNoneEmptyUserFields(userInfoCombo, userInfoText);
		if (!mes.confirmGRT(noneEmpty, userInfoCombo, dbHandler, this)) {
			return;
		}
		if (noneEmpty.size() == 0)
			DialogManager.alertWarning("Nothing to save", null, "All input fields were empty, nothing was saved");
		// Get information to be changed
		ArrayList<String> changes = mes.getUserChanges(noneEmpty, indices, userInfoCombo, userInfoText);
		ArrayList<String> IDS = new ArrayList<String>();
		for (int i : indices) {
			IDS.add(dbHandler.getUserByIndex(i).getID());
		}
		
		// Setup information to be changed in proper format
		ArrayList<ArrayList<String>> outer = new ArrayList<ArrayList<String>>();
		for (int i = 0; i < IDS.size(); i++) {
			ArrayList<String> inner = new ArrayList<String>();
			inner.add(IDS.get(i));
			inner.addAll(changes);
			outer.add(inner);
		}
		
		// Change information in database
		if (!dbHandler.editMultiple("users", outer)) {
			DialogManager.alertError("SQLError", null, "Something went wrong while saving information. "
					+ "Application data will be refreshed from database.");
			mtc.load();
			return;
		}
		// Update session stuff
		for (String id : IDS) dbHandler.updateUser(changes, id);
		dbHandler.sortUsers(userList, showFilter, showRealNames, sortFilter, listDirectionRising);
	}
	
	// Save one user
	private void saveSingleUser() {
		chosenUser = dbHandler.getUserFromID(userList.getSelectionModel().getSelectedItem().getID());
		if (uta.isUserInfoFieldEmpty(userInfoCombo, userInfoText)) {
			DialogManager.alertWarning("Empty information field", null,
					"User information cannot be saved when there is an empty field");
		} else {
			String rank = userInfo4.getValue(), team = userInfo5.getValue(), study = userInfo12.getText(); //TODO study
			if (!validateInputFields(rank, team))
				return;
			// Get information to be changed
			ArrayList<String> difference = chosenUser.compare(userInfo1.getText(), userInfo2.getText(),
					rank, team, study, userInfo6.getText(), userInfo7.getText(),
					User.confirmGender(userInfo10.getValue()), Long.parseLong(userInfo11.getText()));
			// Check and handle duplicate barcode
			for (int i = 0; i < difference.size(); i += 2) {
				if (difference.get(i).equals("barCode")) {
					if (NumberFormatChecker.isInteger(difference.get(i + 1))) {
						Long newOne = Long.parseLong(difference.get(i + 1));
						Long highest = 1000L;
						boolean assignNew = false;
						for (Long barCode : dbHandler.getUserBarCodes()) {
							if (newOne == barCode) {
								DialogManager.alertWarning("Barcode duplicate", null, "The new barcode was already occupied by another user. "
										+ "Assigning new barcode");
								assignNew = true;
							}
							if (barCode > highest) highest = barCode;
						}
						if (assignNew) {
							difference.set(i + 1, (highest + 1) + "");
						}
					}
				}
			}
			// Change information
			if (!dbHandler.editInformation("users", difference, chosenUser.getID())) {
				DialogManager.alertError("SQLError", null, "Something went wrong while saving information. "
						+ "Application data will be refreshed from database.");
				mtc.load();
				return;
			}
			
			// Update session stuff
			dbHandler.updateUser(difference, chosenUser.getID());
			String savedID = chosenUser.getID();
			dbHandler.sortUsers(userList, showFilter, showRealNames, sortFilter, listDirectionRising);
			int index = dbHandler.getShownUserIndex(savedID);
			if (index != -1) {
				userList.getSelectionModel().clearAndSelect(index);
			}
			userInfoEdit = false;
			refreshButtonStatus();
			uta.setFieldsEditable(false, true, userInfoText, userInfoCombo,chosenUser, false);
			DialogManager.showPopup("User saved successfully");
		}
	}

	
	// Confirm CRT and gender exists
	private boolean validateInputFields(String rank, String team) {
		// After new restrictions, gender and barCode should never test positive. They were kept just in case
		int gender = User.confirmGender(userInfo10.getValue());
		if (gender == -1) {
			DialogManager.alertError("Gender error", null, "The chosen error wasn't recognized");
			return false;
		}
		if (!NumberFormatChecker.isInteger(userInfo11.getText())) {
			DialogManager.alertError("Bar code error", null,
					"The bar code contained non-valid characters. " + "Only numbers allowed");
			return false;
		}
		if (!GlobalVariables.confirmRank(rank)&& !rank.isEmpty()) {
			boolean answer = DialogManager.alertConfirmation("Unknown rank", null, "The given rank " + '"'
					+ rank + '"' + "is not registered. Do you want to register it?");
			if (answer) {
				GlobalVariables.addRank(rank);
				if (!dbHandler.editSettings("ranks", GlobalVariables.getSaveStrings("ranks"))) {
					return false;
				}
				updateComboBoxes();
			} else {
				return false;
			}
		}
		if (!GlobalVariables.confirmTeam(team) && !team.isEmpty()) {
			boolean answer = DialogManager.alertConfirmation("Unknown team", null, "The given team " + '"'
					+ team + '"' + "is not registered. Do you want to register it?");
			if (answer) {
				GlobalVariables.addTeam(team);
				if (!dbHandler.editSettings("teams", GlobalVariables.getSaveStrings("teams"))) {
					return false;
				}
				updateComboBoxes();
			} else {
				return false;
			}
		}
		return true;
	}
	
	public void updateComboBoxes() {
		uta.setupComboBoxes(userInfoCombo);
		uta.resetComboBox(userInfoCombo, chosenUser);
	}

	// Deleting a user
	@FXML void deleteUserInformation() {
		ObservableList<Integer> indices = userList.getSelectionModel().getSelectedIndices();
		String[] IDs = new String[indices.size()];
		String names = "";
		// Get up to 10 selected names
		for (int i = 0; i < indices.size(); i ++) {
			User temp = dbHandler.getUserByIndex(indices.get(i));
			IDs[i] = temp.getID();
			if (i < 10) {
				if (i != 0) names += ", ";
				names += temp.getName();
			}
		}
		if (indices.size() >= 10) names += "...";
		String text = "You are about to delete " + IDs.length + " user" + (IDs.length > 1 ? "s" : "") + ": " + names + 
				". Are you sure you want to do it?";
		boolean confirm = DialogManager.alertConfirmation("Deleting user" + (IDs.length > 1 ? "s" : ""), null, text);
		
		// Delete users
		if (confirm) {
			userInfoEdit = false;
			refreshButtonStatus();
			dbHandler.deleteUsers(IDs);
			dbHandler.updateAllProductSold();
			dbHandler.updateUserList(userList, showFilter, showRealNames, sortFilter);
			mtc.setDepricated(IDs, new String[0]);
			dbHandler.cleanupRTC();
		}
	}

	// Called when user toggles whether name or callingName should be shown
	@FXML void nameToggleChanged() {
		showRealNames = nameToggle.isSelected();
		dbHandler.updateUserListType(userList, showRealNames, sortFilter);
	}

	// Changes direction of the userList
	@FXML void changeDirection() {
		listDirectionRising = !listDirectionRising;
		directionLabel.setText((listDirectionRising ? "Rising" : "Falling"));
		dbHandler.changeUserDirection(userList, showRealNames, sortFilter);
	}

	// Resetting information in userproduct fields, and setting them uneditable
	@FXML void cancelEditUserProductInformation() {
		userProductEdit = false;
		refreshButtonStatus();
		uta.setTextUserProductInfoEditable(false, userProductTextFields);
		int index = userList.getSelectionModel().getSelectedIndex();
		updateChosenUser(index, false, true);
	}

	// Enables editing of userproduct fields
	@FXML void editUserProductInformation() {
		userProductEdit = true;
		refreshButtonStatus();
		uta.setTextUserProductInfoEditable(true, userProductTextFields);
	}

	// Saves user product information
	@FXML void saveUserProductInformation() {
			try {
				// Selected users
				ObservableList<User> users = userList.getSelectionModel().getSelectedItems();
				boolean compareUserProduct;
				ArrayList<UserProduct> userProducts = new ArrayList<UserProduct>();
				switch(users.size()) {
				case 0: return;
				case 1: compareUserProduct = true; userProducts.addAll(users.get(0).getUserProducts()); break;
				default: compareUserProduct = false; userProducts.addAll(dbHandler.getAUser().getUserProducts()); break;
				}
				
				// Difference, if 1 selected return changed userproductfield, if more selected, return non-zero
				ArrayList<String> difference = uta.getNoneEmptyUserProductFields(userProductTextFields, 
						userProducts, compareUserProduct);
				if (difference.size() == 0) return;
				userProductEdit = false;
				refreshButtonStatus();
				uta.setTextUserProductInfoEditable(false, userProductTextFields);

				// Array containing changes, used to update database
				ArrayList<ArrayList<String>> outer = new ArrayList<ArrayList<String>>();
				for (User user : users) {
					ArrayList<String> inner = new ArrayList<String>();
					inner.add(user.getID());
					inner.addAll(difference);
					outer.add(inner);
				}
				
				// Updating database
				if (!dbHandler.editMultiple("users", outer)) {
					DialogManager.alertError("SQLError", null, "Something went wrong while saving information. "
							+ "Data will be refreshed to synchronize with database.");
					pullData();
					return;
				}
				
				// Updating usermanager and committing transactions
				for (ArrayList<String> inner : outer) {
					String id = inner.get(0);
					inner.remove(0);
					dbHandler.updateUser(inner, id);
					for (int i = 0; i < inner.size(); i += 2) {
						mtc.addTransaction(dbHandler.getUserNameFromID(id), dbHandler.getProductNameFromID(inner.get(i)), 
								Double.valueOf(inner.get(i + 1)), true, inner.get(i), id, false);
					}
				}
				
				// Refresh stuff
				dbHandler.sortUsers(userList, showFilter, showRealNames, sortFilter, listDirectionRising);
				dbHandler.updateAllProductSold();
				mtc.updateProductList();
				DialogManager.showPopup("User products saved");
			} catch (NumberFormatException e) {
				DialogManager.alertError("Number format exception", null,
						"One or more fields doesn't comply with the number format");
			}
//		}
	}

	// A user has been clicked on in the list in the user tab
	private void updateChosenUser(int index, boolean users, boolean products) {
		chosenUser = dbHandler.getUserByIndex(index);
		if (chosenUser == null) {
			disableUserInfoStuff();

		} else {
			if (users) {
				showUser.setVisible(true);
				theGridPane.setDisable(false);
				uta.showUserData(chosenUser, userInfoCombo, userInfoText);
				uta.setFieldsEditable(false, true, userInfoText, userInfoCombo, chosenUser, false);
			}
			if (products) {
				showUserProducts();
				uta.setTextUserProductInfoEditable(false, userProductTextFields);
			}
		}
	}

	// Update userProduct fields to show current info from chosenUser
	private void showUserProducts() {
		ArrayList<UserProduct> userProducts = chosenUser.getUserProducts();
		userProductInfoContainer.setDisable(false);
		userProductInfoContainer.setVisible(true);

		if (userProducts != null) {
			uta.loadUserProducts(false, userProductInfoPane, userProducts, userProductTextFields, userProductLabels);
		} else {
			userProductInfoPane.getChildren().clear();
		}
	}

	// Sets all userproduct fields empty, called when multiple users selected
	private void clearUserProductFields() {
		ArrayList<UserProduct> userProducts = chosenUser.getUserProducts();
		userProductInfoContainer.setDisable(false);
		userProductInfoContainer.setVisible(true);
		if (userProducts != null) {
			uta.loadUserProducts(true, userProductInfoPane, userProducts, userProductTextFields, userProductLabels);
		} else {
			userProductInfoPane.getChildren().clear();
		}
	}
	
	// Clears search text and update userlist
	@FXML
	void clearSearch() {
		fieldSearch.clear();
		showFilter = "";
		dbHandler.updateUserList(userList, showFilter, showRealNames, sortFilter);
	}

	// The text in searchfield changed, updates userlist
	@FXML
	void searchFieldUpdated() {
		dbHandler.updateUserList(userList, showFilter, showRealNames, sortFilter);
	}

	// Button Press 'Pull data', updates data and refreshes lists
	@FXML
	void pullData() {
		mtc.load();
	}

	// Updates userlist
	public void load() {
		dbHandler.updateUserList(userList, showFilter, showRealNames, sortFilter);
	}

	// When user shouldn't be shown anymore
	private void disableUserInfoStuff() {
		theGridPane.setDisable(true);
		userProductInfoContainer.setDisable(true);
		uta.clearUserInfoFields(userInfoCombo, userInfoText);
		uta.setTextUserInfoUneditable(false, userInfoText, userInfoCombo, chosenUser);
		uta.setTextUserProductInfoEditable(false, userProductTextFields);
		userProductInfoPane.getChildren().clear();
	}

	// Updates whether the buttons are disabled or not
	private void refreshButtonStatus() {
		if (userInfoEdit) {
			cancelUserInformation.setDisable(false);
			editUserInformation.setDisable(true);
			saveUserInformation.setDisable(false);
			deleteUserInformation.setDisable(false);
		} else {
			cancelUserInformation.setDisable(true);
			editUserInformation.setDisable(false);
			saveUserInformation.setDisable(true);
			deleteUserInformation.setDisable(false);
		}
		if (userProductEdit) {
			cancelProductInformation.setDisable(false);
			editProductInformation.setDisable(true);
			saveProductInformation.setDisable(false);
		} else {
			cancelProductInformation.setDisable(true);
			editProductInformation.setDisable(false);
			saveProductInformation.setDisable(true);
		}
	}

	
	// All listeners
	private void setListeners() {
		sortChooser.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> selected, String oldChoice, String newChoice) {
				sortChooserChanged(newChoice);
			}
		});
		fieldSearch.textProperty().addListener((obj, oldVal, newVal) -> {
			showFilter = newVal.trim();
			dbHandler.updateUserList(userList, showFilter, showRealNames, sortFilter);

		});
		userList.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<User>() {
			@Override
			public void changed(ObservableValue<? extends User> arg0, User arg1, User arg2) {
				userListChanged();
			}
		});
		userInfo11.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d*")) {
					userInfo11.setText(newValue.replaceAll("[^\\d]", ""));
				}
			}
		});
		uta.setupComboBoxesBahaviour(userInfoCombo);
		userList.setCellFactory(new Callback<ListView<User>, ListCell<User>>(){
            @Override
            public ListCell<User> call(ListView<User> p) {
                ListCell<User> cell = new ListCell<User>(){
                    @Override
                    protected void updateItem(User user, boolean empty) {
                        super.updateItem(user, empty);
                        if (user != null && !empty) {
                            setText(user.getWhatToShow());
                        } else {
                        	setText("");
                        }
                    }
                };
                return cell;
            }
        });
	}
	
	//Returns current date, used for transaction
	public String getDate() {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
		return dateFormat.format(date);
	}
	
	// Returns current time, used for Transaction
	public String getTime() {
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		Date date = new Date();
		return dateFormat.format(date);
	}

}
