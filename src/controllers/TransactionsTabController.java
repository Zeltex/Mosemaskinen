package controllers;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import database.DatabaseHandler;
import database.GlobalVariables;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import managers.DialogManager;
import objects.User;
import objects.UserShell;
import tableEntries.Transaction;

public class TransactionsTabController implements Initializable{
	@FXML TableView<Transaction> table;
	@FXML TextField searchFieldUser, searchFieldProduct;
	final int userNameIndex = 0, productNameIndex = 1, amountIdex = 2, absoluteIndex = 3, userIDIndex = 5, productIDIndex = 4, deprecatedIndex = 6;
	String separator = ";";
	int maxFileSize = 200;
	@FXML Button tooltip;
	@FXML VBox toolTipContainer;
	DatabaseHandler dbHandler;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
//		table.getItems().add(new Transaction("Pild ved grisling", 10.0, false, "p1", "u1", false));
		
	}
	
	@FXML void searchUser() {
		loadTransactions(true, searchFieldUser.getText(), "");
	}
	@FXML void searchProduct() {
		loadTransactions(true, "", searchFieldProduct.getText());
	}
	
	@FXML void loadAll() {
		loadTransactions(true, "", "");
	}
	
	@FXML void loadLatest(){
		loadTransactions(false, "", "");
	}
	
	public boolean resetTransactions() {
		ArrayList<String> files = getTransactionsFile(true);
		boolean success = true;
		for (String s : files) {
			File f = new File(s);
			if (!f.delete()) {
				success = false;
			}
		}
		return success;
	}

//	// Logic for showing and hiding tooltip
//	@FXML void tooltip() {
//		if (tooltip.getTooltip().isShowing()) {
//			tooltip.getTooltip().hide();
//		} else {
//			tooltip.getTooltip().show(tooltip.getScene().getWindow());
//		}
//	}
	
	private void setTooltip() {
		String tooltipText =
				"Search for users or products, or load all transactions or the latest up to 200 transactions\n"
				+ "The 'Absolute' field in the table is if the transaction was added to the existing number, "
				+ "or overwrote it. aka. if it was done from user page or admin page."
				+ "\n\nPress 'Esc' to close.";
		toolTipContainer.getChildren().add(DialogManager.createTooltip(tooltipText));
		Pane p = new Pane();
		p.setMinWidth(50);
		toolTipContainer.getChildren().add(p);
	}

	public void init(DatabaseHandler dbHandler) {
		this.dbHandler = dbHandler;
//		loadTransactions(false, "", "");
		setTooltip();
		searchFieldProduct.setOnKeyPressed(new EventHandler<KeyEvent>() {
		    @Override
		    public void handle(KeyEvent keyEvent) {
		        if (keyEvent.getCode() == KeyCode.ENTER)  {
		            searchProduct();
		        }
		    }
		});
		searchFieldUser.setOnKeyPressed(new EventHandler<KeyEvent>() {
		    @Override
		    public void handle(KeyEvent keyEvent) {
		        if (keyEvent.getCode() == KeyCode.ENTER)  {
		            searchUser();
		        }
		    }
		});
	}
	
	//Initial load of transactions
	public void loadTransactions(boolean all, String filterUser, String filterProduct) {
		filterUser = filterUser.toLowerCase();
		filterProduct = filterProduct.toLowerCase();
		table.getItems().clear();
		ArrayList<String> paths = getTransactionsFile(all);
		if (paths != null) {
			ArrayList<PathSorting> pathSorting = new ArrayList<PathSorting>();
			for (String path : paths)
				pathSorting.add(new PathSorting(path));
			pathSorting.sort(Comparator.comparing(PathSorting::getComparingString));
			if (pathSorting != null) {
				for (PathSorting path : pathSorting) {
					if (path != null) {
						File importFile = new File(path.toString());
						try (BufferedReader br = new BufferedReader(new FileReader(importFile))) {
							String line;
							ArrayList<Transaction> t = new ArrayList<Transaction>();
							while ((line = br.readLine()) != null) {
								String[] entries = line.split("\\" + separator);
								String userName = entries[userNameIndex].toLowerCase(), productName = entries[productNameIndex].toLowerCase();
								if (!entries[deprecatedIndex].equals("true")) {
									if ((userName.contains(filterUser) && filterProduct.isEmpty()) || 
											(productName.contains(filterProduct) && filterUser.isEmpty()) || 
											(filterUser.isEmpty() && filterProduct.isEmpty()))
									t.add(new Transaction(entries));
								}
							}
							table.getItems().addAll(t);
							// return true;
						} catch (FileNotFoundException e) {
							e.printStackTrace();
							// return false;
						} catch (IOException e) {
							e.printStackTrace();
							// return false;
						}
					}
				}
			}
		}
	}
	
	public boolean validateDatabaseSync() {
		ArrayList<UserShell> data = new ArrayList<UserShell>();
		for (User user : dbHandler.getUsers()) {
			data.add(new UserShell(user));
		}
		ArrayList<String> paths = getTransactionsFile(true);
		if (paths != null) {
			ArrayList<PathSorting> pathSorting = new ArrayList<PathSorting>();
			for (String path : paths)
				pathSorting.add(new PathSorting(path));
			pathSorting.sort(Comparator.comparing(PathSorting::getComparingString));
			if (pathSorting != null) {
				for (PathSorting path : pathSorting) {
					if (path != null) {
						File importFile = new File(path.toString());
						try (BufferedReader br = new BufferedReader(new FileReader(importFile))) {
							String line;
							ArrayList<Transaction> t = new ArrayList<Transaction>();
							while ((line = br.readLine()) != null) {
								String[] entries = line.split("\\" + separator);
								if (!entries[deprecatedIndex].equals("true")) {
									//TODO add to stack
									t.add(new Transaction(entries));
									for (UserShell us : data) {
										if (us.getID().equals(entries[userIDIndex])) {
											us.addTransaction(entries[productIDIndex], Double.parseDouble(entries[amountIdex]), entries[absoluteIndex].equals("true"));
										}
									}
								}
							}
						} catch (FileNotFoundException e) {
							e.printStackTrace();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
		List<Integer> error = new ArrayList<Integer>();
		int index = 0;
		for (User user : dbHandler.getUsers()) {
			if (!data.get(index).compare(user.getUserProducts())) {
				error.add(index);
			}
			index ++;
		}
		System.out.println("Total errors: " + error.size());
		if (error.size() > 0) {
			return false;
		} else {
			return true;
		}
	}
	
	class PathSorting {
		String path;
		public PathSorting(String path) {
			this.path = path;
		}
		public String getComparingString() {
			String[] split = path.split("\\\\");
			String name = split[split.length - 1];
			return name.substring(13, name.length());
		}
		public String toString() {
			return path;
		}
	}
	
	//Debug stuff
	@FXML void debugAddTransaction() {
		String[] debugIDS = new String[]{"u1", "u2", "u3", "u4", "u5"};
		ArrayList<Transaction> t = new ArrayList<Transaction>();
		for (int i = 0; i < 70; i ++) t.add(
				new Transaction("user" + i, "product" + i, Math.random() * 10, false, 
						"p", debugIDS[i % 4], false, getDate(), getTime()));
		addTransactions(t);
		
//		setFileFinished(GlobalVariables.transactionFile);
	}
	
	@FXML void debugSetDeprecated() {
		setDeprecated(new String[]{"u3"}, new String[]{});
	}
	
	public void setDeprecated(String[] userID, String[] productID) {
		File directory = new File(GlobalVariables.getTransactionsFolder());
		if (!directory.isDirectory() || !directory.exists()) {
			directory.mkdirs();
		} else {
			File[] files = directory.listFiles();
			for (File f : files) {
				if (f.getName().substring(0, 12).equals("transactions")) {
					if (f.getName().charAt(12) != 'D') {
						if (markDeprecated(userID, productID, f)) {
							rename(f.getAbsolutePath(), "D");
						}
//						GlobalVariables.transactionFile = f.getAbsolutePath();
					}
				}
			}
		}
	}
	
	private boolean markDeprecated(String[] userID, String[] productID, File f) {
		BufferedWriter writer = null;
		try (BufferedReader br = new BufferedReader(new FileReader(f))) {
			String line;
			boolean rewriteFile = false;
			StringBuilder sb = new StringBuilder();
			int deprecatedLines = 0, totalLines = 0;
			boolean lineSetDeprecated;
			while ((line = br.readLine()) != null) {
				totalLines ++;
				lineSetDeprecated = false;
				String[] entries = line.split(separator);
				if (entries[deprecatedIndex].equals("false")){
					for (String id : userID) {
						if (entries[userIDIndex].equals(id)) {
							entries[deprecatedIndex] = "true";
							rewriteFile = true;
							if (!lineSetDeprecated) {
								deprecatedLines ++;
								lineSetDeprecated = true;
							}
						}
					}
					for (String id : productID) {
						if (entries[productIDIndex].equals(id)) {
							entries[deprecatedIndex] = "true";
							rewriteFile = true;
							if (!lineSetDeprecated) {
								deprecatedLines ++;
								lineSetDeprecated = true;
							}
						}
					}
				} else {
					deprecatedLines ++;
				}
				String toAppend = "";
				int current = 0, max = entries.length - 1;
				for (String s : entries) {
					toAppend += s;
					if (current != max) toAppend += separator;
					else toAppend += "\n";
					current ++;
				}
				sb.append(toAppend);
			}
			if (rewriteFile) {
				writer = new BufferedWriter(new FileWriter(f));
				writer.append(sb);
			    writer.close();
			}
			if (totalLines == deprecatedLines) {
				return true;
				
//				rename(f.getAbsolutePath(), "D");
			}
			return false;
//			table.getItems().addAll(t);
//			return true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
//			return false;
		} catch (IOException e) {
			e.printStackTrace();
//			return false;
		} finally {
			if (writer != null)  {
				try {
					writer.close();
				} catch (IOException e) {
					
				}
			}
		}
		return false;
	}
	
	//Logic to retrieve the one transaction file that isn't finished or deprecate
	public ArrayList<String> getTransactionsFile(boolean all) {
		ArrayList<String> paths = new ArrayList<String>();
		File directory = new File(GlobalVariables.getTransactionsFolder());
		if (!directory.isDirectory() || !directory.exists()) {
			directory.mkdirs();
		}
		File[] files = directory.listFiles();
		GlobalVariables.transactionFile = "";
		for (File f : files) {
			if (f.getName().substring(0,12).equals("transactions")) {
				if (f.getName().charAt(12) != 'D') {
					if (all) {
						paths.add(f.getAbsolutePath());
					}
					if (f.getName().charAt(12) != 'F') {
						GlobalVariables.transactionFile = f.getAbsolutePath();
					}
				}
			}
		}
		if (GlobalVariables.transactionFile.isEmpty()) {
			setNewFile(null);
			File f = new File(GlobalVariables.transactionFile);
			if (!f.exists()) {
				f.getParentFile().mkdirs();
				try {
					f.createNewFile();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return null;
		}
		if (paths.isEmpty()) paths.add(GlobalVariables.transactionFile);
		
		return paths;
	}
	
	//The method used to register transaction
	public void addTransaction(Transaction t) {
		GlobalVariables.transactionFile = getTransactionsFile(false).get(0);
		if (saveTransaction(t)) {
			table.getItems().add(t);
		} else {
			DialogManager.alertError("Transaction error", null, "An error ocurred while saving transaction");
		}
	}
	
	public void addTransactions(ArrayList<Transaction> t) {
		GlobalVariables.transactionFile = getTransactionsFile(false).get(0);
		if (saveTransactions(t)) {
			table.getItems().addAll(t);
		} else {
			DialogManager.alertError("Transaction error", null, "An error ocurred while saving transaction");
		}
	}
	
	public void addTransaction(String userName, String productName, double amount, boolean absolute, String productID, 
			String userID, boolean deprecated) {
		addTransaction(new Transaction(userName, productName, amount, absolute, productID, userID, deprecated, getDate(), getTime()));
		
	}
	
	//Sets the name of the next file that will be used to save transactions
	private void setNewFile(String timeStamp) {
		if (timeStamp == null) timeStamp = getPlainDate();
		boolean finished = false;
		int i = 0;
		while(!finished) {
			String add2 = "-" + i;
			if (new File(getNewFilePath(" ", add2, timeStamp)).exists() || 
					new File(getNewFilePath("F", add2, timeStamp)).exists() || 
					new File(getNewFilePath("D", add2, timeStamp)).exists())  {
				i++;
			} else {
				GlobalVariables.transactionFile = getNewFilePath(" ", add2, timeStamp);
				finished = true;
			}
		}
	}
		
	public String getNewFilePath(String add1, String add2, String timeStamp) {
		return  GlobalVariables.getTransactionsFolder() + "\\transactions" + add1 + timeStamp + add2 + ".csv";
	}
	
	//Sets file as finished, it contains 50 transaction, this is to avoid large files
	private void setFileFinished(String path, String timeStamp) {
		rename(path, "F");
		setNewFile(timeStamp);
	}
	
	//Sets file as deprecated, none of its transactions are valid anymore
	@SuppressWarnings("unused")
	private void setFileDeprecated(String path, String timeStamp) {
		rename(path, "D");
		setNewFile(timeStamp);
	}
	
	//Rename a file to add 'F' or 'D'
	private void rename(String path, String letter) {		
		String[] s = path.split("\\\\");
		int index = s.length - 1;
		s[index] = "transactions" + letter + s[index].substring(13, s[index].length());
		String newPath = "";
		int times = 0;
		for (String string : s) {
			newPath += string;
			if (times != index) newPath += "\\"; 
			times ++;
		}
		File newFile = new File(newPath);
		File oldFile = new File(path);
		oldFile.renameTo(newFile);
	}
	
	//Adds transaction to csv file
	public boolean saveTransaction(Transaction t) {
		try {
			if (countLines(GlobalVariables.transactionFile) >= maxFileSize) {
				setFileFinished(GlobalVariables.transactionFile, null);
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(GlobalVariables.transactionFile, true));){
			bw.write(t.toString(separator) + "\n");
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}
	
	public boolean saveTransactions(ArrayList<Transaction> t) {
		int lines = 0;
		String timeStamp = getPlainDate();
		try {
			if ((lines = countLines(GlobalVariables.transactionFile)) >= maxFileSize) {
				setFileFinished(GlobalVariables.transactionFile, timeStamp);
				lines = 0;
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		boolean finished = false;
		int index = 0;
		boolean first = true;
		while (!finished) {
			if (first) {
				first = false;
			} else {
				setFileFinished(GlobalVariables.transactionFile, timeStamp);
			}
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(GlobalVariables.transactionFile, true));){
			StringBuilder sb = new StringBuilder();
			for (int i = index; i < t.size(); i ++) {
				if ((i + lines) % maxFileSize == 0 && i != index) {
					bw.append(sb);
					index = i;
					break;
				}
				sb.append(t.get(i).toString(separator) + "\n");
				if (i + 1 >= t.size()) {
					finished = true;
					bw.append(sb);
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		}
		return true;
	}
	
	//Returns the amount of lines in file, used for setting files done when they reach 200 lines
	public int countLines(String filename) throws IOException {    
	    try (InputStream is = new BufferedInputStream(new FileInputStream(filename));){
	        byte[] c = new byte[1024];
	        int count = 0;
	        int readChars = 0;
	        boolean empty = true;
	        while ((readChars = is.read(c)) != -1) {
	            empty = false;
	            for (int i = 0; i < readChars; ++i) {
	                if (c[i] == '\n') {
	                    ++count;
	                }
	            }
	        }
	        return (count == 0 && !empty) ? 1 : count;
	    } finally {
	    }
	}
	
	//Returns current date, used to name new files
	public String getPlainDate() {
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmm");
		Date date = new Date();
		return dateFormat.format(date);
	}
	
	public String getDate() {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		Date date = new Date();
		return dateFormat.format(date);
	}
	
	public String getTime() {
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		Date date = new Date();
		return dateFormat.format(date);
	}
	
}
