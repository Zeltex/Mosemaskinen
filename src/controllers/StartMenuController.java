package controllers;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

public class StartMenuController implements Initializable {
	
	@FXML Button button1, button2, button3;
	@FXML ImageView banner;
	@FXML TabPane tabPane;
	@FXML Tab tabUserBar, tabProductBar;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
//		File f = new File("src/style/craft-beers.jpg");
		File f = new File("res/pic/craft-beers.jpg");
		Image image = new Image(f.toURI().toString());
		banner.setImage(image);
	}
	
	@FXML void manageUsers(ActionEvent event) {
		try {
			Stage primaryStage = (Stage) ((Node)event.getSource()).getScene().getWindow();
			loadNewScene(primaryStage, "../view/Users.fxml", "Users");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	@FXML void manageProducts(ActionEvent event) {
		try {
			Stage primaryStage = (Stage) ((Node)event.getSource()).getScene().getWindow();
			loadNewScene(primaryStage, "../view/Products.fxml", "Products");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	@FXML void exit(ActionEvent event) {
		
		Platform.exit();
	}
	
	private void loadNewScene(Stage primaryStage, String path, String title) throws IOException{
		Parent root = FXMLLoader.load(getClass().getResource(path));
		
		Scene scene = new Scene(root, 1000,1000);
		
		
		
		primaryStage.setScene(scene);
		
		primaryStage.setTitle(title);
		
		primaryStage.show();
	}
	
	
}
