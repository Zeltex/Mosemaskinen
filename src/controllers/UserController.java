package controllers;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class UserController implements Initializable{
	
	@FXML ImageView banner;
	@FXML ListView<String> userList;
	@FXML TextField fieldSearch;
	
	private List<String> stringList = new ArrayList<>(5);
	private ObservableList<String> observableList = FXCollections.observableArrayList();
	
	@FXML void back(ActionEvent event) {
		try {
			Stage primaryStage = (Stage) ((Node)event.getSource()).getScene().getWindow();
			loadNewScene(primaryStage, "../view/StartMenu.fxml", "Start menu");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	@FXML void clearSearch() {
		fieldSearch.clear();
	}
	
	@FXML void searchFieldUpdated() {
		updateUserList();
	}
	
	@FXML void importUsers(ActionEvent event) {
		//Promt user to direct to database
//		String directoryName = "c:/sqlite/db";
//		String fileName = "test.db";
//		
//		File directory = new File(directoryName);
//		
////		File f = new File("c:/sqlite/db");
//		if (!directory.exists()) {
//			directory.mkdirs();
//		}
////		SQLiteDriverConnection.connect();
//		
//		SQLiteDriverConnection driver = new SQLiteDriverConnection();
////		driver.createNewDatabase(fileName);
////		driver.createNewTable();
//////		driver.selectAll();
////		driver.insert("firstID", "Pild ved", "Grisling");
////		driver.insert("secondID", "Han er", "Mantana");
////		driver.insert("thirdID", "Can touch", "Tits");
//////		
////		driver.select("Pild ved");
//		driver.select("secondID");
		
//		driver.selectAll();
		
//		SQLiteDriverConnection.createNewDatabase(fileName);
//		SQLiteDriverConnection.createNewTable();
	}
	
	public void updateUserList() {
		
	}
//	
//	private void loadExistingDatabase() {
//		
//	}
	
	

	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		setBanner();
//		load();
		setListView();
	}
	
	private void setBanner() {
		File f = new File("src/style/craft-beers.jpg");
		Image image = new Image(f.toURI().toString());
		banner.setImage(image);
	}
	
	private void setListView() {
		stringList.add("String 1");
		stringList.add("String 2");
		stringList.add("String 3");
		stringList.add("String 4");
		observableList.setAll(stringList);
		
		userList.setItems(observableList);
		userList.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				String chosen = userList.getSelectionModel().getSelectedItem();		
				System.out.println(chosen);
			}
			
		});
		
	}
	
	private void loadNewScene(Stage primaryStage, String path, String title) throws IOException{
		Parent root = FXMLLoader.load(getClass().getResource(path));
		Scene scene = new Scene(root, 1000,1000);
		
		primaryStage.setScene(scene);
		
		primaryStage.setTitle(title);
		
		primaryStage.show();
	}
}
