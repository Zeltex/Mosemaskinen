package controllers;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Pattern;

import database.DatabaseHandler;
import database.GlobalVariables;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.HPos;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.util.converter.DoubleStringConverter;
import managers.DialogManager;
import objects.Product;
import objects.User;
import objects.UserProduct;
import tableEntries.ProductTableEntry;
import tableEntries.UserTableEntry;

public class EconomyTabController {
	@FXML TableView<UserTableEntry> userTable, userTable2;
	@FXML TableView<ProductTableEntry> productTable, productTable2;
	@FXML GridPane settingsPane;
	DatabaseHandler dbHandler;
	MainTabController mtc;
	ArrayList<CheckBox> boxes;
	ArrayList<Label> texts;
	
	// 0 = waste, 1 = handle payment, 2 = export payment
	int showContent;
	@FXML Button /*b0, b1,*/ b2, b3, b4, b5;
	
	ArrayList<String> ranks, teams;
	
	//Handle payment
	ComboBox<String> cb; 
	ComboBox<Integer> cb2;
	TextField text, stats1, stats2, stats3;
	double totalAdminPrice = 0;
	GridPane settingsPaneChildOptions;
	
	public void init(DatabaseHandler dbHandler, MainTabController mtc) {
		this.dbHandler = dbHandler;
		this.mtc = mtc;
		

	}
	
	
	//Method used for unimplemented buttons
	@FXML void toBeDefined(ActionEvent event) {
		settingsPane.getChildren().clear();
		Object o = event.getSource();
		
//		if (o.equals(b0)) {
//			showContent = 0;
//		} else if (o.equals(b1)) {
//			showContent = 1;
//		}else 
			if (o.equals(b2)) {
			showContent = 2;
		}else if (o.equals(b3)) {
			showContent = 3;
		}else if (o.equals(b4)) {
			showContent = 4;
		}else if (o.equals(b5)) {
			showContent = 5;
		}
		setButtonHighlight();
	}
	
	//Button press "handle payment"
	@FXML void handlePayment() {
		
		showContent = 1;
		settingsPane.getChildren().clear();
		setButtonHighlight();
		
		settingsPaneChildOptions = new GridPane();
		
		Pane pane = new Pane();
		pane.setPrefHeight(50);
		settingsPane.add(pane, 0, 1);
		
		//Option 1
		Label label = new Label("Round");
		cb = new ComboBox<String>();
		cb.getItems().addAll("up", "off", "down");
		cb.setValue("up");
		Label label2 = new Label("to nearest");
		cb2 = new ComboBox<Integer>();
		cb2.getItems().addAll(1,2,5,10,20);
		cb2.setValue(5);
		

		GridPane settingsPaneChild = new GridPane();
		settingsPaneChild.setHgap(5);
		settingsPaneChild.add(label, 0, 0);
		settingsPaneChild.add(cb, 1, 0);
		settingsPaneChild.add(label2, 2, 0);
		settingsPaneChild.add(cb2, 3, 0);
		settingsPane.add(settingsPaneChild, 0, 2);
		
		Button exec = new Button("Execute");
		exec.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				executeRound();
			}
			
		});
		settingsPane.add(exec, 2, 2);
		settingsPane.add(DialogManager.createTooltip("Round off chosen users prices. \nThe information is session base, "
				+ "so it will reset if you exit this tab. Export payment to save information."
				+ "\n\n Press 'Esc' to close"), 3, 2);
		
		//Option 2
		Label label3 = new Label("Round up untill excess is ");
		text = new TextField();
		text.setPrefWidth(100);
		setIntegerRestriction(text);
		
		GridPane settingsPaneChild2 = new GridPane();
		settingsPaneChild2.setHgap(5);
		settingsPaneChild2.add(label3, 0, 0);
		settingsPaneChild2.add(text, 1, 0);
		settingsPane.add(settingsPaneChild2, 0, 3);
		
		Button exec2 = new Button("Execute");
		exec2.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				executeIncrease();
			}
			
		});
		settingsPane.add(exec2, 2, 3);
		settingsPane.add(DialogManager.createTooltip(""
				+ "Prices for chosen users are rounded up, and then increased by one untill goal excess is reached."
				+ "\nThe information is session based, "
				+ "so it will reset if you exit this tab. Export payment to save information."
				+ "\n\nPress 'Esc' to close"), 3, 3);
		
		
		
		//Separation line
		Pane line = new Pane();
		line.getStyleClass().add("line-separator");
		line.setPrefWidth(3);
		line.setMaxWidth(3);
		line.setMinWidth(3);
		GridPane.setHalignment(line, HPos.CENTER);
		settingsPane.add(line, 1, 2, 1, 4);
		
		setStats();
		setupHandleWasteGUI();
		
	}
	
	// Updating stat number
	private void setStats() {
		
		GridPane settingsPaneChild3 = new GridPane();
		settingsPaneChild3.add(new Label("To be collected"), 0, 0);
		settingsPaneChild3.add(new Label("Admin payed"), 0, 1);
		settingsPaneChild3.add(new Label("Difference"), 0, 2);
		stats1 = new TextField((!useRounded()
				? String.format("%.2f", Double.valueOf(userTable2.getItems().get(0).getPrice().replaceAll(",", ".")))
				: String.format("%.2f",
						Double.valueOf(userTable2.getItems().get(0).getRounded().replaceAll(",", ".")))));
		stats2 = new TextField(String.format("%.2f", totalAdminPrice));
		stats3 = new TextField(String.format("%.2f", (Double.valueOf(stats1.getText().replace(",", "."))
				- Double.valueOf(stats2.getText().replace(",", ".")))));
		stats1.setEditable(false);
		stats2.setEditable(false);
		stats3.setEditable(false);
		settingsPaneChild3.add(stats1, 1, 0);
		settingsPaneChild3.add(stats2, 1, 1);
		settingsPaneChild3.add(stats3, 1, 2);
		settingsPaneChild3.setHgap(10);
		settingsPaneChild3.setVgap(10);

//		Pane separator = new Pane();
//		separator.setMinHeight(50);
//		settingsPane.add(separator, 0, 2);
		settingsPane.add(settingsPaneChild3, 0, 0, 3, 1);
	}
	
	private void updateStats() {
		System.out.println("Use rounded, lol: " + useRounded());
		stats1.setText((!useRounded()
				? String.format("%.2f", Double.valueOf(userTable2.getItems().get(0).getPrice().replaceAll(",", ".")))
				: String.format("%.2f",	Double.valueOf(userTable2.getItems().get(0).getRounded().replaceAll(",", ".")))));
		stats2.setText(String.format("%.2f", totalAdminPrice));
		stats3.setText(String.format("%.2f", (Double.valueOf(stats1.getText().replace(",", "."))
				- Double.valueOf(stats2.getText().replace(",", ".")))));
	}
	
	// Handles the rounding in option 1 in handle payment
	private void executeRound() {
		int totalPrice = 0;
		int roundTo = cb2.getValue();
		
		boolean useRounded = useRounded();
		ArrayList<String> chosenUsers = getChosenUsers();
		for (UserTableEntry ute : userTable.getItems()) {
			if (chosenUsers.contains(ute.getId())) {
			double price = 0;
			if (useRounded) price = Double.valueOf(ute.getRounded().replace(",", "."));
			else price = Double.valueOf(ute.getPrice().replace(",", "."));
			int newPrice = 0;
			switch(cb.getValue().toLowerCase()) {
			case "up": {
				newPrice = (int) Math.ceil(price);
				if (newPrice % roundTo != 0) newPrice += roundTo - (newPrice % roundTo);
				ute.setRounded(newPrice);
				} break;
			case "down": {
				newPrice = (int) Math.floor(price);
				if (newPrice % roundTo != 0) newPrice -= newPrice % roundTo;
				ute.setRounded(newPrice);
				} break;
			case "off": {
				if (price % roundTo < roundTo / 2) {
					newPrice = (int) Math.floor(price);
					newPrice -= newPrice % roundTo;
				} else {
					newPrice = (int) Math.ceil(price);
					newPrice += roundTo - (newPrice % roundTo);
				}
				ute.setRounded(newPrice);
				} break;
			}
			totalPrice += newPrice;
			} else {
				if (!useRounded) {
					System.out.println("Setting rounded of: " + ute.getName());
					ute.setRounded(Double.valueOf(ute.getPrice().replace(",", ".")));
				}
			}
		}
		stats1.setText(calculateTotalPayment() + "");
		stats3.setText((Double.valueOf(stats1.getText().replace(",", ".")) - Double.valueOf(stats2.getText().replace(",", "."))) + "");
		userTable2.getItems().get(0).setRounded(calculateTotalPayment());
		userTable2.refresh();
		userTable.refresh();
		DialogManager.showPopup("Payment was rounded");
//		update();
	}
	
	private boolean useRounded() {
		int amountOfEmpty = 0;
		for (UserTableEntry ute : userTable.getItems()) {
			if (ute.getRounded().equals("0.0") || ute.getRounded().equals("0,0")) amountOfEmpty ++;
		}
		return amountOfEmpty < userTable.getItems().size();
	}
	
	public ArrayList<String> getChosenUsers() {
		ArrayList<String> chosenRanks = new ArrayList<String>(), chosenTeams = new ArrayList<String>();
		for (int i = 0; i < ranks.size(); i ++) {
			if (boxes.get(i).isSelected()) chosenRanks.add(texts.get(i).getText());
		}
		for (int i = ranks.size(); i < ranks.size() + teams.size(); i ++) {
			if (boxes.get(i).isSelected()) chosenTeams.add(texts.get(i).getText());
		}
		boolean includeRanks = !chosenRanks.isEmpty(), includeTeams = !chosenTeams.isEmpty();
		ArrayList<String> chosenUsers = new ArrayList<String>();
		for (User user : dbHandler.getUsers()) {
			if (includeRanks) {
				if (!chosenRanks.contains(user.getRank())) {
					continue;
				}
			}
			if (includeTeams) {
				if (!chosenTeams.contains(user.getTeam())) {
					continue;
				}
			}
			chosenUsers.add(user.getID());
		}
		return chosenUsers;
	}
	
	// Handles increasing the price in option 2 in handle payment
	private void executeIncrease() {
		
		
		ArrayList<String> chosenUsers = getChosenUsers();
		
		System.out.println("--------------------");
		boolean useRounded = useRounded();
		System.out.println("Use roundede: " + (useRounded));
		int toReach = Integer.valueOf(text.getText().equals("") ? "0" : text.getText());
		int people = chosenUsers.size();
		double excess = Double.valueOf(stats1.getText().replace(",", ".")) - totalAdminPrice;
		System.out.println("Current excess: " + excess);
		//TODO
		for (UserTableEntry ute : userTable.getItems()) {
			if (chosenUsers.contains(ute.getId())) {
				double val = Double.valueOf(useRounded ? ute.getRounded().replace(",", ".") : ute.getPrice().replace(",", ".")) % 1;
				excess += val > 0 ? 1 - val : Math.abs(val);
			}
		}
		System.out.println("Amount of people: " + people);
		System.out.println("New excess: " + excess);
		System.out.println("toReach: " + toReach + "; excess: " + excess);
		int toAdd = (int) Math.ceil((toReach - excess) / people);
		int totalPrice = 0;
		System.out.println("Toadd: " + toAdd);
		for (UserTableEntry ute : userTable.getItems()) {
			if (chosenUsers.contains(ute.getId())) {
				int newPrice = (int) Math.ceil(Double.valueOf(useRounded ? ute.getRounded().replace(",", ".") : ute.getPrice().replace(",", "."))) + toAdd;
				ute.setRounded(newPrice); 
				totalPrice += newPrice;
			} else {
				if (!useRounded) {
					System.out.println("Setting rounded of: " + ute.getName());
					ute.setRounded(Double.valueOf(ute.getPrice().replace(",", ".")));
				}
			}
		}
//		stats1.setText(totalPrice + "");
		stats1.setText(calculateTotalPayment() + "");
		stats3.setText((Double.valueOf(stats1.getText().replace(",", ".")) - Double.valueOf(stats2.getText().replace(",", "."))) + "");
		userTable2.getItems().get(0).setRounded(calculateTotalPayment());
		userTable2.refresh();
		userTable.refresh();
	}
	
	private double calculateTotalPayment() {
		double amount = 0.0;
		for (UserTableEntry ute : userTable.getItems()) {
			amount += Double.valueOf(ute.getRounded().replace(",", "."));
		}
		return amount;
	}
	
	// Restricts textfield to integer format
	private void setIntegerRestriction(TextField text) {
		text.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d*")) {
					text.setText(newValue.replaceAll("[^\\d]", ""));
				}
			}
		});
	}
	
	// Restricts textfield to double format
	public void setDoubleRestriction(TextField t) {
		Pattern validDoubleText = Pattern.compile("-?((\\d*)|(\\d+\\.\\d*))");

		TextFormatter<Double> textFormatter = new TextFormatter<Double>(new DoubleStringConverter(), 0.0, change -> {
			String newText = change.getControlNewText();
			if (validDoubleText.matcher(newText).matches()) {
				return change;
			} else
				return null;
		});
		t.setTextFormatter(textFormatter);
	}
	
	// Button press "export payment"
	@FXML void exportPayment() {
//		showContent = 2;
//		settingsPane.getChildren().clear();
//		setButtonHighlight();
		File saveFile = DialogManager.saveFile(new String[]{"csv"}, new File(GlobalVariables.databasePath).getParent());
		if (saveFile != null) {
			boolean useRounded = !userTable2.getItems().get(0).getRounded().equals("0");
			try (BufferedWriter writer = new BufferedWriter(new FileWriter(saveFile));) {
				StringBuilder sb = new StringBuilder();
				for (UserTableEntry ute : userTable.getItems()) {
					sb.append(ute.getName() + ";");
					for (int i = 26; i > ute.getName().length(); i --) {
						sb.append(" ");
					}
					if (useRounded) {
						 sb.append(ute.getRounded() + "\n");
					} else {
						sb.append(ute.getPrice() + "\n");
					}
				}
				writer.append(sb);
				DialogManager.showPopup("Exported payment successfully");
			} catch (IOException e) {
				DialogManager.alertError("IOException", null, "LOL");
			}
		}
	}
	
	// Button press "handle waste"
	@FXML void handleWaste() {
		showContent = 0;
		settingsPane.getChildren().clear();
		setupHandleWasteGUI();
	}
	
	// Sets up waste gui
	private void setupHandleWasteGUI() {
		
		// Start at row index 4
		
		setButtonHighlight();
		ranks = GlobalVariables.getRanks();
		teams = GlobalVariables.getTeams();
		
		//Mark or unmark all
		Label allRanks = new Label("All"), allTeams = new Label("All");
		CheckBox allRanksCheck = new CheckBox(), allTeamsCheck = new CheckBox();
		GridPane settingsPaneChild4 = new GridPane();
		settingsPaneChild4.setHgap(10);
		settingsPaneChild4.add(allRanks, 0, 3);
		settingsPaneChild4.add(allRanksCheck, 1, 3);
		settingsPaneChild4.add(allTeams, 3, 3);
		settingsPaneChild4.add(allTeamsCheck, 4, 3);
		
		allRanksCheck.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				for (int i = 0; i < ranks.size(); i ++ ){
					boxes.get(i).selectedProperty().set(allRanksCheck.isSelected());
				}
			}
			
		});
		allTeamsCheck.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				for (int i = 0; i < teams.size(); i ++ ){
					boxes.get(ranks.size() + i).selectedProperty().set(allTeamsCheck.isSelected());
				}
			}
			
		});
		
		//Main options, checkboxes and labels
		boxes = new ArrayList<CheckBox>();
		texts = new ArrayList<Label>();
		Pane line = new Pane();
		for (int i = 0; i < ranks.size(); i ++) {
			newRow(i + 1 + 3, ranks.get(i), 0, settingsPaneChild4);
		}
		for (int i = 0; i < teams.size(); i ++) {
			newRow(i + 1 + 3, teams.get(i), 3, settingsPaneChild4);
		}
		
		//Red separator line
		line.getStyleClass().add("line-separator");
		line.setPrefWidth(3);
		line.setMaxWidth(3);
		line.setMinWidth(3);
		GridPane.setHalignment(line, HPos.CENTER);
		int highestRow = Math.max(ranks.size(), teams.size()) + 1;
		settingsPaneChild4.add(line, 2, 3, 1, highestRow);
		
		//Execution buttons
		//TODO
		Label wasteEvenLabel = new Label("Apply waste evenly"), wastePercentLabel = new Label("Apply waste percentwise");
		Button wasteEven = new Button("Execute"), wastePercent = new Button("Execute");
		wasteEven.setMaxWidth(Double.MAX_VALUE);
		wastePercent.setMaxWidth(Double.MAX_VALUE);
		settingsPane.add(wasteEven, 2, 4);
		settingsPane.add(wastePercent, 2, 5);
		settingsPane.add(wasteEvenLabel, 0, 4);
		settingsPane.add(wastePercentLabel, 0, 5);
		GridPane.setFillWidth(wasteEven, true);
		GridPane.setFillWidth(wastePercent, true);
		wasteEven.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				distributeWaste(true);
			}
		});
		wastePercent.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				distributeWaste(false);
			}
		});
		
		//Tooltip
		Button tooltip = DialogManager.createTooltip("Distribute waste evenly amongst chosen users. "
				+ "The chosen users are those whose rank AND team match the selection.\n"
				+ "If no ranks are chosen, it will count as if all ranks are chosen, same goes for teams."
				+ "\n\n Press 'Esc' to close");
		settingsPane.add(tooltip, 3, 4);
		Button tooltip2 = DialogManager.createTooltip("Distribute waste percentwise amongst all users. "
				+ "Each user will pay relative to what they have already bought."
				+ "\n\n Press 'Esc' to close");
		settingsPane.add(tooltip2, 3, 5);
		Pane spacer = new Pane();
		spacer.setPrefHeight(50);
		settingsPane.add(spacer, 0, 6);
//		Pane spacer2 = new Pane();
//		spacer2.setPrefHeight(50);
//		settingsPaneChild4.add(spacer2, 0, highestRow + 3);
		settingsPane.add(settingsPaneChild4, 0, 7, 4, 1);
	}
	
	// Handles the waste distribution in "handle waste" -> "apply waste"
	private void distributeWaste(boolean even) {

		ArrayList<User> chosenUsers = new ArrayList<User>();
		//Get users to distribute to, chosenUsers
		if (even) {
		ArrayList<String> chosenRanks = new ArrayList<String>(), chosenTeams = new ArrayList<String>();
		for (int i = 0; i < ranks.size(); i ++) {
			if (boxes.get(i).isSelected()) chosenRanks.add(texts.get(i).getText());
		}
		for (int i = ranks.size(); i < ranks.size() + teams.size(); i ++) {
			if (boxes.get(i).isSelected()) chosenTeams.add(texts.get(i).getText());
		}
		boolean includeRanks = !chosenRanks.isEmpty(), includeTeams = !chosenTeams.isEmpty();
		for (User user : dbHandler.getUsers()) {
			if (includeRanks) {
				if (!chosenRanks.contains(user.getRank())) {
					continue;
				}
			}
			if (includeTeams) {
				if (!chosenTeams.contains(user.getTeam())) {
					continue;
				}
			}
			chosenUsers.add(user);
		}
		} else {
			chosenUsers.addAll(dbHandler.getUsers());
		}
		
		
		
		//Get products and waste rates
		ArrayList<Double> waste = new ArrayList<Double>();
		ArrayList<String> IDS = new ArrayList<String>();
		ArrayList<Double> rate = new ArrayList<Double>();

		ArrayList<String> difference = new ArrayList<String>();
		for (Product product : dbHandler.getProducts()) {
			difference.add(product.getID());
			difference.add("");
			IDS.add(product.getID());
			waste.add(product.getTotalStock() - product.getCurrentStock() - product.getSold());
			if (even) {
				rate.add(waste.get(waste.size() - 1) / chosenUsers.size());
			} else {
				rate.add((product.getTotalStock() - product.getCurrentStock()) / product.getSold());
			}
		}
		
		//Distribute the waste
		ArrayList<ArrayList<String>> outer = new ArrayList<ArrayList<String>>();
		for (User user : chosenUsers) {
			ArrayList<String> inner = new ArrayList<String>();
			inner.add(user.getID());
			for (int i = 0; i < IDS.size(); i ++) {
				UserProduct up = user.getProductFromID(IDS.get(i));
				if (even) {
					up.setAmount(up.getAmount() + rate.get(i));
				} else {
					up.setAmount(up.getAmount() * rate.get(i));
				}
				inner.add(difference.get(i * 2));
				inner.add(up.getAmount() + "");
//				difference.set(i * 2 + 1, up.getAmount() + "");
			}
			outer.add(inner);
//			dbHandler.editInformation("users", difference, user.getID());
		}
		if (!dbHandler.editMultiple("users", outer)) {
			DialogManager.showPopup("Failed to distribute waste");
			
		}
		dbHandler.updateAllProductSold();
		update();
		updateStats();
		DialogManager.showPopup("Waste was successfully distributed " + 
		(even ? "evenly" : "percentwise"));
	}
	
	// Adds a set of a label and a checkbox for the waste handle
	private void newRow(int i, String s, int col, GridPane settingsPaneChild4) {
		CheckBox tempBox = new CheckBox();
		Label tempLabel = new Label(s);
		boxes.add(tempBox);
		texts.add(tempLabel);
		settingsPaneChild4.add(tempLabel, col, i);
		settingsPaneChild4.add(tempBox, col + 1, i);
	}
	
	// Handles the highlight of current button in menu
	private void setButtonHighlight() {
//		b0.getStyleClass().remove("buttonHighlight");
//		b1.getStyleClass().remove("buttonHighlight");
		b2.getStyleClass().remove("buttonHighlight");
		switch(showContent) {
//		case 0: b0.getStyleClass().add("buttonHighlight"); break;
//		case 1: b1.getStyleClass().add("buttonHighlight"); break;
		case 2: b2.getStyleClass().add("buttonHighlight"); break;
		}
	}
	
	public void tabChanged() {
		System.out.println("Tab changed");
		update();
//		if (showContent != 1) {
//			System.out.println("Handling payment");
//			handlePayment();
//		} else {
//			updateStats();
//		}
		handlePayment();
		if (showContent == 1) updateStats();
	}
	
	// Loads data to tableViews
	public void update() {
		//Removes header of total tableviews
		Pane header = (Pane) userTable2.lookup("TableHeaderRow");
		 if (header.isVisible()){
	            header.setMaxHeight(0);
	            header.setMinHeight(0);
	            header.setPrefHeight(0);
	            header.setVisible(false);
	    }
		header = (Pane) productTable2.lookup("TableHeaderRow");
		if (header.isVisible()){
	            header.setMaxHeight(0);
	            header.setMinHeight(0);
	            header.setPrefHeight(0);
	            header.setVisible(false);
		}
		
		userTable.getItems().clear();
		userTable2.getItems().clear();
		productTable.getItems().clear();
		productTable2.getItems().clear();
		double totalBought = 0.0, totalPrice = 0.0;
		double[] totals = new double[5];
		ArrayList<Product> products = dbHandler.getProducts();
		ArrayList<String> IDS = new ArrayList<String>();
		ArrayList<Double> prices = new ArrayList<Double>();
		totalAdminPrice = 0;
		
		// Load productTable
		for (Product p : products) {
			IDS.add(p.getID());
			prices.add(p.getPrice());
			double waste = p.getTotalStock() - p.getCurrentStock() - p.getSold();
			productTable.getItems().add(new ProductTableEntry(
				p.getName(), p.getTotalStock(), p.getCurrentStock(), p.getSold(), 
				waste, p.getPrice()));
			totals[0] += p.getTotalStock();
			totals[1] += p.getCurrentStock();
			totals[2] += p.getSold();
			totals[3] += waste;
			totals[4] += p.getPrice();
			totalAdminPrice += (p.getTotalStock() - p.getCurrentStock()) * p.getPrice();
		}
		
		// Load userTable
		for (User user : dbHandler.getUsers()) {
			double price = 0.0;
			for (UserProduct up : user.getUserProducts()) {
				for (int i = 0; i < IDS.size(); i ++) {
					if (up.getProductID().equals(IDS.get(i))) {
						price += up.getAmount() * prices.get(i);
					}
				}
			}
			totalPrice += price;
			totalBought += user.getTotalItems();
			userTable.getItems().add(new UserTableEntry(user.getName(), user.getID(), user.getTotalItems(), price));
		}
		
		// Load totaltables
		userTable2.getItems().add(new UserTableEntry("Total", "", totalBought, totalPrice));
		productTable2.getItems().add(new ProductTableEntry("Total", totals[0], totals[1], totals[2], totals[3], totals[4]));
		
		// Load payment info
//		updateStats();
		
	}
	
	
}
