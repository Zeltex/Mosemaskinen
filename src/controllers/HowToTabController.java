package controllers;

import java.net.URL;
import java.util.ResourceBundle;

import database.DatabaseHandler;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.layout.GridPane;
import managers.DialogManager;

public class HowToTabController implements Initializable {
	DatabaseHandler dbHandler;
	MainTabController mtc;
    
    
	@FXML GridPane contentPane, graphPane;
	@FXML TextArea content;
	@FXML ListView<String> overview;
	String[] names = new String[]{"GettingStarted", "General", "Payment", "Users", "Products", "CRT", "IO", "Barcodes","Transactions", "Advanced"};
	String[] titles = new String[]{"Getting started", "General use", "Waste/Payment", "Users", 
			"Products", "Teams/Ranks/Categories", "Import/Export users/products","Export barcodes","Transactions", "Advanced settings"};
	String[] pages = new String[names.length];
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {		
	}
	
	public void init(DatabaseHandler dbHandler, MainTabController mtc) {
		this.dbHandler = dbHandler;
		this.mtc = mtc;
		content.setWrapText(true);
		content.setEditable(false);
		for (int i = 0; i < names.length; i ++) {
			pages[i] = DialogManager.loadText("/resources/howTo/" + names[i] + ".txt");		
//			System.out.println(pages[i]);
//			System.out.println("----------------------");
			overview.getItems().add(titles[i]);
		}
		overview.getSelectionModel().select(0);
		content.setText(pages[0]);
		overview.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> arg0, String oldC, String newC) {
				content.setText(pages[overview.getSelectionModel().getSelectedIndex()]);
			}
		});
		
	}
	
	public void update() {
		
	}

}
