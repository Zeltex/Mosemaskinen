package controllers;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import database.DatabaseHandler;
import database.GlobalVariables;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.util.Duration;
import main.Main;
import objects.Product;
import tableEntries.Transaction;

public class MainTabController implements Initializable{
	
	@FXML private UserTabController userTabController;
	@FXML private ProductTabController productTabController;
	@FXML private SettingsTabController settingsTabController;
	@FXML private HowToTabController howToTabController;
	@FXML private EconomyTabController economyTabController;
	@FXML private TransactionsTabController transactionsTabController;
	
	@FXML private ImageView banner;
	
	@FXML private Tab tabUserBar, tabProductBar, tabSettingsBar, tabStatsBar;
	@FXML VBox pane;
	@FXML TabPane tabPane;
	@FXML Label bannerText;
	
	String tabUser, tabProduct, tabSettings, tabStats;
	
	DatabaseHandler dbHandler;
	Main main;
	static double counter;
	@Override
	public void initialize(URL location, ResourceBundle resources) {
//		pane.setOnMouseClicked(new EventHandler<MouseEvent>() {
//
//			@Override
//			public void handle(MouseEvent event) {
//				// TODO Auto-generated method stub
//				System.out.println("Main pane clicked");
//				if (GlobalVariables.backupInProgress) {
//					event.consume();
//				}
//			}
//			
//		});
//		init();
		
	}
	
	
//	public DatabaseHandler createDbHandler() {
//		dbHandler = new DatabaseHandler(this);
//		return dbHandler;
//	}
	public void setDBHandler(DatabaseHandler dbHandler) {
		this.dbHandler = dbHandler;
	}
	
	public String getStyleSheet() {
		return main.getStyleSheet();
	}
		
	
	public void setTheme(int theme) {
		main.setTheme(theme);
	}
	
	public void setMain(Main main) {
		this.main = main;
	}
	
	public void setUserScene() {
		main.setUserScene();
	}
	
	@FXML void showUserPage() {
		main.setUserScene();
	}
	
	public Window getWindow() {
		return main.getAdminWindow();
	}
	
	public Stage getStage() {
		return main.getStage();
	}
	
	public void init() {
		GlobalVariables.init();
		
		tabPane.getSelectionModel().selectedItemProperty().addListener(
			    new ChangeListener<Tab>() {
			        @Override
			        public void changed(ObservableValue<? extends Tab> ov, Tab t, Tab t1) {
			            switch(t1.getText().toLowerCase()) {
			            case "users": userTabController.load(); break;
			            case "products": productTabController.load(); break;
			            case "settings": settingsTabController.resetContent(); settingsTabController.refreshVariables(); break;
			            case "how to": howToTabController.update(); break;
			            case "transactions": transactionsTabController.loadTransactions(false, "", ""); break;
			            case "economy": economyTabController.tabChanged(); break;
			            }
			        }
			    }
			);
		
//		dbHandler = new DatabaseHandler(this);
		userTabController.init(dbHandler, this);
		settingsTabController.init(dbHandler, this);
		productTabController.init(dbHandler, this);
		howToTabController.init(dbHandler, this);
		economyTabController.init(dbHandler, this);
		transactionsTabController.init(dbHandler);
		load();
		dbHandler.cleanupRTC();
		userTabController.updateComboBoxes();
		productTabController.updateComboBoxes();
		setBanner();
	}
	
	public boolean validateDatabaseSync() {
		return transactionsTabController.validateDatabaseSync();
	}
	
	public void initBackup() {
		settingsTabController.setupBackup();
	}
	
	public void addTransaction(String userName, String productName, double amount, boolean absolute, String productID, 
			String userID, boolean deprecated) {
		transactionsTabController.addTransaction(userName, productName, amount, absolute, productID, userID, deprecated);
	}
	
	public void addTransactions(ArrayList<Transaction> t) {
		transactionsTabController.addTransactions(t);
	}
	
	public void setDepricated(String[] userID, String[] productID) {
		transactionsTabController.setDeprecated(userID, productID);
	}
	
	public boolean resetTransactions() {
		return transactionsTabController.resetTransactions();
	}
	
	public void insertUsers() {
//		dbHandler.addUser(new User("Pild ved Grisling", "Beer guy", "", "Vector", "Luder", "s------", "Vesterborg", 1, 1000L, null));
//		dbHandler.addUser(new User("Han er Mantana", "Transtana", "", "Vector", "Luder", "s------", "Vesterborg", 1, 1001L, null));
//		dbHandler.addUser(new User("Bad Farlig", "Golden boy", "", "Vector","Luder", "s------", "Vesterborg", 1, 1002L, null));
//		dbHandler.addUser(new User("Can touch tits", "Tits", "", "Vector","Luder", "s------", "Vesterborg", 2, 1003L, null));
//		dbHandler.addUser(new User("Pop pop Uranus", "Uranus", "", "Vector","Luder", "s------", "Vesterborg", 2, 1004L, null));
//		dbHandler.addUser(new User("King of Cock", "King", "", "Vector","Luder", "s------", "Vesterborg", 1, 1005L, null));
//		dbHandler.addUser(new User("Guld Fjaps", "Fjaps", "", "Vector","Luder", "s------", "Vesterborg", 2, 1006L, null));
		load();
	}
	
	public void insertProducts() {
		dbHandler.addProduct(new Product("p999", "Royal Export", 105, 5, 45, "Beer", 7,0L));
		dbHandler.addProduct(new Product("p998", "Royal Classic", 110, 15, 45, "Beer", 8,0L));
		dbHandler.addProduct(new Product("p997", "Green Tuborg", 115, 25, 45, "Beer", 1,0L));
		dbHandler.addProduct(new Product("p996", "Carlsberg Classic", 120, 55, 45, "Beer", 0,0L));
		dbHandler.addProduct(new Product("p995", "Tuborg Classic", 125, 155, 45, "Beer", 2,0L));
		load();
	}
	
	public void updateUserComboBoxes() {
		userTabController.updateComboBoxes();
	}
	public void updateProductComboBoxes() {
		productTabController.updateComboBoxes();
	}
	
	public void load() {
//		 userTabController.importUsers(null);
		dbHandler.load();
		productTabController.load();
		userTabController.load();
		transactionsTabController.loadLatest();
//		dbHandler.updateProductList(productTabController.getListView());
	}
	
	public void updateUserList() {
		userTabController.load();
	}
	
	public void updateProductList() {
		productTabController.load();
	}
	
	public void updateLists() {
		userTabController.load();
		productTabController.load();
	}
	
	public void setBanner() {
		if (!GlobalVariables.bannerIconPath.isEmpty()) {
			File f = new File(GlobalVariables.bannerIconPath);
			if (f.exists()) {
				Image image = new Image(f.toURI().toString());
				banner.setImage(image);
			} else {
				dbHandler.editSettings("bannerImage", "");
				GlobalVariables.setBannerLocation("");
			}
		}
		
	}
	
	public void animate() {
		counter = 0;
		Timeline backupTimer = new Timeline(new KeyFrame(Duration.seconds(0.1), event -> {

			// Async task 
			Task<Void> task = new Task<Void>() {
				@Override
				public Void call() throws Exception {
					updateMessage("started");
//					sta.backupDatabase();
//					Thread.sleep(1000);
					updateMessage("done");
					return null;
				}
			};
			// Display message
			task.messageProperty().addListener((obs, oldMessage, newMessage) -> {
//				if (newMessage.equals("started")) {
					bannerText.setTextFill(new Color(counter,0,0,1));
//					Platform.runLater(() -> bannerText.setTextFill(new Color(0,0,0,1)));
//					bannerText.setTextFill(Color.YELLOW);
//					Platform.runLater(() -> bannerText.setTextFill(Color.YELLOW));
					counter = (counter + 0.02) % 1;
//				} else {
////					popup.hide();
////					GlobalVariables.backupInProgress = false;
//				}
			});
			new Thread(task).start();
		
		}));
		backupTimer.setCycleCount(Animation.INDEFINITE);

		backupTimer.play();
	}

}
