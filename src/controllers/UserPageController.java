package controllers;

import java.io.File;
import java.util.ArrayList;
import java.util.regex.Pattern;

import database.DatabaseHandler;
import database.GlobalVariables;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.StackedBarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.util.Callback;
import javafx.util.Duration;
import javafx.util.converter.DoubleStringConverter;
import main.Main;
import managers.DialogManager;
import objects.CustomSeries;
import objects.Product;
import objects.User;
import objects.UserProduct;

public class UserPageController {
	
	DatabaseHandler dbHandler;
	Main main;
	
    @FXML CheckBox xAxisRanks, xAxisTeams, xAxisGender, barCategories, barAllItems,
    proportionTotal, proportionAverage;
    @FXML ComboBox<Product> barSingleItem;
	public int xChosen, yChosen, propChosen;
    
	@FXML GridPane contentPane, graphPane;
	@FXML VBox options;
	@FXML VBox mainPane, instructions;
	@FXML HBox content, userContent;
	
	@FXML ImageView banner;
	
	boolean state;
	User chosenUser;
	Product chosenProduct;
	
	// Transaction view
	HBox transactionView;
	@FXML ListView<UserProduct> shoppingCart;
	@FXML GridPane userInfo, userGraph;
	@FXML TextField infoUserName, infoTeam, infoRank, infoFavorite;
	Label infoUserNameLabel, infoTeamLabel, infoRankLabel, infoItemsLabel;
	CategoryAxis xAxisT;
	NumberAxis yAxisT;
    BarChart<String, Number> bcT;
    double lowestValueT, highestValueT;
	
	
	// Debug transaction
	TextField productBarCode, product, userBarCode, user, amount;
	Button execute;
	GridPane debugTransactionPane;
	
	// Bar chart
	CategoryAxis xAxis;
	NumberAxis yAxis;
    StackedBarChart<String, Number> bc;
    double lowestValue, highestValue;
    Product singleItemChosen;
    boolean ignoreCombo = false;
    ArrayList<CustomSeries> series;
    
    static double counter;
    @FXML Label bannerText;
	
	public void init(DatabaseHandler dbHandler) {
		this.dbHandler = dbHandler;
		xAxisTeams.setSelected(true);
		barCategories.setSelected(true);
		proportionAverage.setSelected(true);
		xChosen = 1;
		yChosen = 0;
		propChosen = 1;
		chosenUser = null;
		singleItemChosen = new Product("None");

		xAxis = new CategoryAxis();
        yAxis = new NumberAxis();
        bc = new StackedBarChart<String,Number>(xAxis, yAxis);
        graphPane.add(bc, 0, 0);
        series = new ArrayList<CustomSeries>();
         
        xAxisT = new CategoryAxis();
        yAxisT = new NumberAxis();
        bcT = new BarChart<String,Number>(xAxisT, yAxisT);
        userGraph.add(bcT, 0, 0); 
         
//		setupChart();
//		setupDebugTransaction();
		setBanner();
		setupTransactionView();
		setComboListener();
		resetCombo(false);
		state = true;
		changeState();
//		options.getChildren().remove(pane);
	}
	
	public void update() {
		resetCombo(true);
	}
	
	public void isVisible() {
		System.out.println("SingleItem in isvisible: "  + singleItemChosen);
		Platform.runLater(() -> {setupChart();});
		ignoreCombo = false;
	}
	
	public void setBanner() {
		if (!GlobalVariables.bannerIconPath.isEmpty()) {
			File f = new File(GlobalVariables.bannerIconPath);
			if (f.exists()) {
				Image image = new Image(f.toURI().toString());
				banner.setImage(image);
			} else {
				dbHandler.editSettings("bannerImage", "");
				GlobalVariables.setBannerLocation("");
			}
		}
		
	}
	
	public void setMain(Main main) { 
		this.main = main;
	}
	
	@FXML void bannerClicked() {
		boolean loggedIn = DialogManager.passwordDialog(
				main.getUserWindow(), GlobalVariables.getPassword(), main.getStyleSheet());
		if (loggedIn) {
			if (state) changeState();
			main.setAdminScene();
		} else {
		}
//		DialogManager.getPassword("Login as admin", null, "Password", "Password", main.getWindow());
		
	
	}
	
	@FXML void changeToStats() {
		if (state) changeState();
	}
	
	// Received a number input
	public void barCodeInput(String input) {
		if (!input.isEmpty()) {
		long barCode = Long.valueOf(input);
		
		//Barcode scanned
		if (barCode >= 0 && barCode < 100) { 
			if (chosenProduct != null) {
				UserProduct temp = null;
				
				//Check if product has already been scanned
				for (UserProduct up : shoppingCart.getItems()) {
					if (up.getProductID().equals(chosenProduct.getID())) {
						if (barCode == 0) {
							temp = up;
						} else {
							up.setAmount(barCode);
						}
					}
				}
				if (temp != null) {
//					shoppingCart.getItems().remove(temp);
					shoppingCart.getItems().clear();
				}
				updateShoppingCart();
				
			}
		// User scanned
		} else if (barCode > 1000 && barCode < 10000) {
			User u = dbHandler.getUserFromBarCode(barCode);
			if (u == null) {
				
			} else {
				// If purchase in progress
				if (state) {
					if (chosenUser != null) {
						// Complete purchase
						if (u.getID().equals(chosenUser.getID())) {
							// saveContent();
							ArrayList<String> difference = new ArrayList<String>();
							for (UserProduct up : shoppingCart.getItems()) {
								difference.add(up.getProductID());
								difference.add((up.getAmount() + 
									chosenUser.getProductFromID(up.getProductID()).getAmount()) + "");
							}
							if (!dbHandler.editInformation("users", difference, chosenUser.getID())) {
								DialogManager.showPopup("Failed to save transactions");
								return;
							}
							dbHandler.updateUser(difference, chosenUser.getID());
							for (UserProduct up : shoppingCart.getItems()) {
								main.addTransaction(chosenUser.getName(), up.getProductName(), up.getAmount(), 
										false, up.getProductID(), chosenUser.getID(), false);
								dbHandler.updateProductSold(up.getProductID());
							}
							DialogManager.showPopup("Saved transactions");
							setupChart();
							clearUserInfo();
							changeState();
							// Change user
						} else {
							chosenUser = u;
							showUserInfo();
						}
					} else {
						showUserInfo();
						changeState();
						
					}
				// If showing graph
				} else {
					chosenUser = u;
					showUserInfo();
					changeState();
				}
			}
			
		// Product scanned
		} else if (barCode > 100000) {
			Product p = dbHandler.getProductFromBarCode(barCode);
			if (p == null) {
				
			} else {
				chosenProduct = p;
				boolean found = false;
				int counter = 0;
				for (UserProduct up : shoppingCart.getItems()) {
					if (up.getProductID().equals(chosenProduct.getID())) {
						found = true;
						System.out.println(up.getProductName() + " scanned");
						for (UserProduct up2 : shoppingCart.getItems()) {
							if (up2.getProductID().equals(chosenProduct.getID())) {
								
								up2.setAmount(up2.getAmount() + 1);
								updateShoppingCart();
							}
						}
//						shoppingCart.getItems().get(counter).
					}
					counter ++;
				}
				if (!found) shoppingCart.getItems().add(
						new UserProduct(chosenProduct.getID(), chosenProduct.getName(), 1));
			}
		}
		}
	}
	
	private void updateShoppingCart() {
		ObservableList<UserProduct> list = FXCollections.observableArrayList();
		list.addAll(shoppingCart.getItems());
		shoppingCart.getItems().clear();
		shoppingCart.getItems().addAll(list);
	}
	
	private void showUserInfo() {
		String name = chosenUser.getName();
//		infoUserName.setText(chosenUser.getName());
		infoUserName.getStyleClass().add("User-Page-Info");
		infoTeam.getStyleClass().add("User-Page-Info");
		infoRank.getStyleClass().add("User-Page-Info");
		infoFavorite.getStyleClass().add("User-Page-Info");
		infoUserName.setText(name);
		infoTeam.setText(chosenUser.getTeam());
		infoRank.setText(chosenUser.getRank());
		infoFavorite.setText(chosenUser.getFavoriteItem());
		shoppingCart.getStyleClass().add("shoppingcart");
		shoppingCart.setPrefWidth(400);
		shoppingCart.getItems().clear();
		
		setupUserGraph();
	}
	
	private void setupUserGraph() {
		highestValueT = 0.0;
		lowestValueT = Double.MAX_VALUE;
		userGraph.getChildren().remove(bcT);
		xAxisT = new CategoryAxis();
        yAxisT = new NumberAxis();
		bcT = new BarChart<String,Number>(xAxisT, yAxisT);
        userGraph.add(bcT, 0, 0);
        bcT.getData().clear();
        bcT.setPrefSize(500, 500);
        
        
        
        ArrayList<String> texts = new ArrayList<String>();
		ArrayList<Double> values = new ArrayList<Double>();
       
		for (UserProduct up : chosenUser.getUserProducts()) {
			texts.add(up.getProductName());
			values.add(up.getAmount());
		}
     	
     	// Create series
       XYChart.Series<String, Number> series1 = new XYChart.Series<String, Number>();
       
       // Add
		while (texts.size() > 0) {
			double highest = 0;
			int index = 0;
			for (int i = 0; i < texts.size(); i++) {
				if (values.get(i) > highest) {
					highest = values.get(i);
					index = i;
				}
				if (values.get(i) < lowestValueT) lowestValueT = values.get(i);
			}
			if (highest > highestValueT) highestValueT = highest;
			series1.getData().add(new XYChart.Data<String, Number>(texts.get(index),values.get(index)));
			
			texts.remove(index);
			values.remove(index);
		}
		bcT.getData().add(series1);
      

        // Setup y axis ranges
        yAxisT.setAutoRanging(false);
        yAxisT.setUpperBound(Math.ceil(highestValueT));
        yAxisT.setLowerBound(0);
        yAxisT.setTickUnit(Math.ceil(highestValueT / 5));
        
	}
	
	private void clearUserInfo() {
		chosenUser = null;
		chosenProduct = null;
		infoUserName.setText("");
		infoTeam.setText("");
		infoRank.setText("");
		infoFavorite.setText("");
		shoppingCart.getItems().clear();
	}
	
	private void changeState() {
		if (state) {
			state = false;
//			if (!content.getChildren().contains(options)) content.getChildren().add(options);
//			if (!content.getChildren().contains(graphPane)) content.getChildren().add(graphPane);
			if (!mainPane.getChildren().contains(content)) mainPane.getChildren().add(content);
			mainPane.getChildren().remove(instructions);
			mainPane.getChildren().remove(userContent);
		} else {
			state = true;
//			content.getChildren().clear();
			mainPane.getChildren().remove(content);
//			setupTransactionView();
			if (!mainPane.getChildren().contains(userContent)) mainPane.getChildren().add(userContent);
			if (!mainPane.getChildren().contains(instructions)) mainPane.getChildren().add(instructions);
		}
	}
	
	private void setupTransactionView() {
		shoppingCart.setCellFactory(new Callback<ListView<UserProduct>, ListCell<UserProduct>>(){
            @Override
            public ListCell<UserProduct> call(ListView<UserProduct> p) {
                ListCell<UserProduct> cell = new ListCell<UserProduct>(){
                    @Override
                    protected void updateItem(UserProduct userProduct, boolean empty) {
                        super.updateItem(userProduct, empty);
                        if (userProduct != null && !empty) {
//                        	setTextFill();
                            setText(userProduct.getShoppingCartFormat());
                        } else {
                        	setText("");
                        }
                    }
                };
                return cell;
            }
        });
		
	}
	
	
	@SuppressWarnings("unused")
	private void setupDebugTransaction() {
		userBarCode = new TextField();
		user = new TextField();
		user.setEditable(false);
		userBarCode.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d*")) {
					userBarCode.setText(newValue.replaceAll("[^\\d]", ""));
				} else {
					if (!newValue.isEmpty()) {
						User u = dbHandler.getUserFromBarCode(Integer.valueOf(newValue));
						if (u == null) {
							user.setText("");
						} else {
							user.setText(u.getName());
						}
					}
				}
			}
		});
		productBarCode = new TextField();
		product = new TextField();
		product.setEditable(false);
		productBarCode.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d*")) {
					productBarCode.setText(newValue.replaceAll("[^\\d]", ""));
				} else {
					if (!newValue.isEmpty()) {
						Product p = dbHandler.getProductFromBarCode(Integer.valueOf(newValue));
						if (p == null) {
							product.setText("");
						} else {
							product.setText(p.getName());
						}
					}
				}
			}
		});
		amount = new TextField();
		setDoubleRestriction(amount);
		execute = new Button("Execute transaction");
		execute.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (product.getText().isEmpty() || user.getText().isEmpty()) {
					DialogManager.showPopup("Empty field");
				} else {
					User user = dbHandler.getUserFromBarCode(Integer.valueOf(userBarCode.getText()));
					Product product = dbHandler.getProductFromBarCode(Integer.valueOf(productBarCode.getText()));
					executeTransaction(user, product, Double.valueOf(amount.getText()));
				}
			}
		});
		debugTransactionPane = new GridPane();
		debugTransactionPane.add(userBarCode, 1, 1);
		debugTransactionPane.add(user, 1, 2);
		debugTransactionPane.add(productBarCode, 1, 3);
		debugTransactionPane.add(product, 1, 4);
		debugTransactionPane.add(amount, 1, 5);
		debugTransactionPane.add(execute, 1, 6);
		debugTransactionPane.add(new Label("user bar code"), 0, 1);
		debugTransactionPane.add(new Label("user name"), 0, 2);
		debugTransactionPane.add(new Label("product bar code"), 0, 3);
		debugTransactionPane.add(new Label("product name"), 0, 4);
		debugTransactionPane.add(new Label("amount"), 0, 5);
		options.getChildren().add(debugTransactionPane);
		
	}
	
	public void executeTransaction(User user, Product product, double amount) {
		UserProduct up = user.getProductFromID(product.getID());
		ArrayList<String> difference = new ArrayList<String>();
		difference.add(product.getID());
		difference.add((up.getAmount() + amount) + "");
		if (!dbHandler.editInformation("users", difference, user.getID())) {
			DialogManager.showPopup("Transaction failed");

		} else {

			up.setAmount(up.getAmount() + amount);
			main.addTransaction(user.getName(), product.getName(), amount, false, product.getID(), user.getID(), false);

			dbHandler.updateProductSold(product.getID());
			main.updateLists();

			DialogManager.showPopup("Transaction completed");
		}
	}
	
	@SuppressWarnings("unused")
	private void setIntegerRestriction(TextField text) {
		text.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if (!newValue.matches("\\d*")) {
					text.setText(newValue.replaceAll("[^\\d]", ""));
				}
			}
		});
	}
	
	public void setDoubleRestriction(TextField t) {
		Pattern validDoubleText = Pattern.compile("-?((\\d*)|(\\d+\\.\\d*))");

		TextFormatter<Double> textFormatter = new TextFormatter<Double>(new DoubleStringConverter(), 0.0, change -> {
			String newText = change.getControlNewText();
			if (validDoubleText.matcher(newText).matches()) {
				return change;
			} else
				return null;
		});
		t.setTextFormatter(textFormatter);
	}
	
	private void setComboListener() {
		barSingleItem.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Product>() {
			@Override
			public void changed(ObservableValue<? extends Product> selected, Product oldChoice, Product newChoice) {
				if (ignoreCombo) {
					ignoreCombo = false;
				} else {
					singleItemChosen = newChoice;
					System.out.println("new choice for singleItemChosen: " + newChoice);
					yChosen = 2;
					yChanged(false, false, true);
				}
			}
		});
		barSingleItem.setCellFactory(new Callback<ListView<Product>, ListCell<Product>>(){
            @Override
            public ListCell<Product> call(ListView<Product> p) {
                ListCell<Product> cell = new ListCell<Product>(){
                    @Override
                    protected void updateItem(Product product, boolean empty) {
                        super.updateItem(product, empty);
                        if (product != null && !empty) {
                            setText(product.getWhatToShow());
                        } else {
                        	setText("");
                        }
                    }
                };
                return cell;
            }
        });
	}
	
	private void resetCombo(boolean saveState) {
		String itemName =  "";
		if (saveState && singleItemChosen != null) {
			itemName = singleItemChosen.toString();
		}
		ignoreCombo = true;
		barSingleItem.getItems().clear();
		barSingleItem.getItems().addAll(dbHandler.getProducts());
		if (saveState && !itemName.isEmpty()) {
			for (Product p : barSingleItem.getItems()) {
				if (p.getName().equals(itemName)) {
					ignoreCombo = true;
					barSingleItem.getSelectionModel().select(p);
					singleItemChosen = p;
					break;
				}
			}
		} else {
			ignoreCombo = true;
			barSingleItem.setValue(new Product("None"));
			System.out.println("IgnoreCombo value: " + (ignoreCombo));
		}
	}
	
	private void xChanged(boolean b0, boolean b1, boolean b2) {
		xAxisRanks.setSelected(b0);
		xAxisGender.setSelected(b1);
		xAxisTeams.setSelected(b2);
		setupChart();
	}
	private void yChanged(boolean b0, boolean b1, boolean b2) {
		barCategories.setSelected(b0);
		barAllItems.setSelected(b1);
		if (!b2) {
			ignoreCombo = true;
			barSingleItem.setValue(new Product("None"));
		}
		setupChart();
	}
	@FXML void ranksChecked() {
		xChosen = 0;
		xChanged(true, false, false);
	}
	@FXML void teamsChecked() {
		xChosen = 1;
		xChanged(false, false, true);
	}
	@FXML void genderChecked() {
		xChosen = 2;
		xChanged(false, true, false);
	}
	@FXML void categoriesChecked() {
		yChosen = 0;
		yChanged(true, false, false);
	}
	@FXML void allItemsChecked() {
		yChosen = 1;
		yChanged(false, true, false);
	}
//	@FXML void singleItemChecked() {
//		yChosen = 2;
//		yChanged(false, false, true);
//	}
	@FXML void totalChecked() {
		proportionTotal.setSelected(true);
		proportionAverage.setSelected(false);
		propChosen = 0;
		setupChart();
	}
	@FXML void averageChecked() {
		proportionAverage.setSelected(true);
		proportionTotal.setSelected(false);
		propChosen = 1;
		setupChart();
	}
	
	// Sets up the barchart, also used to refresh barchart
	public void setupChart() {
		highestValue = 0.0;
		lowestValue = Double.MAX_VALUE;
		graphPane.getChildren().clear();
		xAxis = new CategoryAxis();
        yAxis = new NumberAxis();
		bc = new StackedBarChart<String,Number>(xAxis, yAxis);
        graphPane.add(bc, 0, 0);
        bc.getData().clear();
        bc.setPrefSize(graphPane.getWidth() * 0.8, graphPane.getHeight() * 0.8);
        bc.setAnimated(false);
//        bc.setTitle("Drinking stuff");
        series = new ArrayList<CustomSeries>();
//        bc.setStyle("-fx-border-radius: 10;");
        
        boolean data = false;
        
        // Setup total, all or single item
        if (yChosen == 0) { 
        	for (String s : GlobalVariables.getCategories()) {
        		if (addSeries(s, s, true)) data = true;
        	}
        } else  if (yChosen == 1) { 
            for (Product p : dbHandler.getProducts()) {
            	if (addSeries(p.getID(), p.getName(), false)) data = true;
            }
        } else  if (yChosen == 2) {
        	data = addSeries(singleItemChosen.getID(), singleItemChosen.getName(), false);
        }

        	
        if (series.size() > 0) {
        	if (series.get(0).getSize() != 0) {
        // Get the total value for columns
        ArrayList<Double> totals = new ArrayList<Double>();
        for (int i = 0; i < series.get(0).getSize(); i ++) {
    		totals.add(0.0d);
        	for (int j = 0; j < series.size(); j ++) {
        		totals.set(i, totals.get(i) + series.get(j).get(i));
        	}
        	if (totals.get(i) < lowestValue) lowestValue = totals.get(i);
        }
        
        // Find indices of sorted list, index 0 is highest
        ArrayList<Integer> indices = new ArrayList<Integer>();
        for (int i = 0; i < totals.size(); i ++) {
        	double highest = -1;
        	int highestIndex = -1;
        	for (int j = 0; j < totals.size(); j ++) {
        		if (!indices.contains(j) && totals.get(j) >= highest) {
        			highest = totals.get(j);
        			highestIndex = j;
        		}
        	}
    		indices.add(highestIndex);
        }
        
        // Setup chart
        for (int i = 0; i < series.size(); i ++) {
          XYChart.Series<String, Number> series1 = new XYChart.Series<String, Number>();
          series1.setName(series.get(i).getName());
//          System.out.println();
          for (int j = 0; j < series.get(i).getSize(); j ++) {
//        	  System.out.println("IndexName: " + series.get(i).getIndexName(indices.get(j)));
//        	  System.out.println("IndexValue: " + series.get(i).get(indices.get(j)));
        	  series1.getData().add(new XYChart.Data<String, Number>(series.get(i).getIndexName(indices.get(j)), series.get(i).get(indices.get(j))));
          }
          
          bc.getData().add(series1);
        }
        	
        
       
        if (bc.getData().size() > 0 && (yChosen == 1 || yChosen == 0)) {
//        	for (int i = 0; i < bc.getData().get(0).getData().size(); i ++) {
//        		double highestTemp = 0.0;
//        		for (int j = 0; j < bc.getData().size(); j  ++) {
//        	        highestTemp += (double) bc.getData().get(j).getData().get(i).getYValue();
//        		}
//        		if (highestTemp > highestValue) highestValue = highestTemp;
//        	}\
        }
//        System.out.println("Indices:" + indices.size());
//        System.out.println("Totals:" + totals.size());
    	highestValue = totals.get(indices.get(0));

        bc.setCategoryGap(60);
        
        // Setup y axis ranges
//        yAxis.setAutoRanging(false);
//        yAxis.setUpperBound(Math.ceil(highestValue));
//        if (lowestValue > 20) {
//            yAxis.setLowerBound(Math.floor(lowestValue - 20));
//            yAxis.setTickUnit(Math.ceil((highestValue - lowestValue + 20) / 5));
//        } else {
//            yAxis.setLowerBound(0);
//            yAxis.setTickUnit(Math.ceil(highestValue / 5));
//        }
        }
        }
        if (!data) { 
        	graphPane.getChildren().clear();
        	Label label = new Label("No data");
        	label.setMinWidth(300);
        	graphPane.add(label, 0, 0);
        }
        // Sort graph 
//        Collections.sort(bc.getData().get(0).getData(), new Comparator<XYChart.Data<String,Number>>() {
//			@Override
//			public int compare(Data<String, Number> numb0, Data<String, Number> numb1) {
//				double val0 =  (double) numb0.getYValue();
//				double val1 =  (double) numb1.getYValue();
//				return (val0 < val1 ? 1 : (val0 == val1 ? 0 : -1));
//			}
//		});
	}
	
	public boolean addSeries(String id, String seriesName, boolean category) {
		boolean result = false;
		ArrayList<String> texts = new ArrayList<String>();
		ArrayList<Double> values = new ArrayList<Double>();
       
		// Which, rank, team or gender
		if (xChosen == 0) getRankStats(texts, values, id, category);
     	if (xChosen == 1) getTeamStats(texts, values, id, category);
     	if (xChosen == 2) getGenderStats(texts, values, id, category);
     	// Create series
//       XYChart.Series<String, Number> series1 = new XYChart.Series<String, Number>();
     	CustomSeries series1 = new CustomSeries();
       series1.setName(seriesName); 
       
       if (texts.size() > 0) result = true;
       
       // Add
//		while (texts.size() > 0) {
//			result = true;
//			double highest = 0;
//			int index = 0;
//			for (int i = 0; i < texts.size(); i++) {
//				if (values.get(i) > highest) {
//					highest = values.get(i);
//					index = i;
//				}
//				if (values.get(i) < lowestValue) lowestValue = values.get(i);
//			}
//			if (highest > highestValue) highestValue = highest;
////			series1.getData().add(new XYChart.Data<String, Number>(texts.get(index),values.get(index)));
//			series1.add(texts.get(index),values.get(index));
//			
//			texts.remove(index);
//			values.remove(index);
//		}
       
       for (int i = 0; i < values.size(); i ++) {
    	   series1.add(texts.get(i), values.get(i));
       }
       
//		bc.getData().add(series1);
		System.out.println();
		System.out.println("Series data");
		for (int i = 0; i < series1.data.size(); i ++) {
			System.out.println(series1.getIndexName(i) + " : " + series1.get(i));
		}
		System.out.println("-------------------------");
		
		series.add(series1);
		return result;
	}
	
	public void getRankStats(ArrayList<String> ranks, ArrayList<Double> values, String ID, boolean category) {
		ranks.addAll(GlobalVariables.getRanks());
        ArrayList<Integer> amountOfPeople = new ArrayList<Integer>();
        for (int i = 0; i < ranks.size(); i ++) { values.add(0.0); amountOfPeople.add(0);}
        for (User user : dbHandler.getUsers()) {
        	for (int i = 0; i < ranks.size(); i ++) {
        		if (user.getRank().equals(ranks.get(i))) {
        			if (category) {
            			values.set(i, values.get(i) + user.getCategoryItems(dbHandler.getCategoryIDS(ID)));        				
        			} else {
        				values.set(i, values.get(i) + user.getProductFromID(ID).getAmount());
        			}
        			amountOfPeople.set(i, amountOfPeople.get(i) + 1);
        			break;
        		}
        	}
        }
		if (propChosen == 1) {
			for (int i = 0; i < values.size(); i++) {
				if (amountOfPeople.get(i) > 0) {
					values.set(i, values.get(i) / amountOfPeople.get(i));
				}
			}
		}
	}
	
	public void getTeamStats(ArrayList<String> teams, ArrayList<Double> values, String ID, boolean category) {
		teams.addAll(GlobalVariables.getTeams());
        ArrayList<Integer> amountOfPeople = new ArrayList<Integer>();
        for (int i = 0; i < teams.size(); i ++) { values.add(0.0); amountOfPeople.add(0);}
        for (User user : dbHandler.getUsers()) {
        	for (int i = 0; i < teams.size(); i ++) {
//        		System.out.println("------------------");
//        		System.out.println("Stats for team " + teams.get(i));
        		if (user.getTeam().equals(teams.get(i))) {
        			if (category) {
//        				System.out.println("User " + user.getName());
        				System.out.println("------------------------");
        				System.out.println("IDS for category: " + ID);
        				for (String s : dbHandler.getCategoryIDS(ID)) System.out.println(s);;
            			values.set(i, values.get(i) + user.getCategoryItems(dbHandler.getCategoryIDS(ID)));        				
        			} else {
        				values.set(i, values.get(i) + user.getProductFromID(ID).getAmount());
        			}
        			amountOfPeople.set(i, amountOfPeople.get(i) + 1);
        			break;
        		}
        	}
        }
		if (propChosen == 1) {
			for (int i = 0; i < values.size(); i++) {
				if (amountOfPeople.get(i) > 0) {
					values.set(i, values.get(i) / amountOfPeople.get(i));
				}
			}
		}
	}
	
	public void getGenderStats(ArrayList<String> genders, ArrayList<Double> values, String ID, boolean category) {
		for (String s : GlobalVariables.getGenders()) genders.add(s);
        ArrayList<Integer> amountOfPeople = new ArrayList<Integer>();
        for (int i = 0; i < genders.size(); i ++) { values.add(0.0); amountOfPeople.add(0);}
        for (User user : dbHandler.getUsers()) {
        	for (int i = 0; i < genders.size(); i ++) {
        		if (user.getGender().equals(genders.get(i))) {
        			if (category) {
            			values.set(i, values.get(i) + user.getCategoryItems(dbHandler.getCategoryIDS(ID)));        				
        			} else {
        				values.set(i, values.get(i) + user.getProductFromID(ID).getAmount());
        			}
        			amountOfPeople.set(i, amountOfPeople.get(i) + 1);
        			break;
        		}
        	}
        }
		if (propChosen == 1) {
			for (int i = 0; i < values.size(); i++) {
				if (amountOfPeople.get(i) > 0) {
					values.set(i, values.get(i) / amountOfPeople.get(i));
				}
			}
		}
	}
	
	public void animate() {
		counter = 0;
		Timeline backupTimer = new Timeline(new KeyFrame(Duration.seconds(0.1), event -> {

			// Async task 
			Task<Void> task = new Task<Void>() {
				@Override
				public Void call() throws Exception {
					updateMessage("started");
//					sta.backupDatabase();
//					Thread.sleep(1000);
					updateMessage("done");
					return null;
				}
			};
			// Display message
			task.messageProperty().addListener((obs, oldMessage, newMessage) -> {
//				if (newMessage.equals("started")) {
					bannerText.setTextFill(new Color(counter,0,0,1));
//					Platform.runLater(() -> bannerText.setTextFill(new Color(0,0,0,1)));
//					bannerText.setTextFill(Color.YELLOW);
//					Platform.runLater(() -> bannerText.setTextFill(Color.YELLOW));
					counter = (counter + 0.02) % 1;
//				} else {
////					popup.hide();
////					GlobalVariables.backupInProgress = false;
//				}
			});
			new Thread(task).start();
		
		}));
		backupTimer.setCycleCount(Animation.INDEFINITE);

		backupTimer.play();
	}
	
	@FXML void changed() {
		
	}
}
