package controllers;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import controllerAssistants.BarCodeExport;
import controllerAssistants.IOUsersProducts;
import controllerAssistants.SettingsTabAssistant;
import database.DatabaseHandler;
import database.GlobalVariables;
import importedCode.NumberFormatChecker;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.GridPane;
import javafx.stage.Popup;
import javafx.util.Duration;
import managers.DialogManager;

public class SettingsTabController implements Initializable {

	@FXML
	TextField i0, /*i2,*/ i3, i4, i6, i7;
	@FXML Button b0, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12, b14;
	DatabaseHandler dbHandler;
	MainTabController mtc;
	SettingsTabAssistant sta;
	
	/* 
	 * -1 = nothing, 0 = teams, 1 = ranks, 2 = import users, 3 = export users, 
	 *  4 = import products, 5 = export products, 6 = recovery email, 7 = categories,
	 *  8 = password, 9 = barCodes, 10 = advancedSettings
	*/
	public int showContent = -1;

	@FXML
	GridPane changeSettingsPane;
	
	ArrayList<TextField> textOptions;
	public ArrayList<Button> deleteButtons;
	ArrayList<String> teamRankTracker;
	TextField recoverEmail, password;
	boolean ignoreSelectionChanged = false;
	public ToolBar toolBar;
	Timeline backupTimer;
	int rate = 10;
	boolean backupActive = false;
	
	BarCodeExport bce;
	IOUsersProducts IOUP;
	
	

	public void init(DatabaseHandler dbHandler, MainTabController mtc) {
		this.dbHandler = dbHandler;
		this.mtc = mtc;
		textOptions = new ArrayList<TextField>();
		deleteButtons = new ArrayList<Button>();
		teamRankTracker = new ArrayList<String>();
		sta = new SettingsTabAssistant();
		bce = new BarCodeExport(changeSettingsPane, sta, mtc, dbHandler, this);
		IOUP = new IOUsersProducts(changeSettingsPane, sta, mtc, dbHandler, this);
		IOUP.init();
		setTextFieldsEditable(false);
		refreshVariables();
//		setupBackup();
	}
	
	

	@Override
	public void initialize(URL location, ResourceBundle resources) {
	}

	//Setting editabillity of textFields, only used false. They are textFields and not labels
	//so that the user can highlight and copy the variables.
	public void setTextFieldsEditable(boolean editable) {
		i0.setEditable(editable);
//		i2.setEditable(editable);
		i3.setEditable(editable);
		i4.setEditable(editable);
//		i5.setEditable(editable);
		i6.setEditable(editable);
		i7.setEditable(editable);
	}

	//Refreshing variables shown in the right side
	public void refreshVariables() {
		i0.setText(GlobalVariables.databasePath);
//		i2.setText((GlobalVariables.bannerIconPath.isEmpty() ? "Internal" : GlobalVariables.bannerIconPath));
		i3.setText(GlobalVariables.getRecoveryEmail());
		i4.setText(GlobalVariables.getPassword());
		if (backupActive) {
			i6.setText(GlobalVariables.backupLocation);
			i6.getStyleClass().remove("red-text-field");
		}		else {
			i6.setText(GlobalVariables.backupLocation + " (Disabled)");
			if (!i6.getStyleClass().contains("red-text-field")) i6.getStyleClass().add("red-text-field");
		}
		i7.setText(GlobalVariables.backupTimer + "");
	}
	
	// Button press "change settings"
	@FXML void changeSettings() {
		changeSettingsPane.getChildren().clear();
		showContent = 8;
		setButtonHighlight();

		Button passwordSave = new Button(), backupTimerSave = new Button(),
		/*banner = new Button("Choose"),*/ backupLocation = new Button("Choose"),
		emailSave = new Button(), databaseLocation = new Button("Choose");
		TextField backupTimer = new TextField(),
		password = new TextField(GlobalVariables.getPassword()),
		email = new TextField(GlobalVariables.getRecoveryEmail());
		ComboBox<String> theme = new ComboBox<String>(), lockScanner = new ComboBox<String>();
		
		sta.setupChangeSettingsGUI(changeSettingsPane, /*banner,*/ backupLocation, passwordSave, backupTimerSave, 
				password, backupTimer, theme, lockScanner, email, emailSave, databaseLocation,
				DialogManager.loadText("/resources/tooltips/settings/Settings.txt"));
		backupTimer.setText(GlobalVariables.backupTimer + "");
		/*banner.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				changeBannerLocation();				
			}
		});*/
		databaseLocation.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				changeDatabaseLocation();
			}
		});
		backupLocation.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				changeBackupLocation();
			}
		});
		passwordSave.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (password.getText() != null) {
					if (password.getText().isEmpty()) {
						DialogManager.showPopup("Empty  field");
					} else {
						if (!dbHandler.editSettings("password", password.getText())) {
							DialogManager.showPopup("Failed to save password");
						} else {
							GlobalVariables.password = password.getText();
							refreshVariables();
							DialogManager.showPopup("Saved password");
						}
					}
				}
			}

		});
		backupTimerSave.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (backupTimer.getText() != null) {
					if (NumberFormatChecker.isDouble(backupTimer.getText()) ) {
							if (!dbHandler.editSettings("backupTimer", backupTimer.getText())) {
								DialogManager.showPopup("Failed to save backup timer");
							} else {
								GlobalVariables.backupTimer = Double.valueOf(backupTimer.getText());
								refreshVariables();
								DialogManager.showPopup("Saved backup timer");	
								setupBackup();
							}
							return;
						
					} 
					DialogManager.showPopup("Invalid format");
					
				}
			}
		});
		emailSave.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				String text = email.getText();
				if (text != null) {
					if (!text.isEmpty()) {
						String[] split = text.split("\\@");
						if (split.length != 2) {
							DialogManager.showPopup("Email contains wrong number of '@'");
							return;
						}
						if (!split[1].contains(".")) {
							DialogManager.showPopup("Email doesnt have '.' after '@'");
							return;
						}
						if (!dbHandler.editSettings("recoveryEmail", text)) {
							DialogManager.showPopup("Failed to save email");
							return;
						}
						GlobalVariables.setRecoveryEmail(text);
						refreshVariables();
						DialogManager.showPopup("Recovery email was saved");
					}
				}
			}
		});
		theme.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> selected, String oldChoice, String newChoice) {
				switch (newChoice.toLowerCase()) {
					case "darcula": GlobalVariables.currentThemeInt = 1; 
					Platform.runLater(() -> mtc.setTheme(GlobalVariables.currentThemeInt));break;
					case "default": GlobalVariables.currentThemeInt = 0; 
					Platform.runLater(() -> mtc.setTheme(GlobalVariables.currentThemeInt));break;
				}
				if (!dbHandler.editSettings("theme", GlobalVariables.getSaveStrings("theme"))) {
					DialogManager.showPopup("Failed to save theme");
				}
				DialogManager.showPopup("Theme was saved");
				
				refreshVariables();
			}
		});
		lockScanner.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> selected, String oldChoice, String newChoice) {
				switch (newChoice.toLowerCase()) {
					case "yes": GlobalVariables.lockScan = true; break;
					case "no": GlobalVariables.lockScan = false; break;
				}
				if (!dbHandler.editSettings("lockScan", GlobalVariables.getSaveStrings("lockScan"))) {
					DialogManager.showPopup("Failed to save lockScan");
				}
				DialogManager.showPopup("lockScan was saved");
				refreshVariables();
			}
		});
	}
	
	// Sets the backup timer, initially called at the end of mtc.init
	public void setupBackup() {
		Popup popup = DialogManager.getPopup(mtc.getStage(), "Backup in progress");
		if (backupTimer != null)
			backupTimer.stop();
		if (GlobalVariables.backupTimer != 0.0) {
			backupActive = true;
			refreshVariables();
			// Loop of backup
			backupTimer = new Timeline(new KeyFrame(Duration.hours(GlobalVariables.backupTimer), event -> {

				File directory = new File(GlobalVariables.backupLocation);
				if (directory.exists() && directory.isDirectory()) {
				GlobalVariables.backupInProgress = true;
				// Async task to execute backup
				Task<Void> task = new Task<Void>() {
					@Override
					public Void call() throws Exception {
						updateMessage("started");
						sta.backupDatabase();
						updateMessage("done");
						return null;
					}
				};
				// Display message
				task.messageProperty().addListener((obs, oldMessage, newMessage) -> {
					if (newMessage.equals("started")) {
						popup.show(mtc.getWindow());
					} else {
						popup.hide();
						GlobalVariables.backupInProgress = false;
					}
				});
				new Thread(task).start();
				} else {
					// Alert, using runLater because showAndWait cannot be called from task
					Platform.runLater(() -> {
						DialogManager.alertError("Backup error", null, "Failed to backup database. Confirm chosen directory exists:\n" +
								GlobalVariables.backupLocation + "\n" +
								"Or else choose a new location for backup or disable backup (Set timer to 0)");
					});
					backupActive = false;
					refreshVariables();
					backupTimer.stop();
				}
			}));
			backupTimer.setCycleCount(Animation.INDEFINITE);

			backupTimer.play();
		} else {
			backupActive = false;
			refreshVariables();
		}
	}
	
	// Button press "change database location"
	private void changeDatabaseLocation() {
		File selectedFile = DialogManager.getExistingDatabase("db",
				new File(GlobalVariables.databasePath).getParent());
		if (selectedFile == null) {
			DialogManager.showPopup("Database location change cancelled");
		} else {
			String chosenPath = selectedFile.getAbsolutePath();
			GlobalVariables.databasePath = chosenPath;
			GlobalVariables.databaseRoot = new File(chosenPath).getParent();
			GlobalVariables.setPreferences(GlobalVariables.databasePath, GlobalVariables.databaseRoot);
//			dbHandler.loadDatabase();
			mtc.load();
			refreshVariables();
			DialogManager.showPopup("Database location changed");
		}
	}

	//Button press "change banner icon"
	private void changeBannerLocation() {
		File directory = DialogManager.getExistingFile(new String[] { "jpg", "png" },
				new File(GlobalVariables.databasePath).getParent());
		if (directory != null) {
			String[] temp = directory.getAbsolutePath().split("\\.");
			if (temp[temp.length - 1].equals("jpg") || temp[temp.length - 1].equals("png")) {
				if (dbHandler.editSettings("bannerImage", directory.getAbsolutePath())) {
					GlobalVariables.setBannerLocation(directory.getAbsolutePath());
					refreshVariables();
					mtc.setBanner();
					DialogManager.showPopup("Saved banner icon");
				} else {
					DialogManager.showPopup("Failed to save banner icon");
				}
			} else {
				DialogManager.alertError("File error", null, "Wrong extension on file");
			}
		}
	}
	
	// Changes location for backup of database
	private void changeBackupLocation() {
		File directory = DialogManager.getUserChosenDirectory(new File(GlobalVariables.databasePath).getParent());
		if (directory != null) {
			String path = directory.getAbsolutePath();
			if (dbHandler.editSettings("backupLocation", path)) {
				GlobalVariables.backupLocation = path;
				backupActive = true;
				refreshVariables();
				setupBackup();
				DialogManager.showPopup("Saved backup location");
			} else {
				DialogManager.showPopup("Failed to save backup location");
			}
		}
	}
	
	// Saves recovery email, for password recovery
	private boolean saveRecoveryEmail() {
		String newEmail = recoverEmail.getText();
		if (newEmail != null) {
			if (!newEmail.isEmpty()) {
				if (dbHandler.editSettings("recoveryEmail", newEmail)) {
					GlobalVariables.setRecoveryEmail(newEmail);
					refreshVariables();
					changeSettingsPane.getChildren().clear();
					return true;
				}
			}
		}
		return false;
	}
	
	/* 
	 * -1 = nothing, 0 = teams, 1 = ranks, 2 = import users, 3 = export users, 
	 *  4 = import products, 5 = export products, 6 = recovery email, 7 = categories,
	 *  8 = password
	*/
	
	// Sets the highlight of buttons according to what content is being shown
	public void setButtonHighlight() {
		b0.getStyleClass().remove("buttonHighlight");
		b3.getStyleClass().remove("buttonHighlight");
		b4.getStyleClass().remove("buttonHighlight");
		b5.getStyleClass().remove("buttonHighlight");
		b7.getStyleClass().remove("buttonHighlight");
		b8.getStyleClass().remove("buttonHighlight");
		b9.getStyleClass().remove("buttonHighlight");
		b10.getStyleClass().remove("buttonHighlight");
		b12.getStyleClass().remove("buttonHighlight");
		b14.getStyleClass().remove("buttonHighlight");
		switch(showContent) {
		case 0: b3.getStyleClass().add("buttonHighlight"); break;
		case 1: b4.getStyleClass().add("buttonHighlight"); break;
		case 2: b7.getStyleClass().add("buttonHighlight"); break;
		case 3: b8.getStyleClass().add("buttonHighlight"); break;
		case 4: b9.getStyleClass().add("buttonHighlight"); break;
		case 5: b10.getStyleClass().add("buttonHighlight"); break;
		case 7: b5.getStyleClass().add("buttonHighlight"); break;
		case 8: b0.getStyleClass().add("buttonHighlight"); break;
		case 9: b12.getStyleClass().add("buttonHighlight"); break;
		case 10: b14.getStyleClass().add("buttonHighlight"); break;
		}
	}

	// Function was removed
//	//Button press "change database location"
//	private void changeDatabaseLocation() {
//		int choice = DialogManager.chooseDatabasePathDialog();
//		switch (choice) {
//		case 3:	GlobalVariables.resetDatabasePath();break;
//		case 2:	setNewDatabaseFile(); break;
//		case 1:	setNewDatabaseFolder(); break;
//		case 0: ; break;
//		}
//		refreshVariables();
//	}
	
	//Button press "change password"
	@FXML void changePassword() {
		showContent = 8;
		setButtonHighlight();
		changeSettingsPane.getChildren().clear();
		setToolBar(0, -1, null, true, true, false, "This password is used to login to the admin page (This page)."
				+ " If the user forgets the password, it can be recovered through the recover email, by"
				+ " sending a mail containing the password\n\n"
				+ "Press 'Esc' to close");
		password = new TextField(GlobalVariables.getPassword());
		changeSettingsPane.add(password, 0, 1);
	}

	// Saves password
	private boolean savePassword() {
		String newPassword = password.getText();
		if (newPassword != null) {
			if (!newPassword.isEmpty()) {
				if (dbHandler.editSettings("password", newPassword)) {
					GlobalVariables.setPassword(newPassword);
					refreshVariables();
					changeSettingsPane.getChildren().clear();
					return true;
				}
			}
		}
		return false;
	}
	
	
	@FXML void exportBarCodes() {
		showContent = 9;
		changeSettingsPane.getChildren().clear();
		textOptions.clear();
		setButtonHighlight();
		bce.exportBarCodes();
	}
	
	
	// Button press "Advanced settings"
	@FXML void advancedSettings() {
		showContent = 10;
		setButtonHighlight();
		changeSettingsPane.getChildren().clear();
		Button sync = new Button("Validate project synchronization"), 
				reset = new Button("Reset database");
		sta.setupAdvancedSettings(changeSettingsPane, sync,reset);
		sync.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				if (mtc.validateDatabaseSync()) {			
					DialogManager.showPopup("Database is synchronized");
				} else {
					DialogManager.showPopup("Database out of sync");
				}
			}
		});
		reset.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				resetDatabase();			
			}
		});
	}
	
	//Button press "reset database"
	private void resetDatabase() {
		boolean confirm = DialogManager.alertConfirmation("Reset database", null,
				"Are you sure you want to reset the database. "
						+ "This will delete all users, all products and reset saved settings.");
		if (confirm) {
			boolean confirmAgain = DialogManager.alertConfirmation("Reset database", null,
					"Are you like really sure?? This cannot be undone :O");
			if (confirmAgain) {
				File db = new File(GlobalVariables.databasePath);
				boolean success = false;
				if (db.exists()) {
					success = db.delete();
				}
				if (!mtc.resetTransactions() && success) {
					DialogManager.alertError("Failed to delete transactions",null,
							"One or more transactions weren't reset");
				}
				if (success) {
					mtc.init();
					mtc.load();
					DialogManager.showPopup(
							"Database was reset");
				} else {
					DialogManager.alertError("Database reset", null, "An error ocurred, couldn't reset database");
				}

			}

		}
	}

	//Button press "edit ranks"
	@FXML
	void editRanks() {
		showContent = 1;
		setButtonHighlight();
		ArrayList<String> ranks = GlobalVariables.getRanks();
		
		teamRankTracker.clear();
		for (String s : ranks) {
			teamRankTracker.add(s);
		}
		
		addFields(ranks,  DialogManager.loadText("/resources/tooltips/settings/Ranks.txt"));
	}
	
	//Button press "edit team"
	@FXML
	void editTeams() {
		showContent = 0;
		setButtonHighlight();
		ArrayList<String> teams = GlobalVariables.getTeams();
		teamRankTracker.clear();
		for (String s : teams) {
			teamRankTracker.add(s);
		}
		
		addFields(teams, DialogManager.loadText("/resources/tooltips/settings/Teams.txt"));
		
	}
	
	//Button press "edit categories"
	@FXML void editCategories() {
		showContent = 7;
		setButtonHighlight();
		ArrayList<String> categories = GlobalVariables.getCategories();
		teamRankTracker.clear();
		for (String s : categories) {
			teamRankTracker.add(s);
		}
		
		addFields(categories, DialogManager.loadText("/resources/tooltips/settings/Categories.txt"));
	}
	
	// Setting up gridPane to edit categories/ranks/teams layout
	public void addFields(ArrayList<String> options, String toolTip) {
		changeSettingsPane.getChildren().clear();
		setToolBar(0, -1, null, true, true, true, toolTip);
		textOptions.clear();
		for (int i = 0; i < options.size(); i++) {
			textOptions.add(new TextField(options.get(i)));
			addDelete(textOptions.get(i), i, 1);
			changeSettingsPane.add(textOptions.get(i), 0, i + 1);
		}
	}
	
	//Saves ranks or teams...
	public void saveContent() {
		switch(showContent) {
		//Teams
		case 0: 
			if (saveTeams()) {
				contentSaved("Teams saved");
			} else {
				contentFailed("Failed to save teams");
			} break;
		//Ranks
		case 1: 
			if (saveRanks()) {
				contentSaved("Ranks saved");
			} else {
				contentFailed("Failed to save ranks");
			} break;
		// Recovery email
		case 6:
			if (saveRecoveryEmail()) {
				contentSaved("Recovery email saved");
			} else {
				contentFailed("Failed to save recovery email");
			} break;
		// Categories
		case 7:
			if (saveCategories()) {
				contentSaved("Categories saved");
			} else {
				contentFailed("Failed to save categories");
			} break;
		// Password
		case 8:
			if (savePassword()) {
				contentSaved("Password saved");
			} else {
				contentFailed("Failed to save password");
			} break;
		}
	}
	
	// Method for successful save, used in saveContent
	private void contentSaved(String text) {
		DialogManager.showPopup(text);
		changeSettingsPane.getChildren().clear();
		showContent = -1;
		setButtonHighlight();
	}
	
	// Method for failed save, used in saveContent
	private void contentFailed(String text) {
		DialogManager.showPopup(text);
		mtc.load();
	}
	
	// Saves categories
	private boolean saveCategories() {
		ArrayList<String> values = new ArrayList<String>();
		for(int i = 0; i < textOptions.size(); i ++) {
			if (textOptions.get(i).getText() == null) values.add("");
			else values.add(textOptions.get(i).getText());
		}
		ArrayList<String> newValues = GlobalVariables.getNewCategories(values);
		ArrayList<String> missingValues = GlobalVariables.getMissingCategories(teamRankTracker);
		ArrayList<Integer> changedIndices = new ArrayList<Integer>();
		for (int i = 0; i < teamRankTracker.size(); i ++) {
			if (!values.get(i).equals(teamRankTracker.get(i))) changedIndices.add(i);
		}
		boolean confirm = DialogManager.alertConfirmation("Changing categories", null, ""
				+ "Found: \n" + 
				(newValues.size() - changedIndices.size()) + " new categor" + 
				(newValues.size() - changedIndices.size() == 1 ? "y" : "ies") + "\n" +
				(changedIndices.size()) + " changed categor" + 
				(changedIndices.size() == 1 ? "y" : "ies") + "\n" + 
				(missingValues.size()) + " deleted categor"+ 
				(missingValues.size() == 1 ? "y" : "ies"));
		if (confirm) {
			
			// Changing the changed categories
			for (int i : changedIndices) {
				GlobalVariables.updateCategory(teamRankTracker.get(i), values.get(i));
				if (!dbHandler.updateCategories(teamRankTracker.get(i), values.get(i))) {
					return false;
				}
			}
			// Adding the new categories
			for (String s : newValues) {
				boolean addIt = true;
				for (int i : changedIndices) {
					if(s.equals(values.get(i))) addIt = false;
				}
				if (addIt) GlobalVariables.addCategory(s);
			}
			
			// Removing deleted categories
			for (String s : missingValues) {
				GlobalVariables.deleteCategory(s);
				dbHandler.updateCategories(s, "Unknown");
			}
			
			// Resetting categories if all were deleted
			if (textOptions.size() <= 0) GlobalVariables.setCategoriesEmpty();
			
			// Ensuring the default category "Unknown" exists if another category has been deleted
			if (missingValues.size() != 0) {
				if (!GlobalVariables.getCategories().contains("Unknown")) {
					GlobalVariables.addCategory("Unknown");
					
				}
			}
			dbHandler.cleanupRTC();
			// Update database of categories
			if (!dbHandler.editSettings("categories", GlobalVariables.getSaveStrings("categories"))) {
				return false;
			}
			mtc.updateProductComboBoxes();
			mtc.load();
			return true;
		}
		return false;
	}
	
	// Save teams
	private boolean saveTeams() {
		ArrayList<String> values = new ArrayList<String>();
		// Get values to be saved
		for(int i = 0; i < textOptions.size(); i ++) {
			if (textOptions.get(i).getText() == null) values.add("");
			else values.add(textOptions.get(i).getText());
		}
		// Get new values
		ArrayList<String> newValues = GlobalVariables.getNewTeams(values);
		// Get deleted values
		ArrayList<String> missingValues = GlobalVariables.getMissingTeams(teamRankTracker);
		ArrayList<Integer> changedIndices = new ArrayList<Integer>();
		// Detect changed values
		for (int i = 0; i < teamRankTracker.size(); i ++) {
			if (!values.get(i).equals(teamRankTracker.get(i))) changedIndices.add(i);
		}
		boolean confirm = DialogManager.alertConfirmation("Changing teams", null, ""
				+ "Found: \n" + 
				(newValues.size() - changedIndices.size()) + " new team" + 
				(newValues.size() - changedIndices.size() == 1 ? "" : "s") + "\n" +
				(changedIndices.size()) + " changed team" + 
				(changedIndices.size() == 1 ? "" : "s") + "\n" + 
				(missingValues.size()) + " deleted team"+ 
				(missingValues.size() == 1 ? "" : "s"));
		if (confirm) {
			// Update changed values
			for (int i : changedIndices) {
				GlobalVariables.updateTeam(teamRankTracker.get(i), values.get(i));
				if (!dbHandler.updateTeams(teamRankTracker.get(i), values.get(i))) {
					return false;
				}
			}
			// Add new values
			for (String s : newValues) {
				boolean addIt = true;
				for (int i : changedIndices) {
					if(s.equals(values.get(i))) addIt = false;
				}
				if (addIt) GlobalVariables.addTeam(s);
			}
			// Delete missing values
			for (String s : missingValues) {
				GlobalVariables.deleteTeam(s);
				dbHandler.updateTeams(s, "Unknown");
			}
			if (textOptions.size() <= 0) GlobalVariables.setTeamsEmpty();
			if (missingValues.size() != 0) {
				if (!GlobalVariables.getTeams().contains("Unknown")) {
					GlobalVariables.addTeam("Unknown");
					
				}
			}
			dbHandler.cleanupRTC();
			// Commit changes
			if (!dbHandler.editSettings("teams", GlobalVariables.getSaveStrings("teams"))) {
				return false;
			}
					
			mtc.updateUserComboBoxes();
			mtc.load();
			return true;
		}
		return false;
	}
	
	// Saves the ranks
	private boolean saveRanks() {
		ArrayList<String> values = new ArrayList<String>();
		// Retrieve values to be saved
		for(int i = 0; i < textOptions.size(); i ++) {
			if (textOptions.get(i).getText() == null) values.add("");
			else values.add(textOptions.get(i).getText());
		}
		// Get new values
		ArrayList<String> newValues = GlobalVariables.getNewRanks(values);
		// Get deleted values
		ArrayList<String> missingValues = GlobalVariables.getMissingRanks(teamRankTracker);
		ArrayList<Integer> changedIndices = new ArrayList<Integer>();
		// Detect changed values
		for (int i = 0; i < teamRankTracker.size(); i ++) {
			if (!values.get(i).equals(teamRankTracker.get(i))) changedIndices.add(i);
		}
		boolean confirm = DialogManager.alertConfirmation("Changing ranks", null, ""
				+ "Found: \n" + 
				(newValues.size() - changedIndices.size()) + " new rank" + 
				(newValues.size() - changedIndices.size() == 1 ? "" : "s") + ".\n" +
				(changedIndices.size()) + " changed rank" + 
				(changedIndices.size() == 1 ? "" : "s") + "\n" +
				(missingValues.size()) + " deleted rank"+ 
				(missingValues.size() == 1 ? "" : "s"));
		if (confirm) {
			// Update changed values
			for (int i : changedIndices) {
				GlobalVariables.updateRank(teamRankTracker.get(i), values.get(i));
				if (!dbHandler.updateRanks(teamRankTracker.get(i), values.get(i))) {
					return false;
				}
			}
			// Add new values
			for (String s : newValues) {
				boolean addIt = true;
				for (int i : changedIndices) {
					if(s.equals(values.get(i))) addIt = false;
				}
				if (addIt) GlobalVariables.addRank(s);
			}
			// Delete missing values
			for (String s : missingValues) {
				GlobalVariables.deleteRank(s);
				dbHandler.updateRanks(s, "Unknown");
			}
			
			if (textOptions.size() <= 0) GlobalVariables.setRanksEmpty();
			if (missingValues.size() != 0) {
				if (!GlobalVariables.getRanks().contains("Unknown")) {
					GlobalVariables.addRank("Unknown");
					
				}
			}
			dbHandler.cleanupRTC();
			// Commit changes
			if (!dbHandler.editSettings("ranks", GlobalVariables.getSaveStrings("ranks"))) {
				return false;
			}
			mtc.updateUserComboBoxes();
			mtc.load();
			return true;
		}
		return false;
	}

	//Add textField or comboBox to gridPane
	private void addTextField(int id) {
		ObservableList<Node> list = changeSettingsPane.getChildren();
		int highest = -1;
		for (int i = 0; i < list.size(); i++) {
			int current = GridPane.getRowIndex(list.get(i));
			if (current > highest)
				highest = current;
		}
		//Ranks or teams, textField
		if (showContent == 0 || showContent == 1 || showContent == 7) {
			TextField textField = new TextField("");
			textOptions.add(textField);
			addDelete(textField, highest, 1);
			changeSettingsPane.add(textField, 0, highest + 1);
		
		//Import/export of users/products
		} else if (showContent >= 2 && showContent <= 5) {
			IOUP.addPressed(highest);
		} else if (showContent == 9) {
			bce.addPressed(id);
		}

	}

	//Button press, "import products"
	@FXML
	void importProducts() {
		IOUP.importProducts();
	}

	//Button press, "export users"
	@FXML
	void exportUsers() {
		IOUP.exportUsers();
	}

	//Button press, "export products"
	@FXML
	void exportProducts() {
		IOUP.exportProducts();
	}

	//Button press, "import users"
	@FXML
	void importUsers() {
		IOUP.importUsers();

	}

	//Clears the pane which shows variable content
	public void resetContent() {
		changeSettingsPane.getChildren().clear();
		showContent = -1;
		setButtonHighlight();
	}

	//Adds a delete button to the given row of textfields
	public void addDelete(Node node, int i, int columnIndex) {
		Button delete = new Button();
		sta.setButton(delete, "/resources/images/delete.png", 15, null);
		delete.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (node != null) {
				if (showContent == 0 || showContent == 1 || showContent == 7) {
					int index = GridPane.getRowIndex(node) - 1;
					if (index < teamRankTracker.size()) teamRankTracker.remove(index);
						textOptions.remove(node);

						changeSettingsPane.getChildren().remove(node);
						changeSettingsPane.getChildren().remove(delete);
						packOptions();
				} else if (showContent == 9) {
					bce.deletePressed(node);
				} else {
						IOUP.deletePressed(node, delete);
				}
				}
			}

		});
		changeSettingsPane.add(delete, columnIndex, i + 1);
		deleteButtons.add(delete);
	}
	
	//Repositioning elements to eliminate unwanted empty rows
	public void packOptions() {
		changeSettingsPane.getChildren().clear();
		changeSettingsPane.add(toolBar, 0, 0, 2, 1);
		
		// Teams, ranks and categories
		if (showContent == 0 || showContent == 1 || showContent == 7) {
			for (int i = 0; i < textOptions.size(); i ++) {
				addDelete(textOptions.get(i), i, 1);
				changeSettingsPane.add(textOptions.get(i), 0, i + 1);
			}
		// Import/export user/products
		} else if (showContent >= 2 && showContent <= 5) {
			IOUP.packOptions();
		} else if (showContent == 9) {
			
		}
	}
	
	// Setting up toolBar to be shown in top row of changeSettingsPane
	public void setToolBar(int rowIndex, int identifier, ToolBar toolBarNew, boolean addCancel, boolean addSave, boolean addAdd, String toolTipText) {
		toolBar = new ToolBar();
		if (toolBarNew == null) {
			toolBarNew = toolBar;
		}
		final int id = identifier;
		if (addCancel) {
			Button cancel = new Button();
			sta.setButton(cancel, "/resources/images/cancel.png", 15, toolBarNew);
			cancel.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent event) {
					resetContent();
				}

			});
		}
		if (addSave) {
			Button save = new Button();
			sta.setButton(save, "/resources/images/save.png", 15, toolBarNew);
			save.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent event) {
					saveContent();
				}

			});
		}
		if (addAdd) {
			Button add = new Button();
			sta.setButton(add, "/resources/images/add.png", 20, toolBarNew);
			add.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent event) {
					addTextField(id);
				}

			});
		}
		if (!toolTipText.isEmpty()) {
			Button toolTip = DialogManager.createTooltip(toolTipText);
			toolBarNew.getItems().add(toolTip);
		}
		toolBarNew.setStyle("-fx-background-color:transparent;");
		changeSettingsPane.add(toolBarNew, 0, rowIndex, 2, 1);

	}

	
	// Function has been disabled for now
	// Button press "change database location" -> "choose folder for new database"
	@SuppressWarnings("unused")
	private void setNewDatabaseFolder() {
		File directory = DialogManager.getUserChosenDirectory(new File(GlobalVariables.databasePath).getParent());
		if (directory != null) {
			String name = DialogManager.getTextInput("Name of database", 
					"Enter a name for the database (*.db), or cancel to use default name", null, ".db");
			if (name == null) name = GlobalVariables.databaseFileName;
			GlobalVariables.setNewDatabase(directory.getAbsolutePath() + "\\" + name
					+ (name.substring(name.length() - 3, name.length()).equals(".db") ? "" : ".db"));
			boolean answer = DialogManager.alertConfirmation("New database folder",
					"Do you want to create a new database?",
					"A new folder has been chosen, but a new database needs to be"
							+ " created in order for this application to save data. The database will be "
							+ "created in the chosen folder, with the name: " + GlobalVariables.databaseFileName);
			if (answer) {
				mtc.load();
			} else {

			}
		}
	}

	// Function has been disabled for now
	// Button press "change database location" -> "choose existing database"
	@SuppressWarnings("unused")
	private void setNewDatabaseFile() {
		File directory = DialogManager.getExistingDatabase("db", new File(GlobalVariables.databasePath).getParent());
		if (directory != null) {
			String[] temp = directory.getAbsolutePath().split("\\.");
			if (temp[temp.length - 1].equals("db")) {
				GlobalVariables.setExistingDatabase(directory.getAbsolutePath());
			} else {
				DialogManager.alertError("File error", null, "Wrong extension on file");
			}
		}
	}

}
