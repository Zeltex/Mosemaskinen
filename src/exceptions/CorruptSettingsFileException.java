package exceptions;

public class CorruptSettingsFileException extends Exception{
	private static final long serialVersionUID = 1L;
	String message;
	
	public CorruptSettingsFileException(String message) {
		this.message = message;
	}
	
	public String getMessage() {
		return message;
	}
}
